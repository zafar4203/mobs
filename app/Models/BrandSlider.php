<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandSlider extends Model
{
    protected $fillable = ['photo'];
    public $timestamps = false;
}
