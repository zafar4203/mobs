<input type="hidden" value="{{ $prods->total() }}" id="total-pp" />
@if (count($prods) > 0)
	@foreach ($prods as $key => $prod)
    <!-- .product -->
    <div class="product {{$key == 0?'first':''}}">
        <div class="yith-wcwl-add-to-wishlist">
            <a id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
        </div>
        <!-- .yith-wcwl-add-to-wishlist -->
        <a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="{{ route('front.product',$prod->slug) }}">
            <img width="224" height="197" alt="" class="attachment-shop_catalog size-shop_catalog wp-post-image" src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}">
            <span class="price">
                <span class="woocommerce-Price-amount amount">{{ $prod->setCurrency() }}</span>
            </span>
            <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
        </a>
        <!-- .woocommerce-LoopProduct-link -->
        <div class="hover-area">
            <a data-href="{{ route('product.cart.add',$prod->id) }}" class="add-to-cart button" href="javascript:;">Add to cart</a>
            <a class="add-to-compare-link add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}">Add to compare</a>
        </div>
        <!-- .hover-area -->
    </div>
    <!-- .product -->
    @endforeach    

<div class="col-lg-12">
    <div class="page-center mt-5">
        {!! $prods->appends(['search' => request()->input('search')])->links('pagination.default') !!}


    </div>
</div>
@else
<div class="col-lg-12">
<div class="page-center">
        <h4 class="text-center">{{ $langg->lang60 }}</h4>
</div>
</div>
@endif


@if(isset($ajax_check))


<script type="text/javascript">


// Tooltip Section


$('[data-toggle="tooltip"]').tooltip({
});
$('[data-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});




$('[rel-toggle="tooltip"]').tooltip();

$('[rel-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});


// Tooltip Section Ends

</script>

@endif