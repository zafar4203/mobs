<div id="secondary" class="widget-area shop-sidebar" role="complementary">
    <div class="widget woocommerce widget_product_categories techmarket_widget_product_categories" id="techmarket_product_categories_widget-2">
        <ul class="product-categories ">
            <li class="product_cat">
                <span>Browse Categories</span>
                <ul class="show-all-cat">
                    @foreach ($categories as $element)
                    @if(count($element->subs) > 0)
                        <li class="product_cat">
                            <span class="show-all-cat-dropdown"><a href="{{ route('front.category' , $element->slug) }}">{{ $element->name }} ({{count($element->products)}})</a></span>
                            <ul style="border-top:1px solid #ececec;">
                                @foreach ($element->subs as $sub)
                                    @if(count($sub->childs) > 0)
                                        <li class="product_cat">
                                            <span class="show-all-cat-dropdown"><a href="{{ route('front.category' , [$element->slug , $sub->slug]) }}">{{ $sub->name }} ({{count($sub->products)}})</a></span>
                                            <ul style="border-top:1px solid #ececec;">
                                                @foreach ($sub->childs as $child)
                                                    <li class="cat-item"><a href="{{ route('front.category' , [$element->slug , $sub->slug , $child->slug]) }}">{{ $child->name }} ({{count($child->products)}})</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                    <li class="cat-item"><a href="{{ route('front.category' , [$element->slug , $sub->slug]) }}">{{ $sub->name }} ({{count($sub->products)}})</a></li>  
                                    @endif




                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="cat-item">
                            <a href="{{ route('front.category' , $element->slug) }}"><span class="no-child"></span>{{$element->name}} ({{ count($element->products) }})</a>
                        </li>
                    @endif
                    @endforeach
                </ul>
            </li>
        </ul>
    </div>
    <div id="techmarket_products_filter-3" class="widget widget_techmarket_products_filter">
        <span class="gamma widget-title">Filters</span>
        <div class="widget woocommerce widget_price_filter" id="woocommerce_price_filter-2">
            <p>
                <span class="gamma widget-title">Filter by price</span>
            </p>
            <form id="catalogForm" action="{{ route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')]) }}" method="GET">

                @if (!empty(request()->input('search')))
                <input type="hidden" name="search" value="{{ request()->input('search') }}">
                @endif
                @if (!empty(request()->input('sort')))
                <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
                @endif

                <input type="hidden" name="min" value="0" id="minamount" />
                <input type="hidden" name="max" value="500" id="maxamount" />
    
                <div class="price_slider_amount">
                    <input id="amount" type="text" placeholder="Min price" data-min="6" value="33" name="min_price" style="display: none;">
                    <button type="submit" class="button filter-btn" type="button">Filter</button>
                </div>
                <div id="slider-range" class="price_slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                    <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div>
                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span>
                    <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div>
                </div>
            </form>
        </div>

<div class="widget woocommerce widget_layered_nav maxlist-more" id="woocommerce_layered_nav-2">
        @if ((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) && !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) && !empty(json_decode($childcat->attributes, true))))
        <form id="attrForm" action="{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}" method="post">

        @if (!empty($cat) && !empty(json_decode($cat->attributes, true)))
            @foreach ($cat->attributes as $key => $attr)
                    <!-- Product Size Start -->

                        <span class="gamma widget-title">{{$attr->name}}</span>
                        <ul>
                            @if (!empty($attr->attribute_options))
                            @foreach ($attr->attribute_options as $key => $option)
                            <li class="wc-layered-nav-term ">
                                <input class="attribute-input" name="{{$attr->input_name}}[]" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}" type="checkbox">
                                <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                            </li>
                            @endforeach
                            @endif  

                        </ul>


            @endforeach
        @endif



        @if (!empty($subcat) && !empty(json_decode($subcat->attributes, true)))
            @foreach ($subcat->attributes as $key => $attr)
                    
                        <span class="gamma widget-title">{{$attr->name}}</span>
                        <ul>
                            @if (!empty($attr->attribute_options))
                            @foreach ($attr->attribute_options as $key => $option)
                            <li class="wc-layered-nav-term ">
                                <input class="form-check-input attribute-input" name="{{$attr->input_name}}[]" id="{{$attr->input_name}}{{$option->id}}" value="{{$option->name}}" type="checkbox">
                                <label class="form-check-label" for="{{$attr->input_name}}{{$option->id}}">{{$option->name}}</label>
                            </li>
                            @endforeach
                            @endif  

                        </ul>

            @endforeach
        @endif


        </form>
        @endif

        </div>
    </div>


    <div class="widget widget_techmarket_products_carousel_widget">
    <section id="single-sidebar-carousel" class="section-products-carousel">
        <header class="section-header">
            <h2 class="section-title">Latest Products</h2>
            <nav class="custom-slick-nav"></nav>
        </header>
        <!-- .section-header -->
        <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;rows&quot;:2,&quot;slidesPerRow&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#single-sidebar-carousel .custom-slick-nav&quot;}">
            <div class="container-fluid">
                <div class="woocommerce columns-1">
                    <div class="products">

                        @php $i=0; 
                            $products = \App\models\Product::query()->where("status",1)->where("latest",1)->get();
                        @endphp
                        @foreach($products as $key => $prod)
                        @if($prod->latest == 1)
                        @if($i < 3)

                        <div class="landscape-product-widget product">
                            <a class="woocommerce-LoopProduct-link" href="{{ route('front.product',$prod->slug) }}" tabindex="-1">
                                <div class="media">
                                <img class="wp-post-image" src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}" alt="">
                                        <div class="media-body">
                                            <span class="price">
                                                <ins>
                                                    <span class="amount"> {{ $prod->setCurrency() }}</span>
                                                </ins>
                                                <!-- <del>
                                                    <span class="amount">26.99</span>
                                                </del> -->
                                            </span>
                                            <!-- .price -->
                                            <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                            <div class="techmarket-product-rating">
                                                <div title="Rated 0 out of 5" class="star-rating">
                                                    <span style="width:0%">
                                                        <strong class="rating">0</strong> out of 5</span>
                                                </div>
                                                <span class="review-count">(0)</span>
                                            </div>
                                            <!-- .techmarket-product-rating -->
                                        </div>
                                        <!-- .media-body -->
                                </div>
                                <!-- .media -->
                            </a>
                            <!-- .woocommerce-LoopProduct-link -->
                        </div>
                        @php $i++; @endphp
                        @endif
                        @endif
                        @endforeach

                    </div>
                    <!-- .products -->
                </div>
                <!-- .woocommerce -->
            </div>
            <!-- .container-fluid -->
        </div>
        <!-- .products-carousel -->
    </section>
    <!-- .section-products-carousel -->
</div>

</div>