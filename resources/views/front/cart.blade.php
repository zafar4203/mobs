
@extends('layouts.front')
@section('styles')
<style>
    table.cart td.product-name .cart-item-product-detail {
        font-size: .875em;
        line-height: 1.5em;
        letter-spacing: -.01em;
        font-weight: 400;
        color: #626060;
        padding-left: 10px;
    }
</style>       
@endsection
@section('class','page-template-default')
@section('content')
<div class="col-full">
    <div class="row">
        <nav class="woocommerce-breadcrumb">
            <a href="home-v1.html">Home</a>
            <span class="delimiter">
                <i class="tm tm-breadcrumbs-arrow-right"></i>
            </span>
            Cart
        </nav>
        <!-- .woocommerce-breadcrumb -->
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="type-page hentry">
                    <div class="entry-content">
                        <div class="woocommerce">
                            <div class="cart-wrapper">
                                <form method="post" action="#" class="woocommerce-cart-form">
                                    @php $discount = 0; @endphp
                                    <table class="shop_table shop_table_responsive cart">
                                        <thead>
                                            <tr>
                                                <th class="product-remove">&nbsp;</th>
                                                <th class="product-thumbnail">&nbsp;</th>
                                                <th class="product-name">Product</th>
                                                <th class="product-price">Price</th>
                                                <th class="product-quantity">Quantity</th>
                                                <th class="product-subtotal">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                        @php
                                            $discount = $discount + App\Models\Product::find($product['item']['id'])->dsc_amt;
                                        @endphp

                                        <tr class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}">
                                                <td class="product-remove">
                                                    <a class="remove" href="#">×</a>
                                                </td>
                                                <td class="product-thumbnail">
                                                    <a href="single-product-fullwidth.html">
                                                        <img width="180" height="180" alt="" class="wp-post-image" src="{{ $product['item']['photo'] ? asset('public/assets/images/products/'.$product['item']['photo']):asset('public/assets/images/noimage.png') }}">
                                                    </a>
                                                </td>
                                                <td data-title="Product" class="product-name">
                                                    <div class="media cart-item-product-detail">
                                                        <a href="single-product-fullwidth.html">
                                                            <img width="180" height="180" alt="" class="wp-post-image" src="{{ $product['item']['photo'] ? asset('public/assets/images/products/'.$product['item']['photo']):asset('public/assets/images/noimage.png') }}">
                                                        </a>
                                                        <div class="media-body align-self-center">
                                                            <a href="{{ route('front.product', $product['item']['slug']) }}">{{mb_strlen($product['item']['name'],'utf-8') > 35 ? mb_substr($product['item']['name'],0,35,'utf-8').'...' : $product['item']['name']}}</a>
                                                            <br>                                     
                                                        @if(!empty($product['size']))
                                                        <b>{{ $langg->lang312 }}</b>: {{ $product['item']['measure'] }}{{str_replace('-',' ',$product['size'])}} <br>
                                                        @endif
                                                        @if(!empty($product['color']))
                                                        <div class="mt-1 mb-1">
                                                        <b>{{ $langg->lang313 }}</b>:   &nbsp;<span id="color-bar" style="padding-right:20px; border: 5px solid #{{$product['color'] == "" ? "white" : $product['color']}}; background:#{{$product['color'] == "" ? "white" : $product['color']}};"></span>
                                                        </div>
                                                        @endif

                                                        @if(!empty($product['keys']))
                                                        @foreach( array_combine(explode(',', $product['keys']), explode(',', $product['values']))  as $key => $value)

                                                            <b>{{ ucwords(str_replace('_', ' ', $key))  }} : </b> {{ $value }} <br>
                                                        @endforeach
                                                        @endif

                                                        </div>
                                                    </div>
                                                </td>
                                                <td data-title="Price" class="product-price">
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{ App\Models\Product::convertPrice($product['item']['price']) }}
                                                    </span>
                                                </td>
                                                <td class="product-quantity" data-title="Quantity">
                                                    <div class="quantity">
                                                    @if($product['item']['type'] == 'Physical')
                                                    <input type="hidden" class="prodid" value="{{$product['item']['id']}}">  
                                                    <input type="hidden" class="itemid" value="{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">     
                                                    <input type="hidden" class="size_qty" value="{{$product['size_qty']}}">     
                                                    <input type="hidden" class="size_price" value="{{$product['item']['price']}}">   
                                                    <div class="plus-minus">
                                                            <div class="dec qtybutton reducing" style="display: inline-block;">
                                                                <span class="qtybtns btn-sm btn btn-default"><i class="fa fa-minus"></i></span>                                    
                                                            </div>
                                                            <span class="qttotal1" id="qty{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">{{ $product['qty'] }}</span>
                                                            <div class="inc qtybutton adding" style="display: inline-block;">
                                                                <span class="qtybtns btn btn-sm btn-default"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                    </div>
                                                    @endif
                                                    </div>
                                                </td>
                                                <td data-title="Total" class="product-subtotal">
                                                    @if($product['size_qty'])
                                                    <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="{{$product['size_qty']}}">
                                                    @elseif($product['item']['type'] != 'Physical') 
                                                    <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="1">
                                                    @else
                                                    <input type="hidden" id="stock{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" value="{{$product['stock']}}">
                                                    @endif
                        
                                                    <span id="prc{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}">
                                                        {{ App\Models\Product::convertPrice($product['price']) }}                 
                                                    </span>
                    
                                                    <a class="removecart cart-remove" data-class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}" data-href="{{ route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])) }}" href="javascript:;" title="Remove this item" class="remove">×</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td class="actions" colspan="6">
                                                    <div class="coupon">
                                                    <input type="hidden" class="coupon-total" id="grandtotal" value="{{ Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00' }}">
                                                    <input type="hidden" id="coupon_val" value="0">     
                                                    <!-- <form  class="coupon"> -->
                                                        <label for="coupon_code">Coupon:</label>
                                                        <input type="text" placeholder="Coupon code" value="" id="code" class="input-text" name="coupon_code">
                                                        <input type="button" value="Apply coupon" name="apply_coupon" class="coupon-btn coupon-submit button">
                                                    <!-- </form> -->
                                                    </div>
                                                    <!-- <input type="submit" value="Update cart" name="update_cart" class="button"> -->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- .shop_table shop_table_responsive -->
                                </form>
                                <!-- .woocommerce-cart-form -->
                                <div class="cart-collaterals">
                                    <div class="cart_totals">
                                        <h2>Cart totals</h2>
                                        <table class="shop_table shop_table_responsive">
                                            <tbody>
                                            <input type="hidden" id="d-val" value="{{ App\Models\Product::convertPrice($discount)}}">
                                            <input type="hidden" id="c-val" value="{{ $curr->sign }}">
                                            <input type="hidden" id="c-sign-length" value="{{ mb_strlen($curr->sign) }}">

                                            <input type="hidden" class="coupon-total" id="grandtotal" value="{{ Session::has('cart') ? App\Models\Product::convertPrice($mainTotal) : '0.00' }}">
                                            <input type="hidden" id="coupon_val" value="0">

                                                <tr class="cart-subtotal">
                                                    <th>Subtotal</th>
                                                    <td data-title="Subtotal">
                                                        <span class="woocommerce-Price-amount amount  cart-total">
                                                            {{ Session::has('cart') ? App\Models\Product::convertPrice($totalPrice) : '0.00' }}
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr class="cart-subtotal">
                                                    <th>Tax <small>({{$gs->tax}}%)</small></th>
                                                    @php 
                                                    if($gs->tax != 0)
                                                        {
                                                            $tax = ($totalPrice / 100) * $gs->tax;
                                                            $mainTotal = $totalPrice + $tax;
                                                        }
                                                    @endphp
                                                    <td data-title="Tax"><span id="tax-total" class="amount">{{ App\Models\Product::convertPrice($tax)}}</span></td>
                                                </tr>

                                                <tr class="cart-subtotal">
                                                    <th>Discount</th>
                                                    <td data-title="Discount"><span class="woocommerce-Price-amount amount discount">{{ App\Models\Product::convertPrice($discount)}}</span></td>
                                                </tr>
                                                <tr class="order-total">
                                                    <th>Total</th>
                                                    <td data-title="Total">
                                                        <strong>
                                                            <span class="woocommerce-Price-amount amount main-total">
                                                                @if(Session::has('cart')) {{$curr->sign}} @endif {{ Session::has('cart') ?  App\Models\Product::convertOnlyPrice($mainTotal) - App\Models\Product::convertOnlyPrice($discount)  : '0.00' }}                                    
                                                            </span>
                                                        </strong>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- .shop_table shop_table_responsive -->
                                        <div class="wc-proceed-to-checkout">

                                            <!-- .wc-proceed-to-checkout -->
                                            <a class="checkout-button button alt wc-forward" href="{{ route('front.checkout') }}">
Proceed to checkout</a>
                                            <a class="back-to-shopping" href="{{ url('/') }}">Back to Shopping</a>
                                        </div>
                                        <!-- .wc-proceed-to-checkout -->
                                    </div>
                                    <!-- .cart_totals -->
                                </div>
                                <!-- .cart-collaterals -->
                            </div>
                            <!-- .cart-wrapper -->
                        </div>
                        <!-- .woocommerce -->
                    </div>
                    <!-- .entry-content -->
                </div>
                <!-- .hentry -->
            </main>
            <!-- #main -->
        </div>
        <!-- #primary -->
    </div>
    <!-- .row -->
</div>
        @endsection