@extends('layouts.front')
@section('styles')
<style>
.form-check-label {
    font-size: 15px;
    text-transform: capitalize;
    color: #444444;
    padding-bottom: 0.2em;
    font-weight:normal !important;
    margin-bottom:0.333em;
}
input[type=checkbox]
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  transform: scale(1.5);
  padding: 8px;
  color:#d5d5d5;
}
.disabled {
    pointer-events: none;
}

.page-numbers {
    font-size: 1em;
    display: inline-block;
    padding: 0.667em 1.133em;
    color: #444444;
    font-weight: 500;
    line-height: 1.543em;
}
.current {
    background-color: #0063d1;
    color: #fff;
    padding: 0.733em 1.4em;
    border-radius: 5px;
}


#secondary .widget_product_categories ul.product-categories li.product_cat ul.show-all-cat li.product_cat span {
    font-size: 1.071em;
    font-weight: normal;
    padding:0.3em 0.5em 0.3em 1.3em !important;
    border-bottom:0px;
}

#sortby option {
    color:black;
}
</style>
@endsection

@section('content')

@php 
  if (Session::has('currency')) {
      $curr = \App\Models\Currency::find(Session::get('currency'));
  }
  else {
      $curr = \App\Models\Currency::where('is_default','=',1)->first();
  }
@endphp
<input type="hidden" id="curr" value="{{ $curr->sign }}">

<input type="hidden" id="curr" value="{{ $curr->sign }}">
<div class="col-full">
                    <div class="row">
                        <nav class="woocommerce-breadcrumb">
                        <a href="{{ route('front.index') }}">Home</a> 
                        @if(!empty($cat))
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                            <a href="{{ route('front.category' , [$cat->slug]) }}">{{ $cat->name }}</a>                            
                            @else
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                                Category
                            @endif

                            @if(!empty($subcat))
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                            <a href="{{ route('front.category' , [$cat->slug , $subcat->slug]) }}">{{ $subcat->name }}</a>
                            @endif

                            @if(!empty($childcat)) 
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                            <a href="{{ route('front.category' , [$cat->slug , $subcat->slug, $childcat->slug]) }}">{{ $childcat->name }}</a> 
                            @endif
                        </nav>
                        <!-- .woocommerce-breadcrumb -->
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                @include('includes.filter')
                                <div class="tab-content">
                                    <div id="grid" class="tab-pane active" role="tabpanel" aria-expanded="true">
                                        <div class="woocommerce columns-4">
                                            <div class="products" id="ajaxContent">
                                                @include('includes.product.filtered-products')
                                            </div>
                                            <!-- .products -->
                                        </div>
                                        <!-- .woocommerce -->
                                    </div>
                                    <!-- .tab-pane -->
                                </div>
                                <!-- .tab-content -->
                            </main>
                            <!-- #main -->
                        </div>
                        <!-- #primary -->
                        @include('includes.catalog')
                    </div>
                    <!-- .row -->
                </div>
@endsection
@section('scripts')
<script>

  $(document).ready(function() {
    var categories = [];
    @php
    $caties = [];
    if(isset($_GET['categories'])){
     $caties = [];
     $caties = explode(',', $_GET['categories']);     
    }
    @endphp
    categories = @php echo json_encode($caties); @endphp;
    addToPagination();
    // when dynamic attribute changes
    $(".attribute-input, #sortby").on('change', function() {
      $("#ajaxLoader").show();
      filter();
    });

    $('input[name="cat[]"]').on('change', function (e) {
        e.preventDefault();
        categories = [];
        $('input[name="cat[]"]:checked').each(function(){
            categories.push($(this).val());
        });
        $("#ajaxLoader").show();
        filter();
    });

    // when price changed & clicked in search button
    $(".filter-btn").on('click', function(e) {
      e.preventDefault();
      $("#ajaxLoader").show();
      filter();
    });

  function filter() {
    let filterlink = '';

    if ($("#prod_name").val() != '' && $("#prod_name").val() != undefined) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?search='+$("#prod_name").val();
      } else {
        filterlink += '&search='+$("#prod_name").val();
      }
    }

    if (categories.length > 0) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+'categories='+categories;
      } else {
        filterlink += '&categories='+categories;
      }
    }

    $(".attribute-input").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });

    if ($("#sortby").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#sortby").attr('name')+'='+$("#sortby").val();
      } else {
        filterlink += '&'+$("#sortby").attr('name')+'='+$("#sortby").val();
      }
    }

    if ($("#minamount").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#minamount").attr('name')+'='+$("#minamount").val();
      } else {
        filterlink += '&'+$("#minamount").attr('name')+'='+$("#minamount").val();
      }
    }

    if ($("#maxamount").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      } else {
        filterlink += '&'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      }
    }

    // console.log(filterlink);
    console.log(encodeURI(filterlink));
    $("#ajaxContent").load(encodeURI(filterlink), function(data) {
      // add query string to pagination
      addToPagination();
      $("#ajaxLoader").fadeOut(1000);
      $('#show-total-pp').html($('#total-pp').val());  
    });
  }

  // append parameters to pagination links
  function addToPagination() {
    // add to attributes in pagination links
    $('.pagination-url').each(function() {
      let url = $(this).attr('href');
      let queryString = '?' + url.split('?')[1]; // "?page=1234...."

      let urlParams = new URLSearchParams(queryString);
      let page = urlParams.get('page'); // value of 'page' parameter
      let fullUrl = '{{route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')])}}?page='+page+'&search='+'{{request()->input('search')}}';

      $(".attribute-input").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });

      if ($("#sortby").val() != '') {
        fullUrl += '&sort='+encodeURI($("#sortby").val());
      }

      if ($("#minamount").val() != '') {
        fullUrl += '&min='+encodeURI($("#minamount").val());
      }

      if ($("#maxamount").val() != '') {
        fullUrl += '&max='+encodeURI($("#maxamount").val());
      }

    if (categories.length > 0) {
        fullUrl += '&categories='+categories;
    }

      $(this).attr('href', fullUrl);
    });
  }

  $(document).on('click', '.categori-item-area .pagination li a', function (event) {
    event.preventDefault();
    if ($(this).attr('href') != '#' && $(this).attr('href')) {
      $('#preloader').show();
      $('#ajaxContent').load($(this).attr('href'), function (response, status, xhr) {
        if (status == "success") {
          $('#preloader').fadeOut();
          $("html,body").animate({
            scrollTop: 0
          }, 1);

          addToPagination();
        }
      });
    }
  });
});

</script>
@endsection