@extends('layouts.front')
@section('title' , 'Login')
@section('styles')
 
@endsection
@section('class','page-template-default  pace-done')
@section('content')
<div class="col-full">
<div class="row">
    <nav class="woocommerce-breadcrumb">
        <a href="{{ route('front.index') }}">Home</a>
        <span class="delimiter">
            <i class="tm tm-breadcrumbs-arrow-right"></i>
        </span>Login
    </nav>
    <!-- .woocommerce-breadcrumb -->
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="type-page hentry">
                <div class="entry-content">
                    <div class="woocommerce">
                        <div class="">
                            <!-- <span class="or-text">or</span> -->
                            <div id="#" class="u-columns col2-set">
                                <div class="mx-auto col-md-6 signin-form">
                                    <h2>Login</h2>
                                    @include('includes.admin.form-login')
                                    <form class="signin-form mloginform woocomerce-form woocommerce-form-login login" action="{{ route('user.login.submit') }}" method="post">
                                    @csrf
                                        <p class="form-row form-row-wide">
                                            <label for="username">Email address
                                                <span class="required">*</span>
                                            </label>
                                            <input type="email" class="input-text" name="email" placeholder="Enter Email Address" id="email" required>
                                        </p>
                                        <p class="form-row form-row-wide">
                                            <label for="password">Password
                                                <span class="required">*</span>
                                            </label>
                                            <input class="input-text" type="password" name="password" id="password" placeholder="Enter Password" required>
                                        </p>
                                        <p class="form-row">
                                            <input type="hidden" name="modal" value="1">
                                            <input class="mauthdata" type="hidden" value="{{ $langg->lang177 }}">     
                                            <input class="woocommerce-Button button" type="submit" value="Login" name="login">
                                            <label for="rememberme" class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                                <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever"> Remember me
                                            </label>
                                        </p>
                                        <p class="woocommerce-LostPassword lost_password">
                                            <a href="{{ route('user-forgot') }}">Lost your password?</a>
                                            <a class="pull-right" href="{{ route('user-register') }}">Register an account</a>
                                        </p>
                                    </form>
                                    <!-- .woocommerce-form-login -->
                                </div>
                            </div>
                            <!-- .col2-set -->
                        </div>
                        <!-- .customer-login-form -->
                    </div>
                    <!-- .woocommerce -->
                </div>
                <!-- .entry-content -->
            </div>
            <!-- .hentry -->
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->
</div>
<!-- .row -->
</div>
@endsection