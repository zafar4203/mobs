@extends('layouts.front')
@section('title' , 'Login')
@section('styles')
 
@endsection
@section('class','page-template-default  pace-done')
@section('content')
<div class="col-full">
<div class="row">
    <nav class="woocommerce-breadcrumb">
        <a href="{{ route('front.index') }}">Home</a>
        <span class="delimiter">
            <i class="tm tm-breadcrumbs-arrow-right"></i>
        </span>Register
    </nav>
    <!-- .woocommerce-breadcrumb -->
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="type-page hentry">
                <div class="entry-content">
                    <div class="woocommerce">
                        <div class="">
                            <!-- <span class="or-text">or</span> -->
                            <div id="#" class="u-columns col2-set">
                                <div class="mx-auto col-md-6">
                                    <h2>Register</h2>
                                    <form method="post" class="woocomerce-form woocommerce-form-login login">
                                        <p class="form-row form-row-wide">
                                            <label for="username">Name
                                                <span class="required">*</span>
                                            </label>
                                            <input type="text" class="" name="name" id="username" placeholder="Enter Your Name" value="{{ old('name') }}" required>
                                        </p>
                                        <p class="form-row form-row-wide">
                                            <label for="password">Email
                                                <span class="required">*</span>
                                            </label>
                                            <input type="email" name="email" class="form-control" placeholder="Enter Email Address" value="{{ old('email') }}" required>
                                        </p>
                                        <p class="form-row form-row-wide">
                                            <label for="password">Phone
                                                <span class="required">*</span>
                                            </label>
                                            <input type="number" name="phone" class="form-control" placeholder="Enter Phone Number" value="{{ old('phone') }}" required>
                                        </p>
                                        <p class="form-row form-row-wide">
                                            <label for="password">Password
                                                <span class="required">*</span>
                                            </label>
                                            <input type="password" name="password" class="form-control" placeholder="Enter Password" value="{{ old('password') }}" required>
                                        </p>
                                        <p class="form-row form-row-wide">
                                            <label for="password">Confirm Password
                                                <span class="required">*</span>
                                            </label>
                                            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" id="confirmPassword" required>
                                        </p>
                                        <p class="form-row">
                                            <input class="woocommerce-Button button" type="submit" value="Register">
                                        </p>
                                        <p class="woocommerce-LostPassword lost_password">
                                            <a href="{{route('user.login')}}">Already Have an Account?</a>
                                        </p>
                                    </form>
                                    <!-- .woocommerce-form-login -->
                                </div>
                            </div>
                            <!-- .col2-set -->
                        </div>
                        <!-- .customer-login-form -->
                    </div>
                    <!-- .woocommerce -->
                </div>
                <!-- .entry-content -->
            </div>
            <!-- .hentry -->
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->
</div>
<!-- .row -->
</div>
@endsection