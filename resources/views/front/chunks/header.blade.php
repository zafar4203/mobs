<div class="top-bar top-bar-v1">
<!-- <div class="col-full">
    <ul id="menu-top-bar-left" class="nav justify-content-center">
        <li class="menu-item animate-dropdown">
            <a title="TechMarket eCommerce - Always free delivery" href="contact-v1.html">TechMarket eCommerce &#8211; Always free delivery</a>
        </li>
        <li class="menu-item animate-dropdown">
            <a title="Quality Guarantee of products" href="shop.html">Quality Guarantee of products</a>
        </li>
        <li class="menu-item animate-dropdown">
            <a title="Fast returnings program" href="track-your-order.html">Fast returnings program</a>
        </li>
        <li class="menu-item animate-dropdown">
            <a title="No additional fees" href="contact-v2.html">No additional fees</a>
        </li>
    </ul>
</div> -->
<!-- .col-full -->
</div>
<!-- .top-bar-v1 -->
<header id="masthead" class="site-header header-v1" style="background-image: none; ">
                <div class="col-full desktop-only">
                    <div class="sticky-wrapper" style=""><div class="techmarket-sticky-wrap">
                        <div class="row">
                            <div class="site-branding">
                                <a href="{{ route('front.index') }}" class="custom-logo-link" rel="home">
                                    <img style="height:60px;" src="{{asset('assets/images/'.$gs->logo)}}" class="img-responsive" alt="logo-image">
                                </a>
                                <!-- /.custom-logo-link -->
                            </div>
                            <!-- /.site-branding -->
                            <!-- ============================================================= End Header Logo ============================================================= -->
                            <nav id="primary-navigation" class="vertical-center primary-navigation" aria-label="Primary Navigation" data-nav="flex-menu">
                                <ul id="menu-primary-menu" class="nav yamm">
                                    <li class="sale-clr yamm-fw menu-item animate-dropdown">
                                        <a title="Super deals" href="{{ route('front.offers') }}">Super deals</a>
                                    </li>

                                    @foreach($categories as $cat)
                                    @if($cat->top_menu == 1)
                                    <li class="{{ count($cat->subs) > 0?'menu-item menu-item-has-children animate-dropdown dropdown':'menu-item animate-dropdown'}}">                                        
                                        <a {{count($cat->subs) > 0?  "data-toggle=dropdown class=dropdown-toggle aria-haspopup=true":''}} title="{{ $cat->name }}" href="{{ route('front.category',$cat->slug) }}">{{ $cat->name }} @if(count($cat->subs) > 0) <span class="caret"></span> @endif</a>
                                        <!-- Home Version Dropdown Start -->
                                        @if(count($cat->subs) > 0)
                                        <ul role="menu" class=" dropdown-menu">
                                            @foreach($cat->subs as $sub)
                                            <li>
                                                <a href="{{ route('front.category',[$cat->slug , $sub->slug]) }}">{{ $sub->name}}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    @endif
                                    @endforeach

                                    @foreach($pages as $pg)
                                    @if($pg->header == 1)
                                    <li class="menu-item animate-dropdown">
                                        <a href="{{ route('front.page',$pg->slug) }}">
                                            {{ $pg->title }}
                                        </a>
                                    </li>
                                    @endif
                                    @endforeach                                    
                                    <li class="techmarket-flex-more-menu-item dropdown" style="display: none;">
                                        <a title="..." href="#" data-toggle="dropdown" class="dropdown-toggle">...</a>
                                        <ul class="overflow-items dropdown-menu"></ul>
                                        <!-- . -->
                                    </li>
                                </ul>
                                <!-- .nav -->
                            </nav>
                            <!-- .primary-navigation -->
                            <nav id="secondary-navigation" class="secondary-navigation" aria-label="Secondary Navigation" data-nav="flex-menu" style="">
                                <ul id="menu-secondary-menu" class="nav">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2802 animate-dropdown">
                                        <a title="Track Your Order" href="track-your-order.html">
                                            <i class="tm tm-order-tracking"></i>Track Your Order</a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-487 animate-dropdown dropdown">
                                        <a title="Dollar (US)" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">
                                            <i class="tm tm-dollar"></i>Dollar (US)
                                            <span class="caret"></span>
                                        </a>
                                        <ul role="menu" class=" dropdown-menu">
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-489 animate-dropdown">
                                                <a title="AUD" href="#">AUD</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-490 animate-dropdown">
                                                <a title="INR" href="#">INR</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-491 animate-dropdown">
                                                <a title="AED" href="#">AED</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-492 animate-dropdown">
                                                <a title="SGD" href="#">SGD</a>
                                            </li>
                                        </ul>
                                        <!-- .dropdown-menu -->
                                    </li>
                                    
                                    <li class="menu-item">
                                        <a title="My Account" href="login-and-register.html">
                                            <i class="tm tm-login-register"></i>Register or Sign in</a>
                                    </li><li class="techmarket-flex-more-menu-item dropdown" style="display: none;">
                                        <a title="..." href="#" data-toggle="dropdown" class="dropdown-toggle">...</a>
                                        <ul class="overflow-items dropdown-menu"></ul>
                                    </li>
                                </ul>
                                <!-- .nav -->
                            </nav>
                            <!-- .secondary-navigation -->
                        </div>
                        <!-- /.row -->
                    </div></div>
                    <!-- .techmarket-sticky-wrap -->
                    <div class="row align-items-center">
                        <div id="departments-menu" class="dropdown departments-menu">
                            <button class="btn dropdown-toggle btn-block" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="tm tm-departments-thin"></i>
                                <span>All Departments</span>
                            </button>
                            <ul id="menu-departments-menu" class="dropdown-menu yamm departments-menu-dropdown" style="">
                                <!-- <li class="highlight menu-item animate-dropdown">
                                    <a title="Top 100 Offers" href="home-v3.html">Offers</a>
                                </li>                                 -->
                                <!-- <li class="highlight menu-item animate-dropdown">
                                    <a title="New Arrivals" href="home-v4.html">New Arrivals</a>
                                </li> -->

                                @foreach($categories as $cat)
                                @if($cat->is_featured == 1)
                                <li class="{{count($cat->subs) > 0 ? 'yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu':'menu-item menu-item-type-custom animate-dropdown'}}">
                                    @php 
                                        $ch = 0;
                                        if(count($cat->subs) > 0)
                                        foreach($cat->subs as $sub){
                                            if(count($sub->childs) > 0){
                                                $ch = 1;
                                            }
                                        }
                                    @endphp
                                    <a title="{{ $cat->name }}" {{ $ch == 1? 'data-toggle=dropdown class=dropdown-toggle aria-haspopup=true':''}} href="{{ route('front.category',$cat->slug) }}"> {{ $cat->name }} <span class="caret"></span></a>
                                    <ul role="menu" class=" dropdown-menu">
                                        <li class="menu-item menu-item-object-static_block animate-dropdown">
                                            <div class="yamm-content">
                                                <!-- .bg-yamm-content -->
                                                <div class="row yamm-content-row">
                                                    @if(count($cat->subs) > 0)
                                                    @foreach($cat->subs as $sub)
                                                    @if(count($sub->childs) > 0)
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="kc-col-container">
                                                            <div class="kc_text_block">
                                                                <ul>
                                                                    <li class="nav-title"><a href="{{ route('front.category' , [$cat->slug , $sub->slug]) }}">{{ $sub->name }}</a></li>
                                                                    @if(count($cat->subs) > 0)
                                                                    @foreach($sub->childs as $child)
                                                                    <li><a href="{{ route('front.category' , [$cat->slug , $sub->slug , $child->slug]) }}">{{ $child->name }}</a></li>
                                                                    @endforeach
                                                                    @endif
                                                                    <!-- <li>
                                                                        <a href="{{ route('front.product' , [$cat->slug , $sub->slug]) }}">
                                                                            <span class="nav-text">{{ $cat->name }}</span>
                                                                            <span class="nav-subtext">Discover more products</span>
                                                                        </a>
                                                                    </li> -->
                                                                </ul>
                                                            </div>
                                                            <!-- .kc_text_block -->
                                                        </div>
                                                        <!-- .kc-col-container -->
                                                    </div>
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                </div>
                                                <!-- .kc_row -->
                                            </div>
                                            <!-- .yamm-content -->
                                        </li>
                                    </ul>
                                </li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                        <!-- .departments-menu -->
                        <form id="searchForm" class="navbar-search" action="{{ route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')]) }}" method="GET">
                            @if (!empty(request()->input('sort')))
                            <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
                            @endif
                            @if (!empty(request()->input('minprice')))
                            <input type="hidden" name="minprice" value="{{ request()->input('minprice') }}">
                            @endif
                            @if (!empty(request()->input('maxprice')))
                            <input type="hidden" name="maxprice" value="{{ request()->input('maxprice') }}">
                            @endif

                            <label class="sr-only screen-reader-text" for="search">Search for:</label>
                            <div class="input-group">
                                <input type="text" id="prod_name" name="search" class="form-control search-field product-search-field" dir="ltr" value="" placeholder="Search for products">
                                <div class="input-group-addon search-categories">
                                    <select name="category" id="category_select" class="postform resizeselect" style="width: 157.875px;">
                                        <option value="" selected="selected">All Categories</option>
                                        @foreach($categories as $cat)
                                        <option class="level-0" value="{{$cat->slug}}" {{ Request::route('category') == $cat->slug ? 'selected' : '' }}>{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- .input-group-addon -->
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-search"></i>
                                        <span class="search-btn">Search</span>
                                    </button>
                                </div>
                                <!-- .input-group-btn -->
                            </div>
                            <!-- .input-group -->
                        </form>
                        <!-- .navbar-search -->
                        <ul class="header-compare nav navbar-nav">
                            <li class="nav-item">
                                <a href="{{ route('product.compare') }}" class="nav-link">
                                    <i class="tm tm-compare"></i>
                                    <span id="compare-count" class="value">{{ Session::has('compare') ? count(Session::get('compare')->items) : '0' }}</span>
                                </a>
                            </li>
                        </ul>
                        <!-- .header-compare -->
                        <ul class="header-wishlist nav navbar-nav">
                            <li class="nav-item">
                                <a href="{{ route('user-wishlists') }}" class="nav-link">
                                    <i class="tm tm-favorites"></i>
                                    <span id="wishlist-count" class="value">@if(Auth::user()) {{ Auth::user()->wishlistCount() }} @else 0 @endif</span>
                                </a>
                            </li>
                        </ul>
                        <!-- .header-wishlist -->
                        <ul id="site-header-cart" class="site-header-cart menu">
                            <li class="animate-dropdown dropdown ">
                                <a class="cart-contents" href="cart.html" data-toggle="dropdown" title="View your shopping cart">
                                    <i class="tm tm-shopping-bag"></i>
                                    <span class="count" id="cart-count">{{ Session::has('cart') ? Session::get('cart')->totalCount() : '0' }}</span>
                                    <span class="amount">
                                        <span class="price-label">Your Cart</span><span id="cart-paisa">{{ Session::has('cart') ? App\Models\Product::convertPrice(Session::get('cart')->totalPrice) : '0.00' }}</span></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-mini-cart" id="cart-items">
                                    @include('load.cart')
                                </ul>
                                <!-- .dropdown-menu-mini-cart -->
                            </li>
                        </ul>
                        <!-- .site-header-cart -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- .col-full -->
                <div class="col-full handheld-only">
                    <div class="handheld-header">
                        <div class="row">
                            <div class="site-branding">
                                <a href="{{ route('front.index') }}" class="custom-logo-link" rel="home">
                                    <img style="height:40px;" src="{{asset('assets/images/'.$gs->logo)}}" class="img-responsive" alt="logo-image">
                                </a>
                                <!-- /.custom-logo-link -->
                            </div>
                            <!-- /.site-branding -->
                            <!-- ============================================================= End Header Logo ============================================================= -->
                            <div class="handheld-header-links">
                                <ul class="columns-3">
                                    <li class="my-account">
                                        <a href="{{ route('user-dashboard') }}" class="has-icon">
                                            <i class="tm tm-login-register"></i>
                                        </a>
                                    </li>
                                    <li class="wishlist">
                                        <a href="{{ route('user-wishlists') }}" class="has-icon">
                                            <i class="tm tm-favorites"></i>
                                            <span id="wishlist-count" class="count">@if(Auth::user()) {{ Auth::user()->wishlistCount() }} @else 0 @endif</span>
                                        </a>
                                    </li>
                                    <li class="compare">
                                        <a href="{{ route('product.compare') }}" class="has-icon">
                                            <i class="tm tm-compare"></i>
                                            <span id="compare-count" class="count">{{ Session::has('compare') ? count(Session::get('compare')->items) : '0' }}</span>
                                        </a>
                                    </li>
                                </ul>
                                <!-- .columns-3 -->
                            </div>
                            <!-- .handheld-header-links -->
                        </div>
                        <!-- /.row -->
                        <div class="sticky-wrapper"><div class="techmarket-sticky-wrap">
                            <div class="row">
                                <nav id="handheld-navigation" class="handheld-navigation" aria-label="Handheld Navigation">
                                    <button class="btn navbar-toggler" type="button">
                                        <i class="tm tm-departments-thin"></i>
                                        <span>Menu</span>
                                    </button>
                                    <div class="handheld-navigation-menu">
                                        <span class="tmhm-close">Close</span>
<ul id="menu-departments-menu-1" class="nav">
    <li class="highlight menu-item animate-dropdown">
        <a title="Super deals" href="{{ route('front.offers') }}">Super deals</a>
    </li>

    @foreach($categories as $cat)
    @if($cat->is_featured == 1)
    <li class="{{ count($cat->subs) > 0?'yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu':'menu-item animate-dropdown'}}">                                        
        <a {{count($cat->subs) > 0?  "data-toggle=dropdown class=dropdown-toggle aria-haspopup=true":''}} title="{{ $cat->name }}" href="{{ route('front.category',$cat->slug) }}">{{ $cat->name }} @if(count($cat->subs) > 0) <span class="caret"></span> @endif</a>
        <!-- Home Version Dropdown Start -->
        @if(count($cat->subs) > 0)
        <ul role="menu" class=" dropdown-menu">        
        <li class="menu-item menu-item-object-static_block animate-dropdown">
                <div class="yamm-content">
                    <!-- .bg-yamm-content -->
                    <div class="row yamm-content-row">
                    @foreach($cat->subs as $sub)
                        <div class="col-md-6 col-sm-12">
                            <div class="kc-col-container">
                                <div class="kc_text_block">
                                    <ul>
                                        <li class="{{ count($sub->childs) > 0 ? 'nav-title':'' }}">
                                            <a href="{{ route('front.category',[$cat->slug , $sub->slug]) }}">{{ $sub->name}}</a>
                                        </li>
                                        @if(count($sub->childs) > 0)
                                        @foreach($sub->childs as $child)
                                        <li>
                                            <a href="{{ route('front.category',[$cat->slug , $sub->slug, $child->slug]) }}">{{ $child->name}}</a>
                                        </li>
                                        @endforeach
                                        @endif

                                    </ul>
                                </div>
                                <!-- .kc_text_block -->
                            </div>
                            <!-- .kc-col-container -->
                        </div>
                        @endforeach
                    </div>
                    <!-- .kc-col-container -->
                </div>
            </li>

        </ul>
        @endif
    </li>
    @endif
    @endforeach

    @foreach($pages as $pg)
    @if($pg->header == 1)

    <li class="menu-item animate-dropdown">
        <a href="{{ route('front.page',$pg->slug) }}">
            {{ $pg->title }}
        </a>
    </li>

    @endif
    @endforeach                                    

    <li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown-submenu">
        <a title="Computers &amp; Laptops" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" href="#">Computers &amp; Laptops <span class="caret"></span></a>
        <ul role="menu" class=" dropdown-menu">
            <li class="menu-item menu-item-object-static_block animate-dropdown">
                <div class="yamm-content">
                    <!-- .bg-yamm-content -->
                    <div class="row yamm-content-row">
                        <div class="col-md-6 col-sm-12">
                            <div class="kc-col-container">
                                <div class="kc_text_block">
                                    <ul>
                                        <li class="nav-title">Computers &amp; Accessories</li>
                                        <li><a href="shop.html">All Computers &amp; Accessories</a></li>
                                        <li><a href="shop.html">Laptops, Desktops &amp; Monitors</a></li>
                                        <li><a href="shop.html">Pen Drives, Hard Drives &amp; Memory Cards</a></li>
                                        <li><a href="shop.html">Printers &amp; Ink</a></li>
                                        <li><a href="shop.html">Networking &amp; Internet Devices</a></li>
                                        <li><a href="shop.html">Computer Accessories</a></li>
                                        <li><a href="shop.html">Software</a></li>
                                        <li class="nav-divider"></li>
                                        <li>
                                            <a href="#">
                                                <span class="nav-text">All Electronics</span>
                                                <span class="nav-subtext">Discover more products</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- .kc_text_block -->
                            </div>
                            <!-- .kc-col-container -->
                        </div>
                        <!-- .kc_column -->
                        <div class="col-md-6 col-sm-12">
                            <div class="kc-col-container">
                                <div class="kc_text_block">
                                    <ul>
                                        <li class="nav-title">Office &amp; Stationery</li>
                                        <li><a href="shop.html">All Office &amp; Stationery</a></li>
                                        <li><a href="shop.html">Pens &amp; Writing</a></li>
                                    </ul>
                                </div>
                                <!-- .kc_text_block -->
                            </div>
                            <!-- .kc-col-container -->
                        </div>
                        <!-- .kc_column -->
                    </div>
                    <!-- .kc_row -->
                </div>
                <!-- .yamm-content -->
            </li>
        </ul>
                                            </li>
                                            <li class="menu-item animate-dropdown">
                                                <a title="Virtual Reality" href="shop.html">Virtual Reality</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- .handheld-navigation-menu -->
                                </nav>
                                <!-- .handheld-navigation -->
                                <div class="site-search">
                                    <div class="widget woocommerce widget_product_search">
                                        <form id="searchForm" class="woocommerce-product-search" action="{{ route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')]) }}" method="GET">
                                            @if (!empty(request()->input('sort')))
                                            <input type="hidden" name="sort" value="{{ request()->input('sort') }}">
                                            @endif
                                            @if (!empty(request()->input('minprice')))
                                            <input type="hidden" name="minprice" value="{{ request()->input('minprice') }}">
                                            @endif
                                            @if (!empty(request()->input('maxprice')))
                                            <input type="hidden" name="maxprice" value="{{ request()->input('maxprice') }}">
                                            @endif

                                            <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for:</label>
                                            <input type="search" id="prod_name" name="search" class="search-field" placeholder="Search products…" value="">
                                            <input type="submit" value="Search">
                                            <input type="hidden" name="post_type" value="product">
                                        </form>
                                    </div>
                                    <!-- .widget -->
                                </div>
                                <!-- .site-search -->
                                <a class="handheld-header-cart-link has-icon" href="{{ route('front.cart') }}" title="View your shopping cart">
                                    <i class="tm tm-shopping-bag"></i>
                                    <span class="count" id="cart-count">{{ Session::has('cart') ? Session::get('cart')->totalCount() : '0' }}</span>
                                </a>
                            </div>
                            <!-- /.row -->
                        </div></div>
                        <!-- .techmarket-sticky-wrap -->
                    </div>
                    <!-- .handheld-header -->
                </div>
                <!-- .handheld-only -->
            </header>