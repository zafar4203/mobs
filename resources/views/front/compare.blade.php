@extends('layouts.front')
@section('class','page home page-template-default  pace-done')
@section('content')


<div class="col-full">
	<div class="row">
		<nav class="woocommerce-breadcrumb">
			<a href="{{ route('front.index') }}">Home</a>
			<span class="delimiter">
				<i class="tm tm-breadcrumbs-arrow-right"></i>
			</span>Compare
		</nav>

		@if(isset($products))
		<!-- .woocommerce-breadcrumb -->
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<div class="type-page hentry">
					<div class="entry-content">
						<div class="table-responsive">
		<table class="table table-compare compare-list">
			<tbody>
				<tr>
					<th>Product</th>
					@foreach($products as $product)
					<td class="c{{$product['item']['id']}}">
						<a class="product" href="single-product-fullwidth.html">
							<div class="product-image">
								<div class="image">
									<img width="300" height="300" class="img-fluid attachment-shop_catalog size-shop_catalog wp-post-image" src="{{ $product['item']['thumbnail'] ? asset('public/assets/images/thumbnails/'.$product['item']['thumbnail']):asset('assets/images/noimage.png') }}" alt="Compare product['item']">
								</div>
							</div>

							<div class="product-info">
								<h3 class="product-title">{{ $product['item']['name'] }}<a href="{{ route('front.product', $product['item']['slug']) }}" class="title"></a></h3>
							</div>
						</a>
						<!-- /.product -->
					</td>
					@endforeach
				</tr>
				<tr>
					<th>Price</th>
					@foreach($products as $product)
					<td class="c{{$product['item']['id']}}">
						<div class="product-price price">
						@php
							$prod = \App\models\Product::query()->where("id",$product['item']['id'])->first();
						@endphp

						<ins>
							<span class="woocommerce-Price-amount amount">{{ $prod->showPrice() }}</span>
						</ins>
						<del>
							<span class="woocommerce-Price-amount amount">{{ $prod->showPreviousPrice() }}</span>
						</del>
						</div>
					</td>
					@endforeach
				</tr>
				<tr>
					<th>Availability</th>
					@foreach($products as $product)
					<td class="c{{$product['item']['id']}}">
						<span>In stock</span>
					</td>
					@endforeach
				</tr>
				<tr>
					<th>Description</th>
					@foreach($products as $product)
						<td class="c{{$product['item']['id']}}">
						@php
							$prod = \App\models\Product::query()->where("id",$product['item']['id'])->first();
						@endphp
							<p>{!! $prod->details !!}</p>
						</td>
					@endforeach
				</tr>
				<tr>
					<th>Add to cart</th>
					@foreach($products as $product)
					<td class="c{{$product['item']['id']}}">
						<a href="javascript:;" data-href="{{ route('product.cart.add',$product['item']['id']) }}" class="button add-to-cart">{{ $langg->lang75 }}</a>
					</td>
					@endforeach
				</tr>
				<tr>
					<th>&nbsp;</th>
					@foreach($products as $product)
					<td class="text-center c{{$product['item']['id']}}">
						<a title="Remove" class="remove-icon  compare-remove" data-href="{{ route('product.compare.remove',$product['item']['id']) }}" data-class="c{{$product['item']['id']}}" href="javascript:;">
							<i class="fa fa-times"></i>
						</a>
					</td>
					@endforeach
				</tr>
			</tbody>
		</table>
		<!-- /.table-compare compare-list -->
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- .entry-content -->
				</div>
				<!-- .hentry -->
			</main>
			<!-- #main -->
		</div>
		<!-- #primary -->
		@else
			<div class="col-md-12 mt-5 mb-5">
				<h2 class="text-center">No Product Found to Compare</h2>
			</div>
		@endif

	</div>
	<!-- .row -->
</div>





@endsection