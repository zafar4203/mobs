@extends('layouts.front')
@section('styles')
  
@endsection
@section('class','page home page-template-default')
@section('content')

  

<div class="col-full">
                    <div class="row">
                        <nav class="woocommerce-breadcrumb">
                            <a href="home-v1.html">Home</a>
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                            Contact Us
                        </nav>
                        <!-- .woocommerce-breadcrumb -->
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                <div class="type-page hentry">
                                    <header class="entry-header">
                                        <div class="page-header-caption">
                                            <h1 class="entry-title">Contact Us</h1>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                                    <div class="entry-content">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="text-block">
                                                    <h2 class="contact-page-title">Leave us a Message</h2>
                                                    <!-- <p>Aenean massa diam, viverra vitae luctus sed, gravida eget est. Etiam nec ipsum porttitor, consequat libero eu, dignissim eros. Nulla auctor lacinia enim id mollis. Curabitur luctus interdum eleifend. Ut tempor lorem a turpis fermentum,.</p> -->
                                                </div>
                                                <div class="contact-form">
                                                    <div role="form" class="wpcf7" id="wpcf7-f425-o1" lang="en-US" dir="ltr">
                                                        <div class="screen-reader-response"></div>
                                                        <form id="contactform" class="contact-form wpcf7-form" action="{{route('front.contact.submit')}}" method="POST">
                                                            {{csrf_field()}}
                                                            @include('includes.admin.form-both')  
                                                            <div style="display: none;">
                                                                <input type="hidden" name="_wpcf7" value="425" />
                                                                <input type="hidden" name="_wpcf7_version" value="4.5.1" />
                                                                <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f425-o1" />
                                                                <input type="hidden" name="_wpnonce" value="e6363d91dd" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Name
                                                                    <abbr title="required" class="required">*</abbr>
                                                                </label>
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap first-name">
                                                                    <input type="text" aria-invalid="false" aria-required="true" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required input-text" size="40" value="" name="name">
                                                                </span>
                                                                <!-- .col -->
                                                            </div>
                                                                                                                        <!-- .form-group -->
                                                            <div class="form-group">
                                                                <label>Email
                                                                    <abbr title="required" class="required">*</abbr>
                                                                </label>
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap subject">
                                                                    <input type="email" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" value="" name="email">
                                                                </span>
                                                            </div>

                                                            <!-- .form-group -->
                                                            <div class="form-group">
                                                                <label>Phone
                                                                    <abbr title="required" class="required">*</abbr>
                                                                </label>
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap subject">
                                                                    <input type="number" aria-invalid="false" class="wpcf7-form-control wpcf7-text input-text" size="40" value="" name="phone">
                                                                </span>
                                                            </div>

                                                            <!-- .form-group -->
                                                            <div class="form-group">
                                                                <label>Your Message
                                                                    <abbr title="required" class="required">*</abbr>
                                                                </label>
                                                                <br>
                                                                <span class="wpcf7-form-control-wrap your-message">
                                                                    <textarea aria-invalid="false" class="wpcf7-form-control wpcf7-textarea" rows="10" cols="40" name="text"></textarea>
                                                                </span>
                                                            </div>
                                                            <!-- .form-group-->
                                                            <div class="form-group clearfix">
                                                                <p>
                                                                    <input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit" />
                                                                </p>
                                                            </div>
                                                            <!-- .form-group-->
                                                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                        </form>
                                                        <!-- .wpcf7-form -->
                                                    </div>
                                                    <!-- .wpcf7 -->
                                                </div>
                                                <!-- .contact-form7 -->
                                            </div>
                                            <!-- .col -->
                                            <div class="col-md-6 store-info store-info-v2">
                                                <div class="google-map">
                                                    <iframe height="288" allowfullscreen="" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2481.593303940039!2d-0.15470444843858283!3d51.53901886611164!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ae62edd5771%3A0x27f2d823e2be0249!2sPrincess+Rd%2C+London+NW1+8JR%2C+UK!5e0!3m2!1sen!2s!4v1458827996435"></iframe>
                                                </div>
                                                <!-- .google-map -->
                                                <div class="kc-elm kc-css-773435 kc_text_block">
                                                    <h2 class="contact-page-title">Our Address</h2>
                                                    <p>{{ $ps->street }}
                                                        <br> Support {{ $ps->phone }}
                                                        <br> Email: <a href="mailto:contact@yourstore.com">{{ $ps->email }}</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- .col -->
                                        </div>
                                        <!-- .row -->
                                    </div>
                                    <!-- .entry-header -->
                                </div>
                                <!-- .hentry -->
                            </main>
                            <!-- #main -->
                        </div>
                        <!-- #primary -->
                    </div>
                    <!-- .row -->
                </div>
                <!-- .col-full -->


@endsection 
@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAq7MrCR1A2qIShmjbtLHSKjcEIEBEEwM"></script>
    <script>
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 11,
                scrollwheel: false,
                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(23.761226, 90.420766), // New York
                // How you would like to style the map. 
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{
                        "featureType": "landscape",
                        "stylers": [{
                                "hue": "#FFA800"
                            },
                            {
                                "saturation": 0
                            },
                            {
                                "lightness": 0
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "stylers": [{
                                "hue": "#53FF00"
                            },
                            {
                                "saturation": -73
                            },
                            {
                                "lightness": 40
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "stylers": [{
                                "hue": "#FBFF00"
                            },
                            {
                                "saturation": 0
                            },
                            {
                                "lightness": 0
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "stylers": [{
                                "hue": "#00FFFD"
                            },
                            {
                                "saturation": 0
                            },
                            {
                                "lightness": 30
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "stylers": [{
                                "hue": "#c1b17e"
                            },
                            {
                                "saturation": 6
                            },
                            {
                                "lightness": 8
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "stylers": [{
                                "hue": "#679714"
                            },
                            {
                                "saturation": 33.4
                            },
                            {
                                "lightness": -25.4
                            },
                            {
                                "gamma": 1
                            }
                        ]
                    }
                ]
            };
            // Get the HTML DOM element that will contain your map 
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');
            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);
            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(23.761226, 90.420766),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script>
@endsection