@extends('layouts.front')

@section('styles')

@endsection

@section('class','page-template-default woocommerce-checkout woocommerce-page')
@section('content')


<div class="col-full">
<div class="row">
    <nav class="woocommerce-breadcrumb">
        <a href="home-v1.html">Home</a>
        <span class="delimiter">
            <i class="tm tm-breadcrumbs-arrow-right"></i>
        </span>
        Checkout
    </nav>
    <!-- .woocommerce-breadcrumb -->
    <div class="content-area" id="primary">
        <main class="site-main" id="main">
            <div class="type-page hentry">
                <div class="entry-content">
                    <div class="woocommerce">
                        <!-- .collapse -->
                        <form action="{{ route('paypal.submit') }}" class="checkout woocommerce-checkout checkout-form" method="post" name="checkout">
                        {{ csrf_field() }}
                            <div class="col-md-12">
                                @include('includes.form-success')
                                @include('includes.form-error')
                            </div>

                            <div id="customer_details" class="col2-set">
                                <div class="col-1">
                                    <div class="woocommerce-billing-fields">
                                        <h3>Billing Details</h3>
                                        <div class="woocommerce-billing-fields__field-wrapper-outer">
                                            <div class="woocommerce-billing-fields__field-wrapper">
                                                <p id="billing_first_name_field" class="form-row form-row-wide validate-required woocommerce-invalid woocommerce-invalid-required-field">
                                                    <label class="" for="billing_first_name">Name
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" placeholder="" id="billing_first_name" name="name" class="input-text" value="{{ old('name') }}" required>
                                                </p>
                                                <div class="clear"></div>
                                                <p id="billing_company_field" class="form-row form-row-wide">
                                                    <label class="" for="billing_company">Company Name</label>
                                                    <input type="text" placeholder="" id="billing_company" name="company_name" value="{{ old('company_name') }}" class="input-text ">
                                                </p>
                                                <p id="billing_country_field" class="form-row form-row-wide validate-required validate-email">
                                                    <label class="" for="billing_country">Country
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <select class="country_to_state country_select select2-hidden-accessible" id="customer_country" name="customer_country" tabindex="-1" aria-hidden="true" required>
    													@include('includes.countries')
    												</select>
                                                </p>
                                                <div class="clear"></div>
                                                <p id="billing_address_1_field" class="form-row form-row-wide address-field validate-required">
                                                    <label class="" for="billing_address_1">Street address
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" placeholder="Street address" id="address" name="address" value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->address : '' }}" class="input-text ">
                                                </p>
                                                <!-- <p id="billing_address_2_field" class="form-row form-row-wide address-field">
                                                    <input type="text" value="" placeholder="Apartment, suite, unit etc. (optional)" id="billing_address_2" name="billing_address_2" class="input-text ">
                                                </p> -->
                                                <p id="billing_city_field" class="form-row form-row-wide address-field validate-required" data-o_class="form-row form-row form-row-wide address-field validate-required">
                                                    <label class="" for="billing_city">Town / City
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" placeholder="" id="city" name="city" class="input-text" value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->city : '' }}">
                                                </p>

                                                <p id="billing_state_field" class="form-row form-row-wide address-field validate-required" data-o_class="form-row form-row form-row-wide address-field validate-required">
                                                    <label class="" for="billing_city">State
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" placeholder="" id="state" name="state" class="input-text" value="{{ old('state') }}">
                                                </p>


                                                <p id="billing_postcode_field" class="form-row form-row-wide address-field validate-postcode validate-required" data-o_class="form-row form-row form-row-last address-field validate-required validate-postcode">
                                                    <label class="" for="billing_postcode">Postcode / ZIP
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" placeholder="" id="zip" name="zip" class="input-text" value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->zip : '' }}">
                                                </p>
                                                <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                                                    <label class="" for="billing_phone">Phone
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="tel" placeholder="" id="phone" name="phone" class="input-text" value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->phone : '' }}">
                                                </p>
                                                <p id="billing_email_field" class="form-row form-row-first validate-required validate-email">
                                                    <label class="" for="billing_email">Email Address
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="email" placeholder="" id="email" name="email" class="input-text" value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->email : '' }}">
                                                </p>
                                            </div>
                                        </div>
                                        <!-- .woocommerce-billing-fields__field-wrapper-outer -->
                                    </div>
                                    <!-- .woocommerce-billing-fields -->
                                    <div class="woocommerce-account-fields">
                                        <!-- <p class="form-row form-row-wide woocommerce-validated">
                                            <label class="collapsed woocommerce-form__label woocommerce-form__label-for-checkbox checkbox" data-toggle="collapse" data-target="#createLogin" aria-controls="createLogin">
                                                <input type="checkbox" value="1" name="createaccount" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox">
                                                <span>Create an account?</span>
                                            </label>
                                        </p> -->
                                        <div class="create-account collapse" id="createLogin">
                                            <p data-priority="" id="account_password_field" class="form-row validate-required woocommerce-invalid woocommerce-invalid-required-field">
                                                <label class="" for="account_password">Account password
                                                    <abbr title="required" class="required">*</abbr>
                                                </label>
                                                <input type="password" value="" placeholder="Password" id="account_password" name="account_password" class="input-text ">
                                            </p>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <!-- .woocommerce-account-fields -->
                                </div>
                                <!-- .col-1 -->
                                <div class="col-2">
                                    <div class="woocommerce-shipping-fields">
                                        <h3 id="ship-to-different-address">
                                            <label class="collapsed woocommerce-form__label woocommerce-form__label-for-checkbox checkbox" data-toggle="collapse" data-target="#shipping-address" aria-controls="shipping-address">
                                                <input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" type="checkbox" value="1" name="ship_to_different_address">
                                                <span>Ship to a different address?</span>
                                            </label>
                                        </h3>
                                        <div class="shipping_address collapse" id="shipping-address">
                                            <div class="woocommerce-shipping-fields__field-wrapper">
                                                <p id="shipping_first_name_field" class="form-row form-row-wide validate-required">
                                                    <label class="" for="shipping_first_name">Name
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" autofocus="autofocus" autocomplete="given-name" value="" placeholder="" id="shipping_name" name="shipping_name" class="input-text ">
                                                </p>
                                                <p id="shipping_company_field" class="form-row form-row-wide">
                                                    <label class="" for="shipping_company">Company name</label>
                                                    <input type="text" autocomplete="organization" value="" placeholder="" id="shipping_company" name="shipping_company" class="input-text ">
                                                </p>
                                                <p id="shipping_country_field" class="form-row form-row-wide address-field update_totals_on_change validate-required woocommerce-validated">
                                                    <label class="" for="shipping_country">Country
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <select autocomplete="country" class="country_to_state country_select select2-hidden-accessible" id="shipping_country" name="shipping_country" tabindex="-1" aria-hidden="true">
                                                        @include('includes.countries')
                                                    </select>
                                                </p>
                                                <p id="shipping_address_1_field" class="form-row form-row-wide address-field validate-required">
                                                    <label class="" for="shipping_address_1">Street address
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" autocomplete="address-line1" value="" placeholder="House number and street name" id="shipping_address_1" name="shipping_address" class="input-text ">
                                                </p>
                                                <!-- <p id="shipping_address_2_field" class="form-row form-row-wide address-field">
                                                    <input type="text" autocomplete="address-line2" value="" placeholder="Apartment, suite, unit etc. (optional)" id="shipping_address_2" name="shipping_address_2" class="input-text ">
                                                </p> -->
                                                <p id="shipping_city_field" class="form-row form-row-wide address-field validate-required" data-o_class="form-row form-row-wide address-field validate-required">
                                                    <label class="" for="shipping_city">Town / City
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" autocomplete="address-level2" value="" placeholder="" id="shipping_city" name="shipping_city" class="input-text ">
                                                </p>
                                                <p id="shipping_state_field" class="form-row form-row-wide address-field validate-state woocommerce-invalid woocommerce-invalid-required-field validate-required" data-o_class="form-row form-row-wide address-field validate-required validate-state woocommerce-invalid woocommerce-invalid-required-field">
                                                    <label class="" for="shipping_state">State / County
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" value="" placeholder="" id="shipping_state" name="shipping_state" class="input-text ">
                                                </p>
                                                <p data-priority="90" id="shipping_postcode_field" class="form-row form-row-wide address-field validate-postcode validate-required" data-o_class="form-row form-row-wide address-field validate-required validate-postcode">
                                                    <label class="" for="shipping_postcode">Postcode / ZIP
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="text" autocomplete="postal-code" value="" placeholder="" id="shipping_zip" name="shipping_zip" class="input-text ">
                                                </p>

                                                <p id="billing_phone_field" class="form-row form-row-last validate-required validate-phone">
                                                    <label class="" for="shipping_phone">Phone
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="tel" placeholder="" id="shipping_phone" name="shipping_phone" class="input-text" value="{{ old('shipping_phone') }}">
                                                </p>
                                                <p id="billing_email_field" class="form-row form-row-first validate-required validate-email">
                                                    <label class="" for="shipping_email">Email Address
                                                        <abbr title="required" class="required">*</abbr>
                                                    </label>
                                                    <input type="email" placeholder="" id="shipping_email" name="shipping_email" class="input-text" value="{{ old('shipping_email') }}">
                                                </p>

                                            </div>
                                            <!-- .woocommerce-shipping-fields__field-wrapper -->
                                        </div>
                                        <!-- .shipping_address -->
                                    </div>
                                    <!-- .woocommerce-shipping-fields -->
                                    <div class="woocommerce-additional-fields">
                                        <div class="woocommerce-additional-fields__field-wrapper">
                                            <p id="order_comments_field" class="form-row notes">
                                                <label class="" for="order_comments">Order notes</label>
                                                <textarea name="order_note" cols="5" rows="2" placeholder="Notes about your order, e.g. special notes for delivery." id="order_comments" class="input-text "></textarea>
                                            </p>
                                        </div>
                                        <!-- .woocommerce-additional-fields__field-wrapper-->
                                    </div>
                                    <!-- .woocommerce-additional-fields -->
                                </div>
                                <!-- .col-2 -->
                            </div>
                            <!-- .col2-set -->
                            <h3 id="order_review_heading">Your order</h3>
                            <div class="woocommerce-checkout-review-order" id="order_review">
                                <div class="order-review-wrapper">
                                    <h3 class="order_review_heading">Your Order</h3>
                                    <table class="shop_table woocommerce-checkout-review-order-table">
                                        <thead>
                                        @php
                                            $discount = 0;
                                        @endphp
                                            <tr>
                                                <th class="product-name">Product</th>
                                                <th class="product-total">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if($products)
                                        @foreach($products as $product)
                                        @php
                                        $discount = $discount + App\Models\Product::find($product['item']['id'])->dsc_amt;
                                        @endphp

                                            <tr class="cart_item">
                                                <td class="product-name">
                                                    <strong class="product-quantity">{{ $product['qty'] }} ×</strong> {{ str_limit($product['item']['name'] , 20) }}&nbsp;
                                                </td>
                                                <td class="product-total">
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{ App\Models\Product::convertPrice($product['price']) }}
                                                    </span>
                                                </td>
                                            </tr>

                                        @endforeach
                                        @endif 
    
                                        </tbody>
                                        <tfoot>
                                            <tr class="cart-subtotal">
                                                <th>Subtotal</th>
                                                <td>
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{ Session::has('cart') ? App\Models\Product::convertPrice(Session::get('cart')->totalPrice) : '0.00' }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="cart-subtotal">
                                            @php 
                                                if($gs->tax != 0)
                                                {
                                                    if(Session::has('cart')){
                                                        $total = Session::get('cart')->totalPrice;
                                                    }
                                                    $tax = ($total / 100) * $gs->tax;
                                                }
                                            @endphp

                                                <th>Tax</th>
                                                <td>
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{ App\Models\Product::convertPrice($tax) }}
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="cart-subtotal">
                                            <input type="hidden" id="inbuild_discount" value="{{ App\Models\Product::convertOnlyPrice($discount) }}">
                                                <th>Discount <small>(In Build)</small></th>
                                                <td>
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{ App\Models\Product::convertPrice($discount) }}
                                                    </span>
                                                </td>
                                            </tr>
                                            @if(Session::has('coupon'))							
                                            <tr class="cart-subtotal">
                                                <th>Discount <small>(Coupon)</small></th>
                                                <td>
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{ $curr->sign }}{{ Session::get('coupon')}}
                                                    </span>
                                                </td>
                                            </tr>
                                            @endif

                                            @if(Session::has('coupon_total'))
                                            @php 
                                            $d = App\Models\Product::convertOnlyPrice($discount);					
                                            $t = $totalPrice - $d;
                                            @endphp

                                            @if($gs->currency_format == 0)
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td>
                                                    <strong>
                                                        <span id="alltotal" class="woocommerce-Price-amount amount">
                                                            {{ $curr->sign }}{{ round($t , 2) }}
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            @endif								
                    						@elseif(Session::has('coupon_total1'))
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td>
                                                    <strong>
                                                        <span id="alltotal" class="woocommerce-Price-amount amount">
                                                            {{ round(Session::get('coupon_total1') - $discount , 2) }}
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            @else
                                            <tr class="order-total">
                                                <th>Total</th>
                                                <td>
                                                    <strong>
                                                        <span id="alltotal" class="woocommerce-Price-amount amount">
                                                            {{$curr->sign}}{{ round($totalPrice - ($discount * $curr->value) , 2) }}
                                                        </span>
                                                    </strong>
                                                </td>
                                            </tr>
                                            @endif

                                        </tfoot>
                                    </table>
                                    <!-- /.woocommerce-checkout-review-order-table -->
                                    <div class="woocommerce-checkout-payment" id="payment">

    @foreach($shipping_data as $key => $data)

@if($products)
@php $pr=0; @endphp
@foreach($products as $product)
    @php 
        $prod = \App\Models\Product::query()->where("id",$product['item']['id'])->first();
        if($prod->shipping && isset(explode(',',$prod->shipping)[$key]))
        $pr = $pr + $product['qty'] * explode(',',$prod->shipping)[$key];
    @endphp                        
@endforeach
@php $ttt =  Session::has('cart') ? App\Models\Product::convertOnlyPrice(Session::get('cart')->totalPrice) : '0.00'; 
    if($ttt > $gs->delivery_free){
        $pr = 0;
    }
@endphp
@endif						
<div class="radio-design">
        <input type="radio" class="shipping" id="free-shepping{{ $data->id }}" name="shipping" value="{{round($pr * $curr->value,2)}}" {{ ($loop->first) ? 'checked' : '' }}> 
        <span class="checkmark"></span>
        <label for="free-shepping{{ $data->id }}"> 
                {{ $data->title }}
                + {{ $curr->sign }}{{round($pr * $curr->value,2)}}
                <!-- @if($data->price != 0)
                + {{ $curr->sign }}{{ round($data->price * $curr->value,2) }}
                @endif -->
                <small>{{ $data->subtitle }}</small>
        </label>
</div>

@endforeach		

</div>
{{-- Shipping Method Area End --}}


                                        <div class="form-row place-order">

                                            <input type="hidden" id="shipping-cost" name="shipping_cost" value="0">
                            <input type="hidden" id="packing-cost" name="packing_cost" value="0">
                            <input type="hidden" name="dp" value="{{$digital}}">
                            <input type="hidden" name="tax" value="{{$gs->tax}}">
                            <input type="hidden" name="totalQty" value="{{$totalQty}}">

                            <input type="hidden" name="vendor_shipping_id" value="{{ $vendor_shipping_id }}">
                            <input type="hidden" name="vendor_packing_id" value="{{ $vendor_packing_id }}">


							@if(Session::has('coupon_total'))
                            	<input type="hidden" name="total" id="grandtotal" value="{{ $totalPrice }}">
                            	<input type="hidden" id="tgrandtotal" value="{{ $totalPrice }}">
							@elseif(Session::has('coupon_total1'))
								<input type="hidden" name="total" id="grandtotal" value="{{ preg_replace("/[^0-9,.]/", "", Session::get('coupon_total1') ) }}">
								<input type="hidden" id="tgrandtotal" value="{{ preg_replace("/[^0-9,.]/", "", Session::get('coupon_total1') ) }}">
							@else
                            	<input type="hidden" name="total" id="grandtotal" value="{{$totalPrice}}">
                            	<input type="hidden" id="tgrandtotal" value="{{$totalPrice}}">
							@endif


                            <input type="hidden" name="coupon_code" id="coupon_code" value="{{ Session::has('coupon_code') ? Session::get('coupon_code') : '' }}">
                            <input type="hidden" name="coupon_discount" id="coupon_discount" value="{{ Session::has('coupon') ? Session::get('coupon') : '' }}">
                            <input type="hidden" name="coupon_id" id="coupon_id" value="{{ Session::has('coupon') ? Session::get('coupon_id') : '' }}">
                            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::guard('web')->check() ? Auth::guard('web')->user()->id : '' }}">



                                            <button id="final-btn" type="submit" class="btn btn-block btn-primary text-center">Place order</button>
                                        </div>
                                    </div>
                                    <!-- /.woocommerce-checkout-payment -->
                                </div>
                                <!-- /.order-review-wrapper -->
                            </div>
                            <!-- .woocommerce-checkout-review-order -->
                        </form>
                        <!-- .woocommerce-checkout -->
                    </div>
                    <!-- .woocommerce -->
                </div>
                <!-- .entry-content -->
            </div>
            <!-- #post-## -->
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->
</div>
    <!-- .row -->
</div>
@endsection
@section('scripts')

<script src="https://js.paystack.co/v1/inline.js"></script>


<script type="text/javascript">
	$('a.payment:first').addClass('active');
	$('.checkout-form').prop('action',$('a.payment:first').data('form'));
	$($('a.payment:first').attr('href')).load($('a.payment:first').data('href'));


		var show = $('a.payment:first').data('show');
		if(show != 'no') {
			$('.pay-area').removeClass('d-none');
		}
		else {
			$('.pay-area').addClass('d-none');
		}
	$($('a.payment:first').attr('href')).addClass('active').addClass('show');
	
		   $('.submit-loader').hide();
</script>


<script type="text/javascript">

var coup = 0;
var pos = {{ $gs->currency_format }};

@if(isset($checked))

	$('#comment-log-reg1').modal('show');

@endif

var mship = $('.shipping').length > 0 ? $('.shipping').first().val() : 0;
var mpack = $('.packing').length > 0 ? $('.packing').first().val() : 0;
mship = parseFloat(mship);
mpack = parseFloat(mpack);

$('#shipping-cost').val(mship);
$('#packing-cost').val(mpack);
var ftotal = parseFloat($('#grandtotal').val()) + mship + mpack;
ftotal = parseFloat(ftotal);
      if(ftotal % 1 != 0)
      {
        ftotal = ftotal.toFixed(2);
      }
		if(pos == 0){
			$('#final-cost').html('{{ $curr->sign }}'+ftotal)
		}
		else{
			$('#final-cost').html(ftotal+'{{ $curr->sign }}')
		}

$('#grandtotal').val(ftotal);

$('#shipop').on('change',function(){

	var val = $(this).val();
	if(val == 'pickup'){
		$('#shipshow').removeClass('d-none');
		$("#ship-diff-address").parent().addClass('d-none');
        $('.ship-diff-addres-area').addClass('d-none');  
        $('.ship-diff-addres-area input, .ship-diff-addres-area select').prop('required',false);  
	}
	else{
		$('#shipshow').addClass('d-none');
		$("#ship-diff-address").parent().removeClass('d-none');
        $('.ship-diff-addres-area').removeClass('d-none');  
        $('.ship-diff-addres-area input, .ship-diff-addres-area select').prop('required',true); 
	}

});


$('.shipping').on('click',function(){
	mship = $(this).val();


$('#shipping-cost').val(mship);
var ttotal = parseFloat($('#tgrandtotal').val()) + parseFloat(mship) + parseFloat(mpack);
ttotal = parseFloat(ttotal).toFixed(2);
      if(ttotal % 1 != 0)
      {
        ttotal = parseFloat(ttotal).toFixed(2);
      }
		if(pos == 0){
			$('#final-cost').html('{{ $curr->sign }}'+parseFloat(ttotal).toFixed(2));
		}
		else{
			$('#final-cost').html(parseFloat(ttotal).toFixed(2)+'{{ $curr->sign }}');
		}
	
    var inbuild_discount = $('#inbuild_discount').val();
    var ttt = parseFloat(ttotal).toFixed(2) - parseFloat(inbuild_discount).toFixed(2);
    $('#alltotal').text('{{ $curr->sign }}' + parseFloat(ttt).toFixed(2));

})

$('.packing').on('click',function(){
	mpack = $(this).val();
$('#packing-cost').val(mpack);
var ttotal = parseFloat($('#tgrandtotal').val()) + parseFloat(mship) + parseFloat(mpack);
ttotal = parseFloat(ttotal);
      if(ttotal % 1 != 0)
      {
        ttotal = ttotal.toFixed(2);
      }

		if(pos == 0){
			$('#final-cost').html('{{ $curr->sign }}'+ttotal);
		}
		else{
			$('#final-cost').html(ttotal+'{{ $curr->sign }}');
		}	


$('#grandtotal').val(ttotal);
		
})

    $("#check-coupon-form").on('submit', function () {
        var val = $("#code").val();
        var total = $("#grandtotal").val();
        var ship = 0;
            $.ajax({
                    type: "GET",
                    url:mainurl+"/carts/coupon/check",
                    data:{code:val, total:total, shipping_cost:ship},
                    success:function(data){
                        if(data == 0)
                        {
                        	toastr.error(langg.no_coupon);
                            $("#code").val("");
                        }
                        else if(data == 2)
                        {
                        	toastr.error(langg.already_coupon);
                            $("#code").val("");
                        }
                        else
                        {
                            $("#check-coupon-form").toggle();
                            $(".discount-bar").removeClass('d-none');

							if(pos == 0){
								$('#total-cost').html('{{ $curr->sign }}'+data[0]);
								$('#discount').html('{{ $curr->sign }}'+data[2]);
							}
							else{
								$('#total-cost').html(data[0]+'{{ $curr->sign }}');
								$('#discount').html(data[2]+'{{ $curr->sign }}');
							}
								$('#grandtotal').val(data[0]);
								$('#tgrandtotal').val(data[0]);
								$('#coupon_code').val(data[1]);
								$('#coupon_discount').val(data[2]);
								if(data[4] != 0){
								$('.dpercent').html('('+data[4]+')');
								}
								else{
								$('.dpercent').html('');									
								}


var ttotal = parseFloat($('#grandtotal').val()) + parseFloat(mship) + parseFloat(mpack);
ttotal = parseFloat(ttotal);
      if(ttotal % 1 != 0)
      {
        ttotal = ttotal.toFixed(2);
      }

		if(pos == 0){
			$('#final-cost').html('{{ $curr->sign }}'+ttotal)
		}
		else{
			$('#final-cost').html(ttotal+'{{ $curr->sign }}')
		}	

                        	toastr.success(langg.coupon_found);
                            $("#code").val("");
                        }
                      }
              }); 
              return false;
    });

// Password Checking

        $("#open-pass").on( "change", function() {
            if(this.checked){
             $('.set-account-pass').removeClass('d-none');  
             $('.set-account-pass input').prop('required',true); 
             $('#personal-email').prop('required',true);
             $('#personal-name').prop('required',true);
            }
            else{
             $('.set-account-pass').addClass('d-none');   
             $('.set-account-pass input').prop('required',false); 
             $('#personal-email').prop('required',false);
             $('#personal-name').prop('required',false);

            }
        });

// Password Checking Ends


// Shipping Address Checking

		$("#ship-diff-address").on( "change", function() {
            if(this.checked){
             $('.ship-diff-addres-area').removeClass('d-none');  
             $('.ship-diff-addres-area input, .ship-diff-addres-area select').prop('required',true); 
            }
            else{
             $('.ship-diff-addres-area').addClass('d-none');  
             $('.ship-diff-addres-area input, .ship-diff-addres-area select').prop('required',false);  
            }
            
        });


// Shipping Address Checking Ends


</script>


<script type="text/javascript">
var ck = 0;

	$('.checkout-form').on('submit',function(e){
		if(ck == 0) {
			e.preventDefault();			
		$('#pills-step2-tab').removeClass('disabled');
		$('#pills-step2-tab').click();

	}else {
		$('#preloader').show();
	}
	$('#pills-step1-tab').addClass('active');
	});

	$('#step1-btn').on('click',function(){
		$('#pills-step1-tab').removeClass('active');
		$('#pills-step2-tab').removeClass('active');
		$('#pills-step3-tab').removeClass('active');
		$('#pills-step2-tab').addClass('disabled');
		$('#pills-step3-tab').addClass('disabled');

		$('#pills-step1-tab').click();

	});

// Step 2 btn DONE

	$('#step2-btn').on('click',function(){
		$('#pills-step3-tab').removeClass('active');
		$('#pills-step1-tab').removeClass('active');
		$('#pills-step2-tab').removeClass('active');
		$('#pills-step3-tab').addClass('disabled');
		$('#pills-step2-tab').click();
		$('#pills-step1-tab').addClass('active');

	});

	$('#step3-btn').on('click',function(){
	 	if($('a.payment:first').data('val') == 'paystack'){
			$('.checkout-form').prop('id','step1-form');
		}
		else {
			$('.checkout-form').prop('id','');
		}
		$('#pills-step3-tab').removeClass('disabled');
		$('#pills-step3-tab').click();

		var shipping_user  = !$('input[name="shipping_name"]').val() ? $('input[name="name"]').val() : $('input[name="shipping_name"]').val();
		var shipping_location  = !$('input[name="shipping_address"]').val() ? $('input[name="address"]').val() : $('input[name="shipping_address"]').val();
		var shipping_phone = !$('input[name="shipping_phone"]').val() ? $('input[name="phone"]').val() : $('input[name="shipping_phone"]').val();
		var shipping_email= !$('input[name="shipping_email"]').val() ? $('input[name="email"]').val() : $('input[name="shipping_email"]').val();

		$('#shipping_user').html('<i class="fas fa-user"></i>'+shipping_user);
		$('#shipping_location').html('<i class="fas fas fa-map-marker-alt"></i>'+shipping_location);
		$('#shipping_phone').html('<i class="fas fa-phone"></i>'+shipping_phone);
		$('#shipping_email').html('<i class="fas fa-envelope"></i>'+shipping_email);

		$('#pills-step1-tab').addClass('active');
		$('#pills-step2-tab').addClass('active');
	});

	$('#final-btn').on('click',function(){
		ck = 1;
	})


	$('.payment').on('click',function(){
	    
      $('.submit-loader').show();
		if($(this).data('val') == 'paystack'){
			$('.checkout-form').prop('id','step1-form');
		}
		else {
			$('.checkout-form').prop('id','');
		}
		$('.checkout-form').prop('action',$(this).data('form'));
		$('.pay-area #v-pills-tabContent .tab-pane.fade').not($(this).attr('href')).html('');
		var show = $(this).data('show');
		if(show != 'no') {
			$('.pay-area').removeClass('d-none');
		}
		else {
			$('.pay-area').addClass('d-none');
		}
		$($(this).attr('href')).load($(this).data('href'), function() {
            $('.submit-loader').hide();
        });		  
	})

        $(document).on('submit','#step1-form',function(){
        	$('#preloader').hide();
            var val = $('#sub').val();
            var total = $('#grandtotal').val();
			total = Math.round(total);
                if(val == 0)
                {
                var handler = PaystackPop.setup({
                  key: '{{$gs->paystack_key}}',
                  email: $('input[name=email]').val(),
                  amount: total * 100,
                  currency: "{{$curr->name}}",
                  ref: ''+Math.floor((Math.random() * 1000000000) + 1),
                  callback: function(response){
                    $('#ref_id').val(response.reference);
                    $('#sub').val('1');
                    $('#final-btn').click();
                  },
                  onClose: function(){
                  	window.location.reload();
                  	
                  }
                });
                handler.openIframe();
                    return false;                    
                }
                else {
                	$('#preloader').show();
                    return true;   
                }
        });
</script>
@endsection