@extends('layouts.front')
@section('styles')
    <style>
    .disabled{
        pointer-events: none;
        }
    .main-shop-page{
        margin-top:3%;
        margin-bottom:3%;
    }    
    .page-content{
        margin-top:5%;
    }    


    .breadcrumb-area .container{
        width: auto !important;
    }

    @media only screen and (max-width: 600px) {
        ol.breadcrumb{
            margin-left:0px !important
        }
    }
    </style>
@endsection
@section('class','page-template-default')

@section('content')

<div class="breadcrumb-area">
            <div class="container">
                <ol class="breadcrumb breadcrumb-list">
                    <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
                    <li class="breadcrumb-item active"> {{ $page->title }} </li>
                </ol>
            </div>
        </div>

<div class="col-full">
    <div class="row">
                    <!-- Breadcrumb Area Start Here -->
       
        <!-- Breadcrumb Area End Here -->
        <!-- Shop Page Start -->
             <!-- .woocommerce-breadcrumb -->
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="type-page hentry">
                    <div class="entry-content">
                        <!-- Row End -->
                            <h1>{{ $page->title }}</h1>
                            <div class="page-content">
                                {!! $page->details !!}
                            </div>
                        <!-- Row End -->
                    </div>
                    <!-- Container End -->
                </div>
            </main>
        </div>
    </div>
</div>




@endsection

@section('scripts')
@endsection