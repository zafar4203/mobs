@extends('layouts.front')
@section('styles')
  
@endsection
@section('class','page-template-template-homepage-v1')
@section('content')
<div class="col-full">
<div class="row">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="home-v1-slider home-slider">
            @foreach($sliders as $slider)
                <div class="slider-1" style="background-image: url({{asset('assets/images/sliders/'.$slider->photo)}});background-repeat: no-repeat;background-position: center center;background-size: cover;">
                    <!-- <img src="{{ asset('public/assets/front/assets/images/slider/home-v1-img-1.png ') }}" alt=""> -->
                    <div class="caption">
                        <div style="font-size:{{$slider->title_size}}px !important; color:{{$slider->title_color}} !important; {{$slider->title_anime}}" class="title">{{ $slider->title_text }}</div>
                        <div style="font-size:{{$slider->subtitle_size}}px !important; color:{{$slider->subtitle_color}} !important; {{$slider->subtitle_anime}}" class="sub-title">{{$slider->subtitle_text}}</div>
                        <p style="font-size:{{$slider->details_size}}px !important; color:{{$slider->details_color}} !important; {{$slider->details_anime}}">{{$slider->details_text}}</p>
                        <div class="button"><a href="{{$slider->link}}">Browse Now</a>
                            <i class="tm tm-long-arrow-right"></i>
                        </div>
                        <div class="bottom-caption">Free shipping on US Terority</div>
                    </div>
                </div>
            @endforeach
                <!-- .slider-1 -->
                <div class="slider-1 slider-2" style="background-image: url({{ asset('public/assets/front/assets/images/slider/home-v1-background.jpg ') }});">
                    <!-- <img src="{{ asset('public/assets/front/assets/images/slider/home-v1-img-2.png ') }}" alt=""> -->
                    <div class="caption">
                        <div class="title">The new-tech gift you
                            <br> are wishing for is
                            <br> right here</div>
                        <div class="sub-title">Big screens in incredibly slim designs
                            <br>that in your hand.</div>
                        <div class="button">Browse now
                            <i class="tm tm-long-arrow-right"></i>
                        </div>
                        <div class="bottom-caption">Free shipping on US Terority </div>
                    </div>
                </div>
                <!-- .slider-2 -->
            </div>
            <!-- .home-v1-slider -->
            <div class="features-list">
                <div class="features">
                    <div class="feature">
                        <div class="media">
                            <i class="feature-icon d-flex mr-3 tm tm-free-delivery"></i>
                            <div class="media-body feature-text">
                                <h5 class="mt-0">Free Delivery</h5>
                                <span>from {{$curr->sign}}{{ $gs->delivery_free }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- .feature -->
                    <div class="feature">
                        <div class="media">
                            <i class="feature-icon d-flex mr-3 tm tm-feedback"></i>
                            <div class="media-body feature-text">
                                <h5 class="mt-0">99% Customer</h5>
                                <span>Feedbacks</span>
                            </div>
                        </div>
                        <!-- .media -->
                    </div>
                    <!-- .feature -->
                    <div class="feature">
                        <div class="media">
                            <i class="feature-icon d-flex mr-3 tm tm-free-return"></i>
                            <div class="media-body feature-text">
                                <h5 class="mt-0">30 Days</h5>
                                <span>for free return</span>
                            </div>
                        </div>
                        <!-- .media -->
                    </div>
                    <!-- .feature -->
                    <div class="feature">
                        <div class="media">
                            <i class="feature-icon d-flex mr-3 tm tm-safe-payments"></i>
                            <div class="media-body feature-text">
                                <h5 class="mt-0">Payment</h5>
                                <span>Secure System</span>
                            </div>
                        </div>
                        <!-- .media -->
                    </div>
                    <!-- .feature -->
                    <div class="feature">
                        <div class="media">
                            <i class="feature-icon d-flex mr-3 tm tm-best-brands"></i>
                            <div class="media-body feature-text">
                                <h5 class="mt-0">Only Best</h5>
                                <span>Brands</span>
                            </div>
                        </div>
                        <!-- .media -->
                    </div>
                    <!-- .feature -->
                </div>
                <!-- .features -->
            </div>
            <!-- /.features list -->
            <div class="section-deals-carousel-and-products-carousel-tabs row">
                <section class="column-1 deals-carousel" id="sale-with-timer-carousel">
                    <div class="deals-carousel-inner-block">
                        <header class="section-header">
                            <h2 class="section-title">
                                <strong>Deals</strong> of the week</h2>
                            <nav class="custom-slick-nav"></nav>
                        </header>
                        <!-- /.section-header -->
                        <div class="sale-products-with-timer-carousel deals-carousel-v1">
                            <div class="products-carousel">
                                <div class="container-fluid">
                                    <div class="woocommerce columns-1">
                                        <div class="products" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#sale-with-timer-carousel .custom-slick-nav&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:767,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:1023,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}}]}">
                                            @foreach($deals as $prod)
                                            <div class="sale-product-with-timer product">
                                                <a class="woocommerce-LoopProduct-link" href="{{ route('front.product',$prod->slug) }}">
                                                    <div class="sale-product-with-timer-header">
                                                        <div class="price-and-title">
                                                            <span class="price">
                                                                <ins>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        {{ $prod->setCurrency() }}
                                                                    </span>
                                                                </ins>
                                                                <del>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        {{$curr->sign}}{{ round($prod->showOnlyPrice() + round($prod->dsc_amt * $curr->value , 2),2) }}
                                                                    </span>
                                                                </del>
                                                            </span>
                                                            <!-- /.price -->
                                                            <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                        </div>
                                                        <!-- /.price-and-title -->
                                                        <div class="sale-label-outer">
                                                            <div class="sale-saved-label">
                                                                <span class="saved-label-text">Discount</span>
                                                                <span class="saved-label-amount">
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span class="woocommerce-Price-currencySymbol">{{$curr->sign}}</span>{{ round($prod->dsc_amt  * $curr->value , 2) }}</span>
                                                                </span>
                                                            </div>
                                                            <!-- /.sale-saved-label -->
                                                        </div>
                                                        <!-- /.sale-label-outer -->
                                                    </div>
                                                    <!-- /.sale-product-with-timer-header -->
                                                    <img width="224" height="197" alt="" class="wp-post-image" src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}">
                                                    <!-- /.deal-progress -->
                                                    <div class="deal-countdown-timer">
                                                        <div class="marketing-text">
                                                            <span class="line-1">Hurry up!</span>
                                                            <span class="line-2">Deal ends in:</span>
                                                        </div>
                                                        <!-- /.marketing-text -->                                                         

                                                        <span class="deal-time-diff" style="display:none;">{{ \Carbon\Carbon::parse($prod->discount_expires)->diffInSeconds() }}</span>
                                                        <div class="deal-countdown countdown"></div>
                                                    </div>
                                                    <!-- /.deal-countdown-timer -->
                                                </a>
                                                <!-- /.woocommerce-LoopProduct-link -->
                                            </div>
                                            <!-- /.sale-product-with-timer -->
                                        @endforeach
                                        <!-- /.products -->
                                    </div>
                                    <!-- /.woocommerce -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>
                            <!-- /.slick-list -->
                        </div>
                        <!-- /.sale-products-with-timer-carousel -->
                        <footer class="section-footer">
                            <nav class="custom-slick-pagination">
                                <a class="slider-prev left" href="#" data-target="#sale-with-timer-carousel .products">
                                    <i class="tm tm-arrow-left"></i>Previous deal</a>
                                <a class="slider-next right" href="#" data-target="#sale-with-timer-carousel .products">Next deal<i class="tm tm-arrow-right"></i></a>
                            </nav>
                        </footer>
                        <!-- /.section-footer -->
                    </div>
                    <!-- /.deals-carousel-inner-block -->
                </section>
                <!-- /.deals-carousel -->
                <section class="column-2 section-products-carousel-tabs tab-carousel-1">
                    <div class="section-products-carousel-tabs-wrap">
                        <header class="section-header">
                            <ul role="tablist" class="nav justify-content-end">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#tab-59f89f0881f930" data-toggle="tab">New Arrivals</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#tab-59f89f0881f931" data-toggle="tab">On Sale</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#tab-59f89f0881f932" data-toggle="tab">Best Rated</a>
                                </li>
                            </ul>
                        </header>
                        <!-- .section-header -->
                        <div class="tab-content">
                            <div id="tab-59f89f0881f930" class="tab-pane active" role="tabpanel">
                                <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;rows&quot;:2,&quot;slidesPerRow&quot;:5,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:1023,&quot;settings&quot;:{&quot;slidesPerRow&quot;:2}},{&quot;breakpoint&quot;:1169,&quot;settings&quot;:{&quot;slidesPerRow&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesPerRow&quot;:3}}]}">
                                    <div class="container-fluid">
                                        <div class="woocommerce">
                                            <div class="products">
                                                @foreach($products as $prod)
                                                @if($prod->latest == 1)
                                                <div class="product">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                    </div>
                                                    <a class="woocommerce-LoopProduct-link" href="{{ route('front.product',$prod->slug) }}">
                                                        <img src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}" width="224" height="197" class="wp-post-image" alt="">
                                                        <span class="price">
                                                            <ins>
                                                                <span class="amount"> </span>
                                                            </ins>
                                                            <span class="amount"> {{ $prod->setCurrency() }}</span>
                                                        </span>
                                                        <!-- /.price -->
                                                        <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                    </a>
                                                    <div class="hover-area">
                                                        <a data-href="{{ route('product.cart.add',$prod->id) }}" class="add-to-cart button" href="javascript:;">Add to cart</a>
                                                        <a class="add-to-compare-link add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}">Add to compare</a>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                <!-- /.product-outer -->
                                            </div>
                                        </div>
                                        <!-- .woocommerce -->
                                    </div>
                                    <!-- .container-fluid -->
                                </div>
                                <!-- .products-carousel -->
                            </div>
                            <!-- .tab-pane -->
                            <div id="tab-59f89f0881f931" class="tab-pane " role="tabpanel">
                                <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;rows&quot;:2,&quot;slidesPerRow&quot;:5,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:1023,&quot;settings&quot;:{&quot;slidesPerRow&quot;:2}},{&quot;breakpoint&quot;:1169,&quot;settings&quot;:{&quot;slidesPerRow&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesPerRow&quot;:3}}]}">
                                    <div class="container-fluid">
                                        <div class="woocommerce">
                                            <div class="products">
                                                @foreach($products as $prod)
                                                @if($prod->sale == 1)
                                                <div class="product">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                    </div>
                                                    <a class="woocommerce-LoopProduct-link" href="{{ route('front.product',$prod->slug) }}">
                                                        <img src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}" width="224" height="197" class="wp-post-image" alt="">
                                                        <span class="price">
                                                            <ins>
                                                                <span class="amount"> </span>
                                                            </ins>
                                                            <span class="amount"> {{ $prod->setCurrency() }}</span>
                                                        </span>
                                                        <!-- /.price -->
                                                        <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                    </a>
                                                    <div class="hover-area">
                                                        <a data-href="{{ route('product.cart.add',$prod->id) }}" class="add-to-cart button" href="javascript:;">Add to cart</a>
                                                        <a class="add-to-compare-link add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}">Add to compare</a>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                <!-- /.product-outer -->

                                                <!-- /.product-outer -->
                                            </div>
                                        </div>
                                        <!-- .woocommerce -->
                                    </div>
                                    <!-- .container-fluid -->
                                </div>
                                <!-- .products-carousel -->
                            </div>
                            <!-- .tab-pane -->
                            <div id="tab-59f89f0881f932" class="tab-pane " role="tabpanel">
                                <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;rows&quot;:2,&quot;slidesPerRow&quot;:5,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:1023,&quot;settings&quot;:{&quot;slidesPerRow&quot;:2}},{&quot;breakpoint&quot;:1169,&quot;settings&quot;:{&quot;slidesPerRow&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesPerRow&quot;:3}}]}">
                                    <div class="container-fluid">
                                        <div class="woocommerce">
                                            <div class="products">

                                            @foreach($products as $prod)
                                                @if($prod->best == 1)
                                                <div class="product">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                    </div>
                                                    <a class="woocommerce-LoopProduct-link" href="{{ route('front.product',$prod->slug) }}">
                                                        <img src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}" width="224" height="197" class="wp-post-image" alt="">
                                                        <span class="price">
                                                            <ins>
                                                                <span class="amount"> </span>
                                                            </ins>
                                                            <span class="amount"> {{ $prod->setCurrency() }}</span>
                                                        </span>
                                                        <!-- /.price -->
                                                        <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                    </a>
                                                    <div class="hover-area">
                                                        <a data-href="{{ route('product.cart.add',$prod->id) }}" class="add-to-cart button" href="javascript:;">Add to cart</a>
                                                        <a class="add-to-compare-link add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}">Add to compare</a>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                <!-- /.product-outer -->

                                            </div>
                                        </div>
                                        <!-- .woocommerce -->
                                    </div>
                                    <!-- .container-fluid -->
                                </div>
                                <!-- .products-carousel -->
                            </div>
                            <!-- .tab-pane -->
                            <div id="tab-59f89f0881f933" class="tab-pane " role="tabpanel">
                                <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;rows&quot;:2,&quot;slidesPerRow&quot;:5,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:1023,&quot;settings&quot;:{&quot;slidesPerRow&quot;:2}},{&quot;breakpoint&quot;:1169,&quot;settings&quot;:{&quot;slidesPerRow&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesPerRow&quot;:3}}]}">
                                    <div class="container-fluid">
                                        <div class="woocommerce">
                                            <div class="products">
                                                <div class="product">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a href="wishlist.html" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                    </div>
                                                    <a href="single-product-fullwidth.html" class="woocommerce-LoopProduct-link">
                                                        <img src="{{ asset('public/assets/front/assets/images/products/6.jpg ') }}" width="224" height="197" class="wp-post-image" alt="">
                                                        <span class="price">
                                                            <ins>
                                                                <span class="amount"> </span>
                                                            </ins>
                                                            <span class="amount"> 456.00</span>
                                                        </span>
                                                        <!-- /.price -->
                                                        <h2 class="woocommerce-loop-product__title">Gear Virtual Reality 3D with Bluetooth Glasses</h2>
                                                    </a>
                                                    <div class="hover-area">
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                        <a class="add-to-compare-link" href="compare.html">Add to compare</a>
                                                    </div>
                                                </div>
                                                <!-- /.product-outer -->
                                                <div class="product">
                                                    <div class="yith-wcwl-add-to-wishlist">
                                                        <a href="wishlist.html" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                    </div>
                                                    <a href="single-product-fullwidth.html" class="woocommerce-LoopProduct-link">
                                                        <img src="{{ asset('public/assets/front/assets/images/products/4.jpg ') }}" width="224" height="197" class="wp-post-image" alt="">
                                                        <span class="price">
                                                            <ins>
                                                                <span class="amount"> </span>
                                                            </ins>
                                                            <span class="amount"> 456.00</span>
                                                        </span>
                                                        <!-- /.price -->
                                                        <h2 class="woocommerce-loop-product__title">4K Action Cam with Wi-Fi & GPS</h2>
                                                    </a>
                                                    <div class="hover-area">
                                                        <a class="button add_to_cart_button" href="cart.html" rel="nofollow">Add to cart</a>
                                                        <a class="add-to-compare-link" href="compare.html">Add to compare</a>
                                                    </div>
                                                </div>
                                                <!-- /.product-outer -->
                                            </div>
                                        </div>
                                        <!-- .woocommerce -->
                                    </div>
                                    <!-- .container-fluid -->
                                </div>
                                <!-- .products-carousel -->
                            </div>
                            <!-- .tab-pane -->
                        </div>
                        <!-- .tab-content -->
                    </div>
                    <!-- .section-products-carousel-tabs-wrap -->
                </section>
                <!-- .section-products-carousel-tabs -->
            </div>
            <!-- .fullwidth-notice -->
            @foreach($categories as $cat)
            @if($cat->name == "Mobiles")
            @foreach($cat->subs as $sub)
            @if($sub->name == "Smartphones")        
            <section class="section-hot-new-arrivals section-products-carousel-tabs techmarket-tabs">
                <div class="section-products-carousel-tabs-wrap">
                    <header class="section-header">
                        <h2 class="section-title">{{ $sub->name }}</h2>
                        <ul role="tablist" class="nav justify-content-end">
                            @foreach($sub->childs as $key => $child)
                            <li class="nav-item"><a class="nav-link {{$key == 0 ? 'active':''}}" href=#tab-59f89f08825d5{{$key}} data-toggle="tab">{{ $child->name }}</a></li>
                            @endforeach
                        </ul>
                    </header>
                    <!-- .section-header -->
                    <div class="tab-content">
                    @foreach($sub->childs as $key => $child)
                        <div id="tab-59f89f08825d5{{$key}}" class="tab-pane {{$key == 0 ? 'active':''}}" role="tabpanel">
                            <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:7,&quot;slidesToScroll&quot;:7,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:700,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:780,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:5}}]}">
                                <div class="container-fluid">
                                    <div class="woocommerce">
                                        <div class="products">
                                            @foreach($child->products as $prod)
                                            <div class="product">
                                                <div class="yith-wcwl-add-to-wishlist">
                                                    <a id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                </div>
                                                <a href="single-product-fullwidth.html" class="woocommerce-LoopProduct-link">
                                                    <img src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}" width="224" height="197" class="wp-post-image" alt="">
                                                    <span class="price">
                                                        <ins>
                                                            <span class="amount"> </span>
                                                        </ins>
                                                        <span class="amount"> {{ $prod->setCurrency() }}</span>
                                                    </span>
                                                    <!-- /.price -->
                                                    <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                </a>
                                                <div class="hover-area">
                                                    <a data-href="{{ route('product.cart.add',$prod->id) }}" class="add-to-cart button" href="javascript:;">Add to cart</a>
                                                    <a class="add-to-compare-link add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}">Add to compare</a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- .woocommerce -->
                                </div>
                                <!-- .container-fluid -->
                            </div>
                            <!-- .products-carousel -->
                        </div>
                        <!-- .tab-pane -->
                        @endforeach
                    </div>
                    <!-- .tab-content -->
                </div>
                <!-- .section-products-carousel-tabs-wrap -->
            </section>
            @endif
            @endforeach
            @endif
            @endforeach


            @foreach($categories as $cat)
            @if($cat->name == "Mobiles")
            @foreach($cat->subs as $sub)
            @if($sub->name == "Accessories")        

            <!-- /.section-landscape-products-carousel -->
            <section class="stretch-full-width section-products-carousel-with-vertical-tabs">
                <header class="section-header">
                    <h2 class="section-title">
                        <strong>Mobile </strong> accessories</h2>
                </header>
                <!-- /.section-header -->
                <div class="products-carousel-with-vertical-tabs row">
                    <ul role="tablist" class="nav">
                        @foreach($sub->childs as $key=> $child)
                        <li class="nav-item">
                            <a class="nav-link {{ $key == 0 ? 'active':''}}" href="#{{ $child->slug }}" data-toggle="tab">
                                <span class="category-title">
                                     {{ $child->name }}</span>
                                <i class="tm tm-arrow-right"></i>
                            </a>
                        </li>
                        @endforeach
                    </ul>

                    <div style="background-size: cover; background-position: center center; background-image: url( {{ asset('public/assets/front/assets/images/banner/vertical-bg.png ') }}); height: 552px;" class="tab-content">
                    @foreach($sub->childs as $key=> $child)                    
                        <div id="{{$child->slug}}" class="tab-pane active" role="tabpanel">
                            <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:6,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1600,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}}]}">
                                <div class="container-fluid">
                                    <div class="woocommerce columns-5">
                                        <div class="products">
                                            @foreach($child->products as $prod)
                                            <div class="product">
                                                <div class="yith-wcwl-add-to-wishlist">
                                                    <a id="{{$prod->id}}" href="javascript:;" class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
                                                </div>
                                                <a href="single-product-fullwidth.html" class="woocommerce-LoopProduct-link">
                                                    <img src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}" width="224" height="197" class="wp-post-image" alt="">
                                                    <span class="price">
                                                        <ins>
                                                            <span class="amount"> </span>
                                                        </ins>
                                                        <span class="amount"> {{ $prod->setCurrency() }}</span>
                                                    </span>
                                                    <!-- /.price -->
                                                    <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                </a>
                                                <div class="hover-area">
                                                    <a data-href="{{ route('product.cart.add',$prod->id) }}" class="add-to-cart button" href="javascript:;">Add to cart</a>
                                                    <a class="add-to-compare-link add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}">Add to compare</a>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- .woocommerce-->
                                </div>
                                <!-- .container-fluid -->
                            </div>
                            <!-- .products-carousel -->
                            @endforeach
                        </div>
                        <!-- .tab-pane -->
                    </div>
                    <!-- .tab-content -->
                </div>
                <!-- /.products-carousel-with-vertical-tabs -->
            </section>
            @endif
            @endforeach
            @endif
            @endforeach


            @if(count($recentlyViewed) > 0)
            <!-- /.product-carousel-with-banners -->
            <section class="section-landscape-products-carousel recently-viewed" id="recently-viewed">
                <header class="section-header">
                    <h2 class="section-title">Recently viewed products</h2>
                    <nav class="custom-slick-nav"></nav>
                </header>
                    <div class="container-fluid">
                        <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:2,&quot;dots&quot;:true,&quot;arrows&quot;:true,&quot;responsive&quot;:[{&quot;breakpoint&quot;:400,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:800,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}}]}">
                        <div class="woocommerce columns-5">
                            <div class="products">
                            @foreach($recentlyViewed as $prod)
                            <div class="landscape-product product">
                                    <a class="woocommerce-LoopProduct-link" href="{{ route('front.product' , $prod->slug) }}">
                                        <div class="media">
                                            <img class="wp-post-image" src="{{filter_var($prod->photo, FILTER_VALIDATE_URL) ?$prod->photo:asset('public/assets/images/products/'.$prod->photo)}}" alt="">
                                            <div class="media-body">
                                                <span class="price">
                                                    <ins>
                                                        <span>{{ $prod->setCurrency() }}</span>
                                                    </ins>
                                                    <span class="amount"> </span>
                                                </span>
                                                <!-- .price -->
                                                <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                <div class="techmarket-product-rating">
                                                    {!! $prod->showRating() !!}
                                                    <span class="review-count">({{count($prod->ratings)}})</span>
                                                </div>
                                                <!-- .techmarket-product-rating -->
                                            </div>
                                            <!-- .media-body -->
                                        </div>
                                        <!-- .media -->
                                    </a>
                                    <!-- .woocommerce-LoopProduct-link -->
                                </div>
                                @endforeach
                                

                            </div>
                        </div>
                        <!-- .woocommerce -->
                    </div>
                    <!-- .container-fluid -->
                </div>
                <!-- .products-carousel -->
            </section>
            @endif

            @if(count($b_sliders) > 0)
            <!-- .section-landscape-products-carousel -->
            <section class="brands-carousel">
                <h2 class="sr-only">Brands Carousel</h2>
                <div class="col-full" data-ride="tm-slick-carousel" data-wrap=".brands" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;responsive&quot;:[{&quot;breakpoint&quot;:400,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:800,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:5}}]}">
                    <div class="brands">
                        @foreach($b_sliders as $bds)
                        <div class="item">
                            <a href="javascript:;">
                                <figure>
                                    <figcaption class="text-overlay">
                                        <div class="info">
                                            <h4>{{ $bds->photo }}</h4>
                                        </div>
                                        <!-- /.info -->
                                    </figcaption>
                                    <img width="145" height="50" class="img-responsive desaturate" alt="apple" src="{{ url('assets/images/brand/sliders/'.$bds->photo) }}">
                                </figure>
                            </a>
                        </div>
                        @endforeach
                        <!-- .item -->
                    </div>
                </div>
                <!-- .col-full -->
            </section>
            <!-- .brands-carousel -->
            @endif
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->
</div>
<!-- .row -->
</div>
<!-- .col-full -->
@endsection 