@extends('layouts.front')
@section('title' , 'Product')
@section('class','woocommerce-active single-product full-width extended')
@section('styles')
<style>
    .custom-control {
        position: relative;
        display: inline-block;
        margin-left:10px;
        min-height: 1.5rem;
        padding-left: 1.5rem;
        padding-top:2px;
    }
    .custom-radio .custom-control-input:checked~.custom-control-label::before {
        background-color: #c7b270;
    }
    .single-makal-product:hover .wish-view::before {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
    .template-color-1 .wish-view:hover::before {
        background: #c7b270;
        color: #fff;
    }
    .wish-view::before {
        content: "";
        font-family: "Pe-icon-7-stroke";
        font-size: 18px;
    }
    .wish-view::before {
        background: #fff none repeat scroll 0 0;
        border-radius: 100%;
        color: #a3a3a3;
        content: "î˜˜";
        display: block;
        font-size: 20px;
        height: 44px;
        line-height: 44px;
        width: 44px;
        opacity: 0;
        position: absolute;
        right: 15px;
        bottom: 30px;
        text-align: center;
        -webkit-transform: scale(0.6);
        transform: scale(0.6);
        z-index: 99;
    }


    /* ratings Csss */

.rating-product{
width:100%;
}
.rating { 
border: none;
margin:0px;
margin-bottom: 0px;
float: left;
}

.rating > input { display: none; } 

.rating.star > label {
color: #fed700;
margin: 1px 0px 0px 0px;
background-color: #ffffff;
border-radius: 0;
height: 40px;
float: right;
width: 20px;
border: 1px solid #ffffff;
}
fieldset.rating.star > label:before { 
margin-top: 0;
padding: 0px;
font-size: 24px;
font-family: FontAwesome;
display: inline-block;
content: "\2605";
position: relative;
top: -9px;
}
.rating > label:before {
margin-top: 2px;
padding: 5px 12px;
font-size: 1.25em;
font-family: FontAwesome;
display: inline-block;
content: "";
}
.rating > .half:before { 
content: "\f089";
position: absolute;
}
.rating.star > label{
background-color: transparent !important;
}
.rating > label { 
color: #fff;
margin: 1px 11px 0px 0px;
background-color: #d8d8d8;
border-radius: 2px;
height: 16px;
float: right;
width: 16px;
border: 1px solid #c1c0c0;  
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { 
background-color:#fed700!important;
cursor:pointer;
} /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { 
background-color:#fed700!important;
cursor:pointer;
} 
.rating.star:not(:checked) > label:hover, /* hover current star */
.rating.star:not(:checked) > label:hover ~ label { 
color:#fed700!important;
background-color: transparent !important;
cursor:pointer;
} /* hover previous stars in list */

.rating.star > input:checked + label:hover, /* hover current star when changing rating.star */
.rating.star > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating.star > input:checked ~ label:hover ~ label { 
color:#fed700!important;
cursor:pointer;
background-color: transparent !important;
} 
.rating.star {
margin-left: 0%;
}

.review-mini-title {
color: #292929;
font-size: 18px;
font-weight: 500;
margin: 20px 0 10px 0;
text-transform: capitalize;
}

.alert{
    width:100%;
}
.alert ul{
    margin-left:10px;
}

.the-rating .star-rating:before {
    content: "";
    font-family: FontAwesome;
    letter-spacing: 0.313em;
    opacity: .25;
    float: left;
    top: 0;
    left: 0;
    position: absolute;
    color: #2c2d33;
}
</style>
@endsection

@section('content')


<div class="col-full">
                <div class="row">
                    <nav class="woocommerce-breadcrumb">
                        <a href="{{ route('front.index') }}">Home</a>

                    <span class="delimiter">
                        <i class="tm tm-breadcrumbs-arrow-right"></i>
                    </span>
                    <a href="{{route('front.category', $productt->category->slug)}}">{{$productt->category->name}}</a>

                @if($productt->subcategory_id != null)
                    <span class="delimiter">
                        <i class="tm tm-breadcrumbs-arrow-right"></i>
                    </span>
                    <a href="{{ route('front.subcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug]) }}">{{$productt->subcategory->name}}</a>
                @endif
                @if($productt->childcategory_id != null)

                <span class="delimiter">
                        <i class="tm tm-breadcrumbs-arrow-right"></i>
                    </span>
                    <a href="{{ route('front.childcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug, 'slug3' => $productt->childcategory->slug]) }}">{{$productt->childcategory->name}}</a>
                @endif
                
                <span class="delimiter">
                    <i class="tm tm-breadcrumbs-arrow-right"></i>
                </span>{{ $productt->name }}

                    </nav>
                    <!-- .woocommerce-breadcrumb -->
                    <div id="primary" class="content-area">
                        <main id="main" class="site-main">
                            <div class="product">
                                <div class="single-product-wrapper">
                                    <div class="product-images-wrapper thumb-count-4">
                                    @if($productt->dsc_amt && $productt->dsc_amt > 0)
                                        <span class="onsale">-
                                            <span class="woocommerce-Price-amount amount">
                                                {{$curr->sign}} {{ round($productt->dsc_amt * $curr->value , 2)}}
                                            </span>
                                        </span>
                                    @endif
                                        <!-- .onsale -->
                                        <div id="techmarket-single-product-gallery" class="techmarket-single-product-gallery techmarket-single-product-gallery--with-images techmarket-single-product-gallery--columns-4 images" data-columns="4">
                                            <div class="techmarket-single-product-gallery-images" data-ride="tm-slick-carousel" data-wrap=".woocommerce-product-gallery__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:false,&quot;asNavFor&quot;:&quot;#techmarket-single-product-gallery .techmarket-single-product-gallery-thumbnails__wrapper&quot;}">
                                                <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4">
                                                    <a href="#" class="woocommerce-product-gallery__trigger">🔍</a>
                                                    <figure class="woocommerce-product-gallery__wrapper ">
                                                        <div data-thumb="{{asset('public/assets/images/products/'.$productt->photo)}}" class="woocommerce-product-gallery__image">
                                                            <a href="{{asset('public/assets/images/products/'.$productt->photo)}}" tabindex="0">
                                                                <img width="600" height="600" src="{{asset('public/assets/images/products/'.$productt->photo)}}" class="attachment-shop_single size-shop_single wp-post-image" alt="">
                                                            </a>
                                                        </div>
                                                        
                                                        @foreach($productt->galleries as $gal)
                                                        <div id="gal{{$gal->id}}" data-thumb="{{asset('public/assets/images/galleries/'.$gal->photo)}}" class="woocommerce-product-gallery__image">
                                                            <a href="{{asset('public/assets/images/galleries/'.$gal->photo)}}" tabindex="0">
                                                                <img width="600" height="600" src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" class="attachment-shop_single size-shop_single wp-post-image" alt="">
                                                            </a>
                                                        </div>
                                                        @endforeach
                                                    </figure>
                                                </div>
                                                <!-- .woocommerce-product-gallery -->
                                            </div>
                                            <!-- .techmarket-single-product-gallery-images -->
                                            <div class="techmarket-single-product-gallery-thumbnails" data-ride="tm-slick-carousel" data-wrap=".techmarket-single-product-gallery-thumbnails__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;vertical&quot;:true,&quot;verticalSwiping&quot;:true,&quot;focusOnSelect&quot;:true,&quot;touchMove&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-up\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-down\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;asNavFor&quot;:&quot;#techmarket-single-product-gallery .woocommerce-product-gallery__wrapper&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:765,&quot;settings&quot;:{&quot;vertical&quot;:false,&quot;horizontal&quot;:true,&quot;verticalSwiping&quot;:false,&quot;slidesToShow&quot;:4}}]}">
                                                <figure class="techmarket-single-product-gallery-thumbnails__wrapper">
                                                    <figure data-thumb="{{asset('public/assets/images/products/'.$productt->photo)}}" class="techmarket-wc-product-gallery__image">
                                                        <img width="180" height="180" src="{{asset('public/assets/images/products/'.$productt->photo)}}" class="attachment-shop_thumbnail size-shop_thumbnail" alt="">
                                                    </figure>                                                    
                                                    @foreach($productt->galleries as $gal)
                                                    <figure data-thumb="{{asset('public/assets/images/galleries/'.$gal->photo)}}" class="techmarket-wc-product-gallery__image">
                                                        <img width="180" height="180" src="{{asset('public/assets/images/galleries/'.$gal->photo)}}" class="attachment-shop_thumbnail size-shop_thumbnail" alt="">
                                                    </figure>
                                                    @endforeach

                                                </figure>
                                                <!-- .techmarket-single-product-gallery-thumbnails__wrapper -->
                                            </div>
                                            <!-- .techmarket-single-product-gallery-thumbnails -->
                                        </div>
                                        <!-- .techmarket-single-product-gallery -->
                                    </div>
                                    <!-- .product-images-wrapper -->
                                    <div class="summary entry-summary">
                                        <div class="single-product-header">
                                            <h1 class="product_title entry-title">{{ $productt->name }}</h1>
                                            <a id="{{$productt->id}}" class="add-to-wishlist add-to-wish" data-href="{{ route('user-wishlist-add',$productt->id) }}"> Add to Wishlist</a>
                                        </div>
                                        <!-- .single-product-header -->
                                        <div class="single-product-meta">
                                            <!-- <div class="brand">
                                                <a href="#">
                                                    <img alt="galaxy" src="assets/images/brands/5.png">
                                                </a>
                                            </div> -->
                                            <div class="cat-and-sku">
                                                <span class="posted_in categories">
                                                    <a rel="tag" href="product-category.html">{{ $productt->category->name}}</a>
                                                </span>
                                                <span class="sku_wrapper">SKU:
                                                    <span class="sku">{{ $productt->sku }}</span>
                                                </span>
                                            </div>
                                            <div class="product-label">
                                                <div class="ribbon label green-label">
                                                    <span>A+</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .single-product-meta -->
                                        <div class="rating-and-sharing-wrapper">
                                            <div class="woocommerce-product-rating">
                                                {!! $productt->showRating() !!}
                                                <a rel="nofollow" class="mt-1 woocommerce-review-link" href="#reviews">(<span class="count">{{count($productt->ratings)}}</span> customer review)</a>
                                            </div>
                                        </div>

                                        <input type="hidden" id="product_price" value="{{ round($productt->vendorPrice() * $curr->value,2) }}">
                                        <input type="hidden" id="product_id" value="{{ $productt->id }}">
                                        <input type="hidden" id="curr_pos" value="{{ $gs->currency_format }}">
                                        <input type="hidden" id="curr_sign" value="{{ $curr->sign }}">

                                        <!-- .rating-and-sharing-wrapper -->
                                        <div class="woocommerce-product-details__short-description">
                                            {!! $productt->short_desc !!}
                                        </div>
                                        <!-- .woocommerce-product-details__short-description -->
                                    </div>
                                    <!-- .entry-summary -->
                                    <div class="product-actions-wrapper">
                                        <div class="product-actions">
                                            <div class="availability">
                                                Availability:
                                                <p class="stock in-stock">{{ $productt->stock }} in stock</p>
                                            </div>
                                            <!-- .availability -->
                                            <!-- <div class="additional-info">
                                                <i class="tm tm-free-delivery"></i>Item with
                                                <strong>Free Delivery</strong>
                                            </div> -->
                                            <!-- .additional-info -->
                                            <p class="price">
                                                <span id="sizeprice" class="woocommerce-Price-amount amount">
                                                    {{$productt->showPrice()}}
                                                </span>
                                            </p>
                                            <!-- .price -->
                                            <form class="variations_form cart">
                                                <table class="variations">
                                                    <tbody>
                                                        <tr>

                      @if (!empty($productt->attributes))
                        @php
                          $attrArr = json_decode($productt->attributes, true);
                        @endphp
                      @endif
                      @if (!empty($attrArr))
                          <div class="col-md-12">
                          <div class="row">
                          @foreach ($attrArr as $attrKey => $attrVal)
                            @if (array_key_exists("details_status",$attrVal) && $attrVal['details_status'] == 1)

                          <div style="padding:0px;" class="col-lg-12">
                              <div class="form-group mb-2">
                                <strong for="" class="text-capitalize">{{ str_replace("_", " ", $attrKey) }} :</strong>
                                <div class="mt-3">

                                <input type="hidden" class="keys" value="">
                                <input type="hidden" class="values" value="">
                                <select name="{{ $attrKey }}" class="custom-select">
                                @foreach ($attrVal['values'] as $optionKey => $optionVal)
                                  <div class="custom-radio">
                                    <!-- <input type="hidden" class="keys" value="">
                                    <input type="hidden" class="values" value=""> -->
                                    <!-- <input type="radio" id="{{$attrKey}}{{ $optionKey }}" name="{{ $attrKey }}" class="product-attr"  data-key="{{ $attrKey }}" data-price = "{{ $attrVal['prices'][$optionKey] * $curr->value }}" value="{{ $optionVal }}" {{ $loop->first ? 'checked' : '' }}> -->
                                    <!-- <label style="display:inline-block" class="radio-inline" for="{{$attrKey}}{{ $optionKey }}">{{ $optionVal }} -->
                                    @if (!empty($attrVal['prices'][$optionKey]))
                                      +
                                      {{$curr->sign}} {{$attrVal['prices'][$optionKey] * $curr->value}}
                                    @endif
                                    <option id="{{$attrKey}}{{ $optionKey }}" class="product-attr"  data-key="{{ $attrKey }}" data-price = "{{ $attrVal['prices'][$optionKey] * $curr->value }}"  {{ $loop->first ? 'selected' : '' }} value="{{ $optionVal }}">{{ $optionVal }}</option>

                                    <!-- </label> -->
                                  <!-- </div> -->
                                @endforeach
                                </div>
                              </div>
                          </div>
                            @endif
                          @endforeach
                          </div>
                          </div>
                      @endif

                      @if($productt->emptyStock())
                      <li class="addtocart">
                        <a href="javascript:;" class="cart-out-of-stock">
                          <i class="icofont-close-circled"></i>
                          {{ $langg->lang78 }}</a>
                      </li>
                      @endif





                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="single_variation_wrap">
                                                    <div class="woocommerce-variation-add-to-cart variations_button woocommerce-variation-add-to-cart-disabled">
                                                        <div class="quantity">
                                                            <label for="quantity-input">Quantity</label>
                                                            <input id="quantity-input" type="number" name="quantity" value="1" title="Qty" class="input-text qty text quantity qttotal" size="4">
                                                        </div>
                                                        <button id="addcrt" class="single_add_to_cart_button button alt wc-variation-selection-needed" type="button">Add to cart</button>
                                                    </div>
                                                </div>
                                                <!-- .single_variation_wrap -->
                                            </form>
                                            <!-- .variations_form -->
                                            <a class="add-to-compare-link add-to-compare" data-href="{{ route('product.compare.add',$productt->id) }}" href="javascript:;">Add to compare</a>
                                        </div>
                                        <!-- .product-actions -->
                                    </div>
                                    <!-- .product-actions-wrapper -->
                                </div>
                                <!-- .single-product-wrapper -->
                                <div class="techmarket-tabs techmarket-tabs-wrapper wc-tabs-wrapper">
                                    <!-- .techmarket-tab -->
                                    <div id="tab-description" class="techmarket-tab">
                                        <div class="tab-content">
                                            <ul role="tablist" class="nav tm-tabs">
                                                <!-- <li class="accessories_tab">
                                                    <a href="#tab-accessories">Accessories</a>
                                                </li> -->
                                                <li class="description_tab active">
                                                    <a href="#tab-description">Description</a>
                                                </li>
                                                @if($productt->specifications)
                                                <li class="specification_tab">
                                                    <a href="#tab-specification">Specification</a>
                                                </li>
                                                @endif
                                                <li class="reviews_tab">
                                                    <a href="#tab-reviews">Reviews ({{ count($productt->ratings) }})</a>
                                                </li>
                                            </ul>
                                            <!-- /.ec-tabs -->
                                            <h2>Description</h2>
                                            <div>
                                                {!! $productt->details !!}
                                            </div>
                                        </div>
                                        <!-- .tab-content -->
                                    </div>
                                    <!-- .techmarket-tab -->
                                    <div id="tab-specification" class="techmarket-tab">
                                        <div class="tab-content">
                                            @if($productt->specifications)
                                            <ul role="tablist" class="nav tm-tabs">
                                                <!-- <li class="accessories_tab">
                                                    <a href="#tab-accessories">Accessories</a>
                                                </li> -->
                                                <li class="description_tab">
                                                    <a href="#tab-description">Description</a>
                                                </li>
                                                <li class="specification_tab active">
                                                    <a href="#tab-specification">Specification</a>
                                                </li>
                                                <li class="reviews_tab">
                                                    <a href="#tab-reviews">Reviews ({{ count($productt->ratings) }})</a>
                                                </li>
                                            </ul>
                                            @endif

                                            <!-- /.ec-tabs -->
                                            @if($productt->specifications)
                                            <div class="tm-shop-attributes-detail like-column columns-3">
                                                @php $specifications = json_decode($productt->specifications) @endphp
                                                @foreach($specifications as $specification)
                                                <h3 class="tm-attributes-title">{{ $specification->heading}}</h3>
                                                <table class="shop_attributes">
                                                    <tbody>
                                                        @foreach($specification->specs as $specs)
                                                        <tr>
                                                            <th>{{ $specs->title }}</th>
                                                            <td>
                                                                <p><a href="#" rel="tag">{{ $specs->value }}</a></p>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                @endforeach
                                            </div>
                                            <!-- /.tm-shop-attributes-detail -->
                                            @endif
                                        </div>
                                        <!-- .tab-content -->
                                    </div>
                                    <!-- .techmarket-tab -->
                                    <div id="tab-reviews" class="techmarket-tab">
                                        <div class="tab-content">
                                            <ul role="tablist" class="nav tm-tabs">
                                                <!-- <li class="accessories_tab">
                                                    <a href="#tab-accessories">Accessories</a>
                                                </li> -->
                                                <li class="description_tab">
                                                    <a href="#tab-description">Description</a>
                                                </li>
                                                @if($productt->specifications)
                                                <li class="specification_tab">
                                                    <a href="#tab-specification">Specification</a>
                                                </li>
                                                @endif
                                                <li class="reviews_tab active">
                                                    <a href="#tab-reviews">Reviews ({{ count($productt->ratings) }})</a>
                                                </li>
                                            </ul>
                                            <!-- /.ec-tabs -->
                                            <div class="techmarket-advanced-reviews" id="reviews">
                                            @if(Auth::guard('web')->check())
                                                <div class="advanced-review row">
                                                    <div class="advanced-review-rating">
                                                        <h2 class="based-title">Review (1)</h2>
                                                        <div class="avg-rating">
                                                            <span class="avg-rating-number">5.0</span>
                                                            <div title="Rated 5.0 out of 5" class="star-rating">
                                                                <span style="width:100%"></span>
                                                            </div>
                                                        </div>
                                                        <!-- /.avg-rating -->
                                                        <div class="rating-histogram">
                                                            <div class="rating-bar">
                                                                <div title="Rated 5 out of 5" class="star-rating">
                                                                    <span style="width:100%"></span>
                                                                </div>
                                                                <div class="rating-count">5</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:100%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 4 out of 5" class="star-rating">
                                                                    <span style="width:80%"></span>
                                                                </div>
                                                                <div class="rating-count zero">4</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:80%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 3 out of 5" class="star-rating">
                                                                    <span style="width:60%"></span>
                                                                </div>
                                                                <div class="rating-count zero">3</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:60%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 2 out of 5" class="star-rating">
                                                                    <span style="width:40%"></span>
                                                                </div>
                                                                <div class="rating-count zero">2</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:40%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 1 out of 5" class="star-rating">
                                                                    <span style="width:20%"></span>
                                                                </div>
                                                                <div class="rating-count zero">1</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:20%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.rating-histogram -->
                                                    </div>
                                                    <!-- /.advanced-review-rating -->
                                                    <div class="advanced-review-comment">
                                                        <div id="review_form_wrapper">
                                                            <div id="review_form">
                                                                <div class="comment-respond" id="respond">
                                                                    <h3 class="comment-reply-title" id="reply-title">Add a review</h3>
                                                                    <form id="reviewform" class="comment-form" action="{{route('front.review.submit')}}" data-href="{{ route('front.reviews',$productt->id) }}" method="POST">
                                                                    @include('includes.admin.form-both')
                                                                    {{ csrf_field() }}

                                                                    <input type="hidden" id="rating" name="rating" value="5">
                                                                    <input type="hidden" name="user_id" value="{{Auth::guard('web')->user()->id}}">
                                                                    <input type="hidden" name="product_id" value="{{$productt->id}}">

                                                                        <div class="comment-form-rating">
                                                                            <label>Your Rating</label>
                                                                            <fieldset class="rating star">
                                            <input type="radio" id="field6_star5" name="rating" value="5" /><label class = "rating full" id="5" for="field6_star5"></label>
                                            <input type="radio" id="field6_star4" name="rating" value="4" /><label class = "rating full" id="4" for="field6_star4"></label>
                                            <input type="radio" id="field6_star3" name="rating" value="3" /><label class = "rating full" id="3" for="field6_star3"></label>
                                            <input type="radio" id="field6_star2" name="rating" value="2" /><label class = "rating full" id="2" for="field6_star2"></label>
                                            <input type="radio" id="field6_star1" name="rating" value="1" /><label class = "rating full" id="1" for="field6_star1"></label>
                                        </fieldset>
                                                                        </div>
                                                                        <p class="comment-form-comment">
                                                                            <label for="comment">Your Review</label>
                                                                            <textarea aria-required="true" rows="8" cols="45" name="review" id="comment"></textarea>
                                                                        </p>
                                                                        <p class="comment-form-author">
                                                                            <label for="author">Name
                                                                                <span class="required">*</span>
                                                                            </label>
                                                                            <input type="text" aria-required="true" size="30" value="{{ Auth::user()->name }}" readonly name="author" id="author">
                                                                        </p>
                                                                        <p class="comment-form-email">
                                                                            <label for="email">Email
                                                                                <span class="required">*</span>
                                                                            </label>
                                                                            <input type="text" aria-required="true" size="30" value="{{ Auth::user()->email }}" name="email" readonly id="email">
                                                                        </p>
                                                                        <p class="form-submit">
                                                                            <input type="submit" value="Add Review" class="submit" id="submit" name="submit">
                                                                        </p>
                                                                    </form>
                                                                    <!-- /.comment-form -->
                                                                </div>
                                                                <!-- /.comment-respond -->
                                                            </div>
                                                            <!-- /#review_form -->
                                                        </div>
                                                        <!-- /#review_form_wrapper -->
                                                    </div>
                                                    <!-- /.advanced-review-comment -->
                                                    @else
                                                </div>
                                                <!-- /.advanced-review -->
                                                <div id="comments">
                                                    <ol class="commentlist">
                                                        @foreach($productt->ratings as $rating)
                                                        <li id="li-comment-83" class="comment byuser comment-author-admin bypostauthor even thread-even depth-1">
                                                            <div class="comment_container" id="comment-83">
                                                                <div class="comment-text the-rating">
                                                                    <div class="star-rating">
                                                                    @php
                                                                        $i = 0;
                                                                        for($i=0;$i<5;$i++){
                                                                        if($i<$rating->rating){
                                                                            echo '<i style="color:#fed700" class="fa fa-star"></i>';                                                       
                                                                        }else{
                                                                            echo '<i style="color:#d3d3d3" class="fa fa-star-o"></i>';                                                       
                                                                        }     
                                                                        }
                                                                        @endphp
                                                                    </div>
                                                                    <p class="meta">
                                                                        <strong itemprop="author" class="woocommerce-review__author">{{ $rating->user->name }}</strong>
                                                                        <time datetime="2017-06-21T08:05:40+00:00" itemprop="datePublished" class="woocommerce-review__published-date">{{ $rating->review_date }}</time>
                                                                    </p>
                                                                    <div class="description">
                                                                        <p>{{$rating->review}}</p>
                                                                    </div>
                                                                    <!-- /.description -->
                                                                </div>
                                                                <!-- /.comment-text -->
                                                            </div>
                                                            <!-- /.comment_container -->
                                                        </li>
                                                        @endforeach
                                                        <!-- /.comment -->

                                                    </ol>
                                                    <!-- /.commentlist -->
                                                </div>

                                                <div class="row">
                                                        <div class="col-md-3">
                                                            <a href="{{ route('user.login') }}"><button class="single_add_to_cart_button button alt wc-variation-selection-needed">Login to Submit Review</button></a>                                                                        
                                                        </div>
                                                    </div>
                                                    @endif

                                                <!-- /#comments -->
                                            </div>
                                            <!-- /.techmarket-advanced-reviews -->
                                        </div>
                                        <!-- .tab-content -->
                                    </div>
                                    <!-- .techmarket-tab -->
                                </div>
                                @if(count($recentlyViewed) > 0)
                                <section class="section-landscape-products-carousel recently-viewed" id="recently-viewed">
                                    <header class="section-header">
                                        <h2 class="section-title">Recently viewed products</h2>
                                        <nav class="custom-slick-nav"></nav>
                                    </header>
                                    <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:2,&quot;dots&quot;:true,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#recently-viewed .custom-slick-nav&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1700,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}}]}">
                                        <div class="container-fluid">
                                            <div class="woocommerce columns-5">
                                                <div class="products">
                                                @foreach($recentlyViewed as $prod)
                                                <div class="landscape-product product">
                                                        <a class="woocommerce-LoopProduct-link" href="{{ route('front.product' , $prod->slug) }}">
                                                            <div class="media">
                                                                <img class="wp-post-image" src="{{filter_var($prod->photo, FILTER_VALIDATE_URL) ?$prod->photo:asset('public/assets/images/products/'.$prod->photo)}}" alt="">
                                                                <div class="media-body">
                                                                    <span class="price">
                                                                        <ins>
                                                                            <span>{{ $prod->setCurrency() }}</span>
                                                                        </ins>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                    <!-- .price -->
                                                                    <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                                    <div class="techmarket-product-rating">
                                                                        {!! $prod->showRating() !!}
                                                                        <span class="review-count">({{count($prod->ratings)}})</span>
                                                                    </div>
                                                                    <!-- .techmarket-product-rating -->
                                                                </div>
                                                                <!-- .media-body -->
                                                            </div>
                                                            <!-- .media -->
                                                        </a>
                                                        <!-- .woocommerce-LoopProduct-link -->
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <!-- .woocommerce -->
                                        </div>
                                        <!-- .container-fluid -->
                                    </div>
                                    <!-- .products-carousel -->
                                </section>
                                @endif
                                <!-- .section-landscape-products-carousel -->
                                <section class="brands-carousel">
                                    <h2 class="sr-only">Brands Carousel</h2>
                                    <div class="col-full" data-ride="tm-slick-carousel" data-wrap=".brands" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;responsive&quot;:[{&quot;breakpoint&quot;:400,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:800,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:5}}]}">
                                        <div class="brands">
                                            @foreach($b_sliders as $bds)
                                            <div class="item">
                                                <a href="javascript:;">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4>{{ $bds->photo }}</h4>
                                                            </div>
                                                            <!-- /.info -->
                                                        </figcaption>
                                                        <img width="145" height="50" class="img-responsive desaturate" alt="apple" src="{{ url('assets/images/brand/sliders/'.$bds->photo) }}">
                                                    </figure>
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- .col-full -->
                                </section>
                                <!-- .brands-carousel -->
                            </div>
                            <!-- .product -->
                        </main>
                        <!-- #main -->
                    </div>
                    <!-- #primary -->
                </div>
                <!-- .row -->
            </div>
            <!-- .col-full -->    



@endsection

@section('scripts')
<script>
$('.social-click').click(function(e){
    e.preventDefault();
    window.open($(this).attr('href'), '_blank');
});

$(document).ready(function(){
    $('.inc').click(function(e){
        var amount = $('#dynamic_price').attr('value');            
    });


$("label.rating").click(function(){
    $(this).parent().find("label").css({"background-color": "#d8d8d8"});
    $(this).css({"background-color": "#fed700"});
    $(this).nextAll().css({"background-color": "#fed700"});

    var val = $(this).attr('id');
    $('#rating').val(val);
});

$(".star label").click(function(){
    $(this).parent().find("label").css({"color": "#d8d8d8"});
    $(this).css({"color": "#fed700"});
    $(this).nextAll().css({"color": "#fed700"});
    $(this).css({"background-color": "transparent"});
    $(this).nextAll().css({"background-color": "transparent"});
});
});

$(document).ready(function(){
    $(function() {
        $('.zoom-image').each(function(){
        var originalImagePath = $(this).find('img').data('original-image');
        $(this).zoom({
        url: originalImagePath,
        magnify: 1
        });
        });

});
}); 
</script>

@endsection