@extends('layouts.front')
@section('styles')
@endsection

@section('content')
<div class="col-full">
        <div class="row">
            <!-- .woocommerce-breadcrumb -->
            <div class="content-area">


            <section class="column-1 deals-carousel" id="sale-with-timer-carousel">
                    <div class="deals-carousel-inner-block">
                        <header class="section-header">
                            <h2 class="section-title">
                                <strong>Deals</strong> of the week</h2>
                            <nav class="custom-slick-nav"></nav>
                        </header>
                        <!-- /.section-header -->
                        <div class="sale-products-with-timer-carousel deals-carousel-v1">
                            <div class="products-carousel">
                                <div class="container-fluid">
                                    <div class="woocommerce {{ count($products) > 1?'columns-4':'columns-1'}}">
                                        <div class="products">
                                            @foreach($products as $prod)
                                            <div class="sale-product-with-timer product">
                                                <a class="woocommerce-LoopProduct-link" href="{{ route('front.product',$prod->slug) }}">
                                                    <div class="sale-product-with-timer-header">
                                                        <div class="price-and-title">
                                                            <span class="price">
                                                                <ins>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        {{ $prod->setCurrency() }}
                                                                    </span>
                                                                </ins>
                                                                <del>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        {{$curr->sign}}{{ round($prod->showOnlyPrice() + round($prod->dsc_amt * $curr->value , 2),2) }}
                                                                    </span>
                                                                </del>
                                                            </span>
                                                            <!-- /.price -->
                                                            <h2 class="woocommerce-loop-product__title">{{ $prod->showName() }}</h2>
                                                        </div>
                                                        <!-- /.price-and-title -->
                                                        <div class="sale-label-outer">
                                                            <div class="sale-saved-label">
                                                                <span class="saved-label-text">Discount</span>
                                                                <span class="saved-label-amount">
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <span class="woocommerce-Price-currencySymbol">{{$curr->sign}}</span>{{ round($prod->dsc_amt  * $curr->value , 2) }}</span>
                                                                </span>
                                                            </div>
                                                            <!-- /.sale-saved-label -->
                                                        </div>
                                                        <!-- /.sale-label-outer -->
                                                    </div>
                                                    <!-- /.sale-product-with-timer-header -->
                                                    <img width="224" height="197" alt="" class="wp-post-image" src="{{ $prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png') }}">
                                                    <!-- /.deal-progress -->
                                                    <div class="deal-countdown-timer">
                                                        <div class="marketing-text">
                                                            <span class="line-1">Hurry up!</span>
                                                            <span class="line-2">Deal ends in:</span>
                                                        </div>
                                                        <!-- /.marketing-text -->                                                         

                                                        <span class="deal-time-diff" style="display:none;">{{ \Carbon\Carbon::parse($prod->discount_expires)->diffInSeconds() }}</span>
                                                        <div class="deal-countdown countdown"></div>
                                                    </div>
                                                    <!-- /.deal-countdown-timer -->
                                                </a>
                                                <!-- /.woocommerce-LoopProduct-link -->
                                            </div>
                                            <!-- /.sale-product-with-timer -->
                                        @endforeach
                                        </div>

                                        <!-- /.products -->
                                    </div>
                                    <!-- /.woocommerce -->
                                </div>
                                <!-- /.container-fluid -->
                            </div>
                            <!-- /.slick-list -->
                        </div>
                        <!-- /.section-footer -->
                    </div>
                    <!-- /.deals-carousel-inner-block -->
                </section>



            </div>
            <!-- #primary -->
        </div>
        <!-- .row -->
    </div>
@endsection
@section('scripts')
<script>

  $(document).ready(function() {
    var categories = [];
    @php
    $caties = [];
    if(isset($_GET['categories'])){
     $caties = [];
     $caties = explode(',', $_GET['categories']);     
    }
    @endphp
    categories = @php echo json_encode($caties); @endphp;
    addToPagination();
    // when dynamic attribute changes
    $(".attribute-input, #sortby").on('change', function() {
      $("#ajaxLoader").show();
      filter();
    });

    $('input[name="cat[]"]').on('change', function (e) {
        e.preventDefault();
        categories = [];
        $('input[name="cat[]"]:checked').each(function(){
            categories.push($(this).val());
        });
        $("#ajaxLoader").show();
        filter();
    });

    // when price changed & clicked in search button
    $(".filter-btn").on('click', function(e) {
      e.preventDefault();
      $("#ajaxLoader").show();
      filter();
    });

  function filter() {
    let filterlink = '';

    if ($("#prod_name").val() != '' && $("#prod_name").val() != undefined) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?search='+$("#prod_name").val();
      } else {
        filterlink += '&search='+$("#prod_name").val();
      }
    }

    if (categories.length > 0) {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+'categories='+categories;
      } else {
        filterlink += '&categories='+categories;
      }
    }

    $(".attribute-input").each(function() {
      if ($(this).is(':checked')) {
        if (filterlink == '') {
          filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$(this).attr('name')+'='+$(this).val();
        } else {
          filterlink += '&'+$(this).attr('name')+'='+$(this).val();
        }
      }
    });

    if ($("#sortby").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#sortby").attr('name')+'='+$("#sortby").val();
      } else {
        filterlink += '&'+$("#sortby").attr('name')+'='+$("#sortby").val();
      }
    }

    if ($("#minamount").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#minamount").attr('name')+'='+$("#minamount").val();
      } else {
        filterlink += '&'+$("#minamount").attr('name')+'='+$("#minamount").val();
      }
    }

    if ($("#maxamount").val() != '') {
      if (filterlink == '') {
        filterlink += '{{route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])}}' + '?'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      } else {
        filterlink += '&'+$("#maxamount").attr('name')+'='+$("#maxamount").val();
      }
    }

    // console.log(filterlink);
    console.log(encodeURI(filterlink));
    $("#ajaxContent").load(encodeURI(filterlink), function(data) {
      // add query string to pagination
      addToPagination();
      $("#ajaxLoader").fadeOut(1000);
      $('#show-total-pp').html($('#total-pp').val());  
    });
  }

  // append parameters to pagination links
  function addToPagination() {
    // add to attributes in pagination links
    $('.pagination-url').each(function() {
      let url = $(this).attr('href');
      let queryString = '?' + url.split('?')[1]; // "?page=1234...."

      let urlParams = new URLSearchParams(queryString);
      let page = urlParams.get('page'); // value of 'page' parameter
      let fullUrl = '{{route('front.category', [Request::route('category'),Request::route('subcategory'),Request::route('childcategory')])}}?page='+page+'&search='+'{{request()->input('search')}}';

      $(".attribute-input").each(function() {
        if ($(this).is(':checked')) {
          fullUrl += '&'+encodeURI($(this).attr('name'))+'='+encodeURI($(this).val());
        }
      });

      if ($("#sortby").val() != '') {
        fullUrl += '&sort='+encodeURI($("#sortby").val());
      }

      if ($("#minamount").val() != '') {
        fullUrl += '&min='+encodeURI($("#minamount").val());
      }

      if ($("#maxamount").val() != '') {
        fullUrl += '&max='+encodeURI($("#maxamount").val());
      }

    if (categories.length > 0) {
        fullUrl += '&categories='+categories;
    }

      $(this).attr('href', fullUrl);
    });
  }

  $(document).on('click', '.categori-item-area .pagination li a', function (event) {
    event.preventDefault();
    if ($(this).attr('href') != '#' && $(this).attr('href')) {
      $('#preloader').show();
      $('#ajaxContent').load($(this).attr('href'), function (response, status, xhr) {
        if (status == "success") {
          $('#preloader').fadeOut();
          $("html,body").animate({
            scrollTop: 0
          }, 1);

          addToPagination();
        }
      });
    }
  });
});

</script>
@endsection