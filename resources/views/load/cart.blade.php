@if(Session::has('cart'))
<li>
	<div class="widget woocommerce widget_shopping_cart">
		<div class="widget_shopping_cart_content">
			<ul class="woocommerce-mini-cart cart_list product_list_widget ">
			@if(Session::has('cart') && Session::get('cart')->items)
		    @foreach(Session::get('cart')->items as $product)
				<li class="woocommerce-mini-cart-item mini_cart_item cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}">
					<a href="javascript:;" class="remove cart-remove" data-class="cremove{{ $product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']) }}" data-href="{{ route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])) }}" aria-label="Remove this item" data-product_id="65" data-product_sku="">×</a>
					<a href="{{ route('front.product',$product['item']['slug']) }}">
						<img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="{{ $product['item']['photo'] ? filter_var($product['item']['photo'], FILTER_VALIDATE_URL) ?$product['item']['photo']:asset('public/assets/images/products/'.$product['item']['photo']):asset('assets/images/noimage.png') }}" alt="product">{{ $product['item']['name'] }}&nbsp;
					</a>
					<span id="cqt{{$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])}}" class="quantity">{{$product['qty']}} × 		
						<span class="woocommerce-Price-amount amount">
							{{ App\Models\Product::convertPrice($product['item']['price']) }}
						</span>
					</span>
				</li>
			@endforeach
			@endif
			</ul>
			<!-- .cart_list -->
			<p class="woocommerce-mini-cart__total total">
				<strong>Subtotal:</strong>
				<span class="woocommerce-Price-amount amount cart-total">
					{{ Session::has('cart') ? App\Models\Product::convertPrice(Session::get('cart')->totalPrice) : '0.00' }}
				</span>
			</p>
			<p class="woocommerce-mini-cart__buttons buttons">
				<a href="{{ route('front.cart') }}" class="button wc-forward">View cart</a>
				<a href="{{ route('front.checkout') }}" class="button checkout wc-forward">Checkout</a>
			</p>
		</div>
		<!-- .widget_shopping_cart_content -->
	</div>
	<!-- .widget_shopping_cart -->
</li>

@else 
<p class="mt-1 p-2 text-center">{{ $langg->lang8 }}</p>
@endif
