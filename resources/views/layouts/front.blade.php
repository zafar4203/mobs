<!doctype html>
<html class="no-js" lang="zxx">


<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @if(isset($page->meta_tag) && isset($page->meta_description))
        <meta name="keywords" content="{{ $page->meta_tag }}">
        <meta name="description" content="{{ $page->meta_description }}">
    		<title>{{$gs->title}}</title>
        @elseif(isset($blog->meta_tag) && isset($blog->meta_description))
        <meta name="keywords" content="{{ $blog->meta_tag }}">
        <meta name="description" content="{{ $blog->meta_description }}">
    		<title>{{$gs->title}}</title>
        @elseif(isset($productt))
      <meta name="keywords" content="{{ !empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): '' }}">
      <meta name="description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}">
	    <meta property="og:title" content="{{$productt->name}}" />
	    <meta property="og:description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}" />
	    <meta property="og:image" content="{{asset('assets/images/thumbnails/'.$productt->thumbnail)}}" />
	    <meta name="author" content="Manara">
    	<title>{{substr($productt->name, 0,11)."-"}}{{$gs->title}}</title>
        @else
        <meta name="keywords" content="{{ $seo->meta_keys }}">
	    <meta name="author" content="Manara">
		<title>{{$gs->title}}</title>
        @endif
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Oragnic Care Shop | @yield('title')</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/bootstrap.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/font-awesome.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/bootstrap-grid.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/bootstrap-reboot.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/font-techmarket.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/slick.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/techmarket-font-awesome.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/slick-style.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/animate.min.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/style.css') }}" media="all" />
        <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/front/assets/css/colors/blue.css') }}" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,900" rel="stylesheet">
        <link rel="shortcut icon" href="assets/images/fav-icon.png">
        <link rel="stylesheet" href="{{ asset('public/assets/front/css/toastr.css') }}">

    @yield('styles')
    <style>
        .widget_shopping_cart_content .product_list_widget, 
        .widget_shopping_cart 
        .product_list_widget{
            max-height:250px;
            overflow-y:scroll;
        } 



        /* width */

.product_list_widget::-webkit-scrollbar {
  width: 10px;
}

/* Track */

.product_list_widget::-webkit-scrollbar-track {
  background: #f1f1f1;
}

/* Handle */
 
.product_list_widget::-webkit-scrollbar-thumb {
  background: #888;
}

/* Handle on hover */ 
.product_list_widget::-webkit-scrollbar-thumb:hover {
  background: #555;
}


.vertical-center {
  max-height:60px;
  padding-top: 1.2%;
  align-items: center;
  border:1px solid #dfdfdf;
  border-radius:5px;
}
    </style>
</head>

<body class="woocommerce-active @yield('class') can-uppercase">
    <div id="page" class="hfeed site">
        @include('front.chunks.header')
        <input type="hidden" value="{{route('user.login')}}" id="user_login_url" />

        <div id="content" class="site-content">
            @yield('content')
        </div>

        <!-- Footer Area Start Here -->
        @include('front.chunks.footer')
        <!-- Footer Area End Here -->
        <!-- Quick View Content Start -->
        <div class="main-product-thumbnail quick-thumb-content">
            <div class="container">
                <!-- The Modal -->
                <div class="modal fade" id="quickview" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="quick-view-modal">
                                
                                </div>
                            </div>
                            <!-- Modal footer -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Quick View Content End -->
    </div>
    <!-- Main Wrapper End Here -->

    <script type="text/javascript">
      var mainurl = "{{url('/')}}";
      var gs      = {!! json_encode($gs) !!};
      var langg    = {!! json_encode($langg) !!};
    </script>

    <!-- For demo purposes – can be removed on production : End -->
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/jquery-migrate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/hidemaxlistitem.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/hidemaxlistitem.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/jquery.easing.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/scrollup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/jquery.waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/waypoints-sticky.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/assets/front/assets/js/scripts.js') }}"></script>
    
    <!-- Main Activation JS -->
    <script src="{{ asset('public/assets/front/js/custom2.js') }}"></script>
    <script src="{{ asset('public/assets/front/js/toastr.js') }}"></script>
    <script src="{{ asset('public/assets/front/js/main.js') }}"></script>

    @yield('scripts')
   
</body>
</html>