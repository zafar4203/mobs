@extends('layouts.front')
@section('title' , 'Login')
@section('styles')
 
@endsection
@section('class','page-template-default  pace-done')
@section('content')
<div class="col-full">
<div class="row">
    <nav class="woocommerce-breadcrumb">
        <a href="{{ route('front.index') }}">Home</a>
        <span class="delimiter">
            <i class="tm tm-breadcrumbs-arrow-right"></i>
        </span>Forgot Password
    </nav>
    <!-- .woocommerce-breadcrumb -->
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <div class="type-page hentry">
                <div class="entry-content">
                    <div class="woocommerce">
                        <div class="">
                            <!-- <span class="or-text">or</span> -->
                            <div id="#" class="u-columns col2-set">
                                <div class="mx-auto col-md-6 signin-form">
                                    <h2>Forgot Password</h2>
                                    @include('includes.admin.form-login')
                                    <form id="forgotform" action="{{route('user-forgot-submit')}}" method="POST">
                                    @csrf
                                        <p class="form-row form-row-wide">
                                            <label for="username">Username or email address
                                                <span class="required">*</span>
                                            </label>
                                            <input type="email" class="input-text" name="email" placeholder="Enter Email Address" id="email" required>
                                        </p>
                                        <p class="form-row">
                                            <input type="hidden" name="modal" value="1">
                                            <input class="authdata" type="hidden" value="Checking ...">     
                                            <input class="woocommerce-Button button" type="submit" value="Login" name="login">
                                            <a class="pull-right" href="{{ route('user.login') }}">Back</a>

                                        </p>
                                    </form>
                                    <!-- .woocommerce-form-login -->
                                </div>
                            </div>
                            <!-- .col2-set -->
                        </div>
                        <!-- .customer-login-form -->
                    </div>
                    <!-- .woocommerce -->
                </div>
                <!-- .entry-content -->
            </div>
            <!-- .hentry -->
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->
</div>
<!-- .row -->
</div>
@endsection