@extends('layouts.front')

@section('class','page woocommerce-wishlist page-template-default  ')
@section('content')


<div class="col-full">
                    <div class="row">
                        <nav class="woocommerce-breadcrumb">
                            <a href="{{ route('front.index') }}">Home</a>
                            <span class="delimiter">
                                <i class="tm tm-breadcrumbs-arrow-right"></i>
                            </span>
                            Wishlist
                        </nav>
                        <!-- .woocommerce-breadcrumb -->
                        @if(count($wishlists) > 0)

                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                <div class="type-page hentry">
                                    <header class="entry-header">
                                        <div class="page-header-caption">
                                            <h1 class="entry-title">Wishlist</h1>
                                        </div>
                                    </header>
                                    <!-- .entry-header -->
                        <div class="entry-content">
                            <form class="woocommerce" method="post" action="#">
                                <table class="shop_table cart wishlist_table">
                                    <thead>
                                        <tr>
                                            <th class="product-remove">
                                            </th>
                                            <th class="product-thumbnail"></th>
                                            <th class="product-name">
                                                <span class="nobr">Product Name</span>
                                            </th>
                                            <th class="product-price">
                                                <span class="nobr">
                                                    Unit Price
                                                </span>
                                            </th>
                                            <th class="product-stock-status">
                                                <span class="nobr">
                                                    Stock Status
                                                </span>
                                            </th>
                                            <th class="product-add-to-cart"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($wishlists as $wishlist)
                                        <tr>
                                            <td class="product-remove">
                                                <a title="Remove this product" class="remove wishlist-remove" data-href="{{ route('user-wishlist-remove',$wishlist->id) }}" href="javascript:;">×</a>
                                            </td>
                                            <td class="product-thumbnail">
                                                <a href="single-product-fullwidth.html">
                                                    <img width="180" height="180" alt="" class="wp-post-image" src="{{ $wishlist->product->thumbnail ? asset('public/assets/images/thumbnails/'.$wishlist->product->thumbnail):asset('assets/images/noimage.png') }}">
                                                </a>
                                            </td>
                                            <td class="product-name">
                                                <a href="{{ route('front.product', $wishlist->product->slug) }}">
                                                    {{ $wishlist->product->name }}
                                                </a>
                                            </td>
                                            <td class="product-price">
                                                <ins>
                                                    <span class="woocommerce-Price-amount amount">
                                                        {{ $wishlist->product->showPrice() }}
                                                    </span>
                                                </ins>
                                                <!-- <del>
                                                    <span class="woocommerce-Price-amount amount">
                                                        <span class="woocommerce-Price-currencySymbol">£</span>229.99</span>
                                                </del> -->
                                            </td>
                                            <td class="product-stock-status">
                                                <span class="wishlist-in-stock">In Stock</span>
                                            </td>
                                            <td class="product-add-to-cart">
                                                <input id="{{ $wishlist->product->id}}_input" class="qt_val" type="hidden" value="1" />
                                                <a class="button add_to_cart_button button alt addcart_wishlist" id="{{ $wishlist->product->id}}" href="javascript:;">add to cart </a>
                                            </td>											
                                        </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                                    <!-- <tr>
                                                        <td colspan="6">
                                                            <div class="yith-wcwl-share">
                                                                <h4 class="yith-wcwl-share-title">Share on:</h4>
                                                                <ul>
                                                                    <li style="list-style-type: none; display: inline-block;">
                                                                        <a title="Facebook" href="https://www.facebook.com/sharer.php?s=100&amp;p%5Btitle%5D=My+wishlist+on+Tech+Market&amp;p%5Burl%5D=http%3A%2F%2Flocalhost%2F%7Efarook%2Ftechmarket%2Fhome-v1.html%2Fwishlist%2Fview%2FD5ON1PW1PYO1%2F" class="facebook" target="_blank"></a>
                                                                    </li>
                                                                    <li style="list-style-type: none; display: inline-block;">
                                                                        <a title="Twitter" href="https://twitter.com/share?url=http%3A%2F%2Flocalhost%2F%7Efarook%2Ftechmarket%2Fhome-v1.html%2Fwishlist%2Fview%2FD5ON1PW1PYO1%2F&amp;text=" class="twitter" target="_blank"></a>
                                                                    </li>
                                                                    <li style="list-style-type: none; display: inline-block;">
                                                                        <a onclick="window.open(this.href); return false;" title="Pinterest" href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Flocalhost%2F%7Efarook%2Ftechmarket%2Fhome-v1.html%2Fwishlist%2Fview%2FD5ON1PW1PYO1%2F&amp;description=&amp;media=" class="pinterest" target="_blank"></a>
                                                                    </li>
                                                                    <li style="list-style-type: none; display: inline-block;">
                                                                        <a onclick="javascript:window.open(this.href, &quot;&quot;, &quot;menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600&quot;);return false;" title="Google+" href="https://plus.google.com/share?url=http%3A%2F%2Flocalhost%2F%7Efarook%2Ftechmarket%2Fhome-v1.html%2Fwishlist%2Fview%2FD5ON1PW1PYO1%2F&amp;title=My+wishlist+on+Tech+Market" class="googleplus" target="_blank"></a>
                                                                    </li>
                                                                    <li style="list-style-type: none; display: inline-block;">
                                                                        <a title="Email" href="mailto:?subject=I+wanted+you+to+see+this+site&amp;body=http%3A%2F%2Flocalhost%2F%7Efarook%2Ftechmarket%2Fhome-v1.html%2Fwishlist%2Fview%2FD5ON1PW1PYO1%2F&amp;title=My+wishlist+on+Tech+Market" class="email"></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr> -->
                                                </tfoot>
                                            </table>
                                            <!-- .wishlist_table -->
                                        </form>
                                        <!-- .woocommerce -->
                                    </div>
                                    <!-- .entry-content -->
                                </div>
                                <!-- .hentry -->
                            </main>
                            <!-- #main -->
                        </div>
                        <!-- #primary -->
                    </div>
                    <!-- .row -->
                </div>

                @else
				<div class="row">
					<div class="mt-5 mb-5 pt-5 pb-5 col-md-12 text-center">
						<h1>Wishlist Empty</h1>
					</div>
				</div>
				@endif



@endsection
@section('scripts')

<script type="text/javascript">
        $("#sortby").on('change',function () {
        var sort = $("#sortby").val();
        window.location = "{{url('/user/wishlists')}}?sort="+sort;
		});
		

		$('.minuso').click(function(e){
			e.preventDefault();
			var val = $('#'+this.id+'_input').val();
			if(val>1){
				val--;
				$('#'+this.id+'_input').attr('value', val);
			}
		});

		$('.pluso').click(function(e){
			e.preventDefault();
			var val = $('#'+this.id+'_input').val();
			val++;
			$('#'+this.id+'_input').attr('value', val);
		});
</script>

@endsection
