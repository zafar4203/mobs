<?php if($paginator->lastPage() > 1): ?>

<div class="shop-control-bar-bottom">
            <form class="form-techmarket-wc-ppp" method="POST">
                <select class="techmarket-wc-wppp-select c-select" onchange="this.form.submit()" name="ppp">
                    <option value="20">Show 20</option>
                    <option value="40">Show 40</option>
                    <option value="-1">Show All</option>
                </select>
                <input type="hidden" value="5" name="shop_columns">
                <input type="hidden" value="15" name="shop_per_page">
                <input type="hidden" value="right-sidebar" name="shop_layout">
            </form>
            <!-- .form-techmarket-wc-ppp -->
            <p class="woocommerce-result-count">
                Showing <?php echo e($paginator->firstItem()); ?>–<?php echo e($paginator->lastItem()); ?> of <?php echo e($paginator->total()); ?> result(s)
            </p>
            <!-- .woocommerce-result-count -->
            <nav class="woocommerce-pagination">
                <ul class="page-numbers">
                    <li>
                        <a class="page-numbers <?php echo e(($paginator->currentPage() == 1) ? ' disabled' : ''); ?>" href="<?php echo e($paginator->url($paginator->currentPage()-1)); ?>">Prev</a>
                    </li>

                    <?php for($i = 1; $i <= $paginator->lastPage(); $i++): ?>
                    <li>
                        <a class="page-numbers <?php echo e(($paginator->currentPage() == $i) ? ' current' : ''); ?>" href="<?php echo e($paginator->url($i)); ?>"><?php echo e($i); ?></a>
                    </li>
                    <?php endfor; ?>

                    <li><a href="<?php echo e($paginator->url($paginator->currentPage()+1)); ?>" class="next page-numbers <?php echo e(($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : ''); ?>">next</a></li>
                </ul>
                <!-- .page-numbers -->
            </nav>
            <!-- .woocommerce-pagination -->
        </div>
        <!-- .shop-control-bar-bottom -->
        <?php endif; ?>




<?php /**PATH C:\wamp64\www\mobs\resources\views/pagination/default.blade.php ENDPATH**/ ?>