<!doctype html>
<html class="no-js" lang="zxx">


<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php if(isset($page->meta_tag) && isset($page->meta_description)): ?>
        <meta name="keywords" content="<?php echo e($page->meta_tag); ?>">
        <meta name="description" content="<?php echo e($page->meta_description); ?>">
    		<title><?php echo e($gs->title); ?></title>
        <?php elseif(isset($blog->meta_tag) && isset($blog->meta_description)): ?>
        <meta name="keywords" content="<?php echo e($blog->meta_tag); ?>">
        <meta name="description" content="<?php echo e($blog->meta_description); ?>">
    		<title><?php echo e($gs->title); ?></title>
        <?php elseif(isset($productt)): ?>
      <meta name="keywords" content="<?php echo e(!empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): ''); ?>">
      <meta name="description" content="<?php echo e($productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description)); ?>">
	    <meta property="og:title" content="<?php echo e($productt->name); ?>" />
	    <meta property="og:description" content="<?php echo e($productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description)); ?>" />
	    <meta property="og:image" content="<?php echo e(asset('assets/images/thumbnails/'.$productt->thumbnail)); ?>" />
	    <meta name="author" content="Manara">
    	<title><?php echo e(substr($productt->name, 0,11)."-"); ?><?php echo e($gs->title); ?></title>
        <?php else: ?>
        <meta name="keywords" content="<?php echo e($seo->meta_keys); ?>">
	    <meta name="author" content="Manara">
		<title><?php echo e($gs->title); ?></title>
        <?php endif; ?>
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Oragnic Care Shop | <?php echo $__env->yieldContent('title'); ?></title>

        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/bootstrap.min.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/font-awesome.min.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/bootstrap-grid.min.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/bootstrap-reboot.min.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/font-techmarket.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/slick.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/techmarket-font-awesome.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/slick-style.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/animate.min.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/style.css')); ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/front/assets/css/colors/blue.css')); ?>" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,900" rel="stylesheet">
        <link rel="shortcut icon" href="assets/images/fav-icon.png">
        <link rel="stylesheet" href="<?php echo e(asset('public/assets/front/css/toastr.css')); ?>">

    <?php echo $__env->yieldContent('styles'); ?>
    <style>
        .widget_shopping_cart_content .product_list_widget, 
        .widget_shopping_cart 
        .product_list_widget{
            max-height:250px;
            overflow-y:scroll;
        } 



        /* width */

.product_list_widget::-webkit-scrollbar {
  width: 10px;
}

/* Track */

.product_list_widget::-webkit-scrollbar-track {
  background: #f1f1f1;
}

/* Handle */
 
.product_list_widget::-webkit-scrollbar-thumb {
  background: #888;
}

/* Handle on hover */ 
.product_list_widget::-webkit-scrollbar-thumb:hover {
  background: #555;
}


.vertical-center {
  max-height:60px;
  padding-top: 1.2%;
  align-items: center;
  border:1px solid #dfdfdf;
  border-radius:5px;
}
    </style>
</head>

<body class="woocommerce-active <?php echo $__env->yieldContent('class'); ?> can-uppercase">
    <div id="page" class="hfeed site">
        <?php echo $__env->make('front.chunks.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <input type="hidden" value="<?php echo e(route('user.login')); ?>" id="user_login_url" />

        <div id="content" class="site-content">
            <?php echo $__env->yieldContent('content'); ?>
        </div>

        <!-- Footer Area Start Here -->
        <?php echo $__env->make('front.chunks.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <!-- Footer Area End Here -->
        <!-- Quick View Content Start -->
        <div class="main-product-thumbnail quick-thumb-content">
            <div class="container">
                <!-- The Modal -->
                <div class="modal fade" id="quickview" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <div class="quick-view-modal">
                                
                                </div>
                            </div>
                            <!-- Modal footer -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Quick View Content End -->
    </div>
    <!-- Main Wrapper End Here -->

    <script type="text/javascript">
      var mainurl = "<?php echo e(url('/')); ?>";
      var gs      = <?php echo json_encode($gs); ?>;
      var langg    = <?php echo json_encode($langg); ?>;
    </script>

    <!-- For demo purposes – can be removed on production : End -->
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/jquery.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/tether.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/jquery-migrate.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/hidemaxlistitem.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/jquery-ui.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/hidemaxlistitem.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/jquery.easing.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/scrollup.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/jquery.waypoints.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/waypoints-sticky.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/pace.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/slick.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('public/assets/front/assets/js/scripts.js')); ?>"></script>
    
    <!-- Main Activation JS -->
    <script src="<?php echo e(asset('public/assets/front/js/custom2.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets/front/js/toastr.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets/front/js/main.js')); ?>"></script>

    <?php echo $__env->yieldContent('scripts'); ?>
   
</body>
</html><?php /**PATH C:\wamp64\www\mobs\resources\views/layouts/front.blade.php ENDPATH**/ ?>