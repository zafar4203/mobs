<?php $__env->startSection('title' , 'Product'); ?>
<?php $__env->startSection('class','woocommerce-active single-product full-width extended'); ?>
<?php $__env->startSection('styles'); ?>
<style>
    .custom-control {
        position: relative;
        display: inline-block;
        margin-left:10px;
        min-height: 1.5rem;
        padding-left: 1.5rem;
        padding-top:2px;
    }
    .custom-radio .custom-control-input:checked~.custom-control-label::before {
        background-color: #c7b270;
    }
    .single-makal-product:hover .wish-view::before {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
    .template-color-1 .wish-view:hover::before {
        background: #c7b270;
        color: #fff;
    }
    .wish-view::before {
        content: "";
        font-family: "Pe-icon-7-stroke";
        font-size: 18px;
    }
    .wish-view::before {
        background: #fff none repeat scroll 0 0;
        border-radius: 100%;
        color: #a3a3a3;
        content: "î˜˜";
        display: block;
        font-size: 20px;
        height: 44px;
        line-height: 44px;
        width: 44px;
        opacity: 0;
        position: absolute;
        right: 15px;
        bottom: 30px;
        text-align: center;
        -webkit-transform: scale(0.6);
        transform: scale(0.6);
        z-index: 99;
    }


    /* ratings Csss */

.rating-product{
width:100%;
}
.rating { 
border: none;
margin:0px;
margin-bottom: 0px;
float: left;
}

.rating > input { display: none; } 

.rating.star > label {
color: #fed700;
margin: 1px 0px 0px 0px;
background-color: #ffffff;
border-radius: 0;
height: 40px;
float: right;
width: 20px;
border: 1px solid #ffffff;
}
fieldset.rating.star > label:before { 
margin-top: 0;
padding: 0px;
font-size: 24px;
font-family: FontAwesome;
display: inline-block;
content: "\2605";
position: relative;
top: -9px;
}
.rating > label:before {
margin-top: 2px;
padding: 5px 12px;
font-size: 1.25em;
font-family: FontAwesome;
display: inline-block;
content: "";
}
.rating > .half:before { 
content: "\f089";
position: absolute;
}
.rating.star > label{
background-color: transparent !important;
}
.rating > label { 
color: #fff;
margin: 1px 11px 0px 0px;
background-color: #d8d8d8;
border-radius: 2px;
height: 16px;
float: right;
width: 16px;
border: 1px solid #c1c0c0;  
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { 
background-color:#fed700!important;
cursor:pointer;
} /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { 
background-color:#fed700!important;
cursor:pointer;
} 
.rating.star:not(:checked) > label:hover, /* hover current star */
.rating.star:not(:checked) > label:hover ~ label { 
color:#fed700!important;
background-color: transparent !important;
cursor:pointer;
} /* hover previous stars in list */

.rating.star > input:checked + label:hover, /* hover current star when changing rating.star */
.rating.star > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating.star > input:checked ~ label:hover ~ label { 
color:#fed700!important;
cursor:pointer;
background-color: transparent !important;
} 
.rating.star {
margin-left: 0%;
}

.review-mini-title {
color: #292929;
font-size: 18px;
font-weight: 500;
margin: 20px 0 10px 0;
text-transform: capitalize;
}

.alert{
    width:100%;
}
.alert ul{
    margin-left:10px;
}

.the-rating .star-rating:before {
    content: "";
    font-family: FontAwesome;
    letter-spacing: 0.313em;
    opacity: .25;
    float: left;
    top: 0;
    left: 0;
    position: absolute;
    color: #2c2d33;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>


<div class="col-full">
                <div class="row">
                    <nav class="woocommerce-breadcrumb">
                        <a href="<?php echo e(route('front.index')); ?>">Home</a>

                    <span class="delimiter">
                        <i class="tm tm-breadcrumbs-arrow-right"></i>
                    </span>
                    <a href="<?php echo e(route('front.category', $productt->category->slug)); ?>"><?php echo e($productt->category->name); ?></a>

                <?php if($productt->subcategory_id != null): ?>
                    <span class="delimiter">
                        <i class="tm tm-breadcrumbs-arrow-right"></i>
                    </span>
                    <a href="<?php echo e(route('front.subcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug])); ?>"><?php echo e($productt->subcategory->name); ?></a>
                <?php endif; ?>
                <?php if($productt->childcategory_id != null): ?>

                <span class="delimiter">
                        <i class="tm tm-breadcrumbs-arrow-right"></i>
                    </span>
                    <a href="<?php echo e(route('front.childcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug, 'slug3' => $productt->childcategory->slug])); ?>"><?php echo e($productt->childcategory->name); ?></a>
                <?php endif; ?>
                
                <span class="delimiter">
                    <i class="tm tm-breadcrumbs-arrow-right"></i>
                </span><?php echo e($productt->name); ?>


                    </nav>
                    <!-- .woocommerce-breadcrumb -->
                    <div id="primary" class="content-area">
                        <main id="main" class="site-main">
                            <div class="product">
                                <div class="single-product-wrapper">
                                    <div class="product-images-wrapper thumb-count-4">
                                    <?php if($productt->dsc_amt && $productt->dsc_amt > 0): ?>
                                        <span class="onsale">-
                                            <span class="woocommerce-Price-amount amount">
                                                <?php echo e($curr->sign); ?> <?php echo e(round($productt->dsc_amt * $curr->value , 2)); ?>

                                            </span>
                                        </span>
                                    <?php endif; ?>
                                        <!-- .onsale -->
                                        <div id="techmarket-single-product-gallery" class="techmarket-single-product-gallery techmarket-single-product-gallery--with-images techmarket-single-product-gallery--columns-4 images" data-columns="4">
                                            <div class="techmarket-single-product-gallery-images" data-ride="tm-slick-carousel" data-wrap=".woocommerce-product-gallery__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:false,&quot;asNavFor&quot;:&quot;#techmarket-single-product-gallery .techmarket-single-product-gallery-thumbnails__wrapper&quot;}">
                                                <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4">
                                                    <a href="#" class="woocommerce-product-gallery__trigger">🔍</a>
                                                    <figure class="woocommerce-product-gallery__wrapper ">
                                                        <div data-thumb="<?php echo e(asset('public/assets/images/products/'.$productt->photo)); ?>" class="woocommerce-product-gallery__image">
                                                            <a href="<?php echo e(asset('public/assets/images/products/'.$productt->photo)); ?>" tabindex="0">
                                                                <img width="600" height="600" src="<?php echo e(asset('public/assets/images/products/'.$productt->photo)); ?>" class="attachment-shop_single size-shop_single wp-post-image" alt="">
                                                            </a>
                                                        </div>
                                                        
                                                        <?php $__currentLoopData = $productt->galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <div id="gal<?php echo e($gal->id); ?>" data-thumb="<?php echo e(asset('public/assets/images/galleries/'.$gal->photo)); ?>" class="woocommerce-product-gallery__image">
                                                            <a href="<?php echo e(asset('public/assets/images/galleries/'.$gal->photo)); ?>" tabindex="0">
                                                                <img width="600" height="600" src="<?php echo e(asset('public/assets/images/galleries/'.$gal->photo)); ?>" class="attachment-shop_single size-shop_single wp-post-image" alt="">
                                                            </a>
                                                        </div>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </figure>
                                                </div>
                                                <!-- .woocommerce-product-gallery -->
                                            </div>
                                            <!-- .techmarket-single-product-gallery-images -->
                                            <div class="techmarket-single-product-gallery-thumbnails" data-ride="tm-slick-carousel" data-wrap=".techmarket-single-product-gallery-thumbnails__wrapper" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;vertical&quot;:true,&quot;verticalSwiping&quot;:true,&quot;focusOnSelect&quot;:true,&quot;touchMove&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-up\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-down\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;asNavFor&quot;:&quot;#techmarket-single-product-gallery .woocommerce-product-gallery__wrapper&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:765,&quot;settings&quot;:{&quot;vertical&quot;:false,&quot;horizontal&quot;:true,&quot;verticalSwiping&quot;:false,&quot;slidesToShow&quot;:4}}]}">
                                                <figure class="techmarket-single-product-gallery-thumbnails__wrapper">
                                                    <figure data-thumb="<?php echo e(asset('public/assets/images/products/'.$productt->photo)); ?>" class="techmarket-wc-product-gallery__image">
                                                        <img width="180" height="180" src="<?php echo e(asset('public/assets/images/products/'.$productt->photo)); ?>" class="attachment-shop_thumbnail size-shop_thumbnail" alt="">
                                                    </figure>                                                    
                                                    <?php $__currentLoopData = $productt->galleries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <figure data-thumb="<?php echo e(asset('public/assets/images/galleries/'.$gal->photo)); ?>" class="techmarket-wc-product-gallery__image">
                                                        <img width="180" height="180" src="<?php echo e(asset('public/assets/images/galleries/'.$gal->photo)); ?>" class="attachment-shop_thumbnail size-shop_thumbnail" alt="">
                                                    </figure>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                </figure>
                                                <!-- .techmarket-single-product-gallery-thumbnails__wrapper -->
                                            </div>
                                            <!-- .techmarket-single-product-gallery-thumbnails -->
                                        </div>
                                        <!-- .techmarket-single-product-gallery -->
                                    </div>
                                    <!-- .product-images-wrapper -->
                                    <div class="summary entry-summary">
                                        <div class="single-product-header">
                                            <h1 class="product_title entry-title"><?php echo e($productt->name); ?></h1>
                                            <a id="<?php echo e($productt->id); ?>" class="add-to-wishlist add-to-wish" data-href="<?php echo e(route('user-wishlist-add',$productt->id)); ?>"> Add to Wishlist</a>
                                        </div>
                                        <!-- .single-product-header -->
                                        <div class="single-product-meta">
                                            <!-- <div class="brand">
                                                <a href="#">
                                                    <img alt="galaxy" src="assets/images/brands/5.png">
                                                </a>
                                            </div> -->
                                            <div class="cat-and-sku">
                                                <span class="posted_in categories">
                                                    <a rel="tag" href="product-category.html"><?php echo e($productt->category->name); ?></a>
                                                </span>
                                                <span class="sku_wrapper">SKU:
                                                    <span class="sku"><?php echo e($productt->sku); ?></span>
                                                </span>
                                            </div>
                                            <div class="product-label">
                                                <div class="ribbon label green-label">
                                                    <span>A+</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .single-product-meta -->
                                        <div class="rating-and-sharing-wrapper">
                                            <div class="woocommerce-product-rating">
                                                <?php echo $productt->showRating(); ?>

                                                <a rel="nofollow" class="mt-1 woocommerce-review-link" href="#reviews">(<span class="count"><?php echo e(count($productt->ratings)); ?></span> customer review)</a>
                                            </div>
                                        </div>

                                        <input type="hidden" id="product_price" value="<?php echo e(round($productt->vendorPrice() * $curr->value,2)); ?>">
                                        <input type="hidden" id="product_id" value="<?php echo e($productt->id); ?>">
                                        <input type="hidden" id="curr_pos" value="<?php echo e($gs->currency_format); ?>">
                                        <input type="hidden" id="curr_sign" value="<?php echo e($curr->sign); ?>">

                                        <!-- .rating-and-sharing-wrapper -->
                                        <div class="woocommerce-product-details__short-description">
                                            <?php echo $productt->short_desc; ?>

                                        </div>
                                        <!-- .woocommerce-product-details__short-description -->
                                    </div>
                                    <!-- .entry-summary -->
                                    <div class="product-actions-wrapper">
                                        <div class="product-actions">
                                            <div class="availability">
                                                Availability:
                                                <p class="stock in-stock"><?php echo e($productt->stock); ?> in stock</p>
                                            </div>
                                            <!-- .availability -->
                                            <!-- <div class="additional-info">
                                                <i class="tm tm-free-delivery"></i>Item with
                                                <strong>Free Delivery</strong>
                                            </div> -->
                                            <!-- .additional-info -->
                                            <p class="price">
                                                <span id="sizeprice" class="woocommerce-Price-amount amount">
                                                    <?php echo e($productt->showPrice()); ?>

                                                </span>
                                            </p>
                                            <!-- .price -->
                                            <form class="variations_form cart">
                                                <table class="variations">
                                                    <tbody>
                                                        <tr>

                      <?php if(!empty($productt->attributes)): ?>
                        <?php
                          $attrArr = json_decode($productt->attributes, true);
                        ?>
                      <?php endif; ?>
                      <?php if(!empty($attrArr)): ?>
                          <div class="col-md-12">
                          <div class="row">
                          <?php $__currentLoopData = $attrArr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attrKey => $attrVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(array_key_exists("details_status",$attrVal) && $attrVal['details_status'] == 1): ?>

                          <div style="padding:0px;" class="col-lg-12">
                              <div class="form-group mb-2">
                                <strong for="" class="text-capitalize"><?php echo e(str_replace("_", " ", $attrKey)); ?> :</strong>
                                <div class="mt-3">

                                <input type="hidden" class="keys" value="">
                                <input type="hidden" class="values" value="">
                                <select name="<?php echo e($attrKey); ?>" class="custom-select">
                                <?php $__currentLoopData = $attrVal['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $optionKey => $optionVal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <div class="custom-radio">
                                    <!-- <input type="hidden" class="keys" value="">
                                    <input type="hidden" class="values" value=""> -->
                                    <!-- <input type="radio" id="<?php echo e($attrKey); ?><?php echo e($optionKey); ?>" name="<?php echo e($attrKey); ?>" class="product-attr"  data-key="<?php echo e($attrKey); ?>" data-price = "<?php echo e($attrVal['prices'][$optionKey] * $curr->value); ?>" value="<?php echo e($optionVal); ?>" <?php echo e($loop->first ? 'checked' : ''); ?>> -->
                                    <!-- <label style="display:inline-block" class="radio-inline" for="<?php echo e($attrKey); ?><?php echo e($optionKey); ?>"><?php echo e($optionVal); ?> -->
                                    <?php if(!empty($attrVal['prices'][$optionKey])): ?>
                                      +
                                      <?php echo e($curr->sign); ?> <?php echo e($attrVal['prices'][$optionKey] * $curr->value); ?>

                                    <?php endif; ?>
                                    <option id="<?php echo e($attrKey); ?><?php echo e($optionKey); ?>" class="product-attr"  data-key="<?php echo e($attrKey); ?>" data-price = "<?php echo e($attrVal['prices'][$optionKey] * $curr->value); ?>"  <?php echo e($loop->first ? 'selected' : ''); ?> value="<?php echo e($optionVal); ?>"><?php echo e($optionVal); ?></option>

                                    <!-- </label> -->
                                  <!-- </div> -->
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                              </div>
                          </div>
                            <?php endif; ?>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </div>
                          </div>
                      <?php endif; ?>

                      <?php if($productt->emptyStock()): ?>
                      <li class="addtocart">
                        <a href="javascript:;" class="cart-out-of-stock">
                          <i class="icofont-close-circled"></i>
                          <?php echo e($langg->lang78); ?></a>
                      </li>
                      <?php endif; ?>





                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="single_variation_wrap">
                                                    <div class="woocommerce-variation-add-to-cart variations_button woocommerce-variation-add-to-cart-disabled">
                                                        <div class="quantity">
                                                            <label for="quantity-input">Quantity</label>
                                                            <input id="quantity-input" type="number" name="quantity" value="1" title="Qty" class="input-text qty text quantity qttotal" size="4">
                                                        </div>
                                                        <button id="addcrt" class="single_add_to_cart_button button alt wc-variation-selection-needed" type="button">Add to cart</button>
                                                    </div>
                                                </div>
                                                <!-- .single_variation_wrap -->
                                            </form>
                                            <!-- .variations_form -->
                                            <a class="add-to-compare-link add-to-compare" data-href="<?php echo e(route('product.compare.add',$productt->id)); ?>" href="javascript:;">Add to compare</a>
                                        </div>
                                        <!-- .product-actions -->
                                    </div>
                                    <!-- .product-actions-wrapper -->
                                </div>
                                <!-- .single-product-wrapper -->
                                <div class="techmarket-tabs techmarket-tabs-wrapper wc-tabs-wrapper">
                                    <!-- .techmarket-tab -->
                                    <div id="tab-description" class="techmarket-tab">
                                        <div class="tab-content">
                                            <ul role="tablist" class="nav tm-tabs">
                                                <!-- <li class="accessories_tab">
                                                    <a href="#tab-accessories">Accessories</a>
                                                </li> -->
                                                <li class="description_tab active">
                                                    <a href="#tab-description">Description</a>
                                                </li>
                                                <?php if($productt->specifications): ?>
                                                <li class="specification_tab">
                                                    <a href="#tab-specification">Specification</a>
                                                </li>
                                                <?php endif; ?>
                                                <li class="reviews_tab">
                                                    <a href="#tab-reviews">Reviews (<?php echo e(count($productt->ratings)); ?>)</a>
                                                </li>
                                            </ul>
                                            <!-- /.ec-tabs -->
                                            <h2>Description</h2>
                                            <div>
                                                <?php echo $productt->details; ?>

                                            </div>
                                        </div>
                                        <!-- .tab-content -->
                                    </div>
                                    <!-- .techmarket-tab -->
                                    <div id="tab-specification" class="techmarket-tab">
                                        <div class="tab-content">
                                            <?php if($productt->specifications): ?>
                                            <ul role="tablist" class="nav tm-tabs">
                                                <!-- <li class="accessories_tab">
                                                    <a href="#tab-accessories">Accessories</a>
                                                </li> -->
                                                <li class="description_tab">
                                                    <a href="#tab-description">Description</a>
                                                </li>
                                                <li class="specification_tab active">
                                                    <a href="#tab-specification">Specification</a>
                                                </li>
                                                <li class="reviews_tab">
                                                    <a href="#tab-reviews">Reviews (<?php echo e(count($productt->ratings)); ?>)</a>
                                                </li>
                                            </ul>
                                            <?php endif; ?>

                                            <!-- /.ec-tabs -->
                                            <?php if($productt->specifications): ?>
                                            <div class="tm-shop-attributes-detail like-column columns-3">
                                                <?php $specifications = json_decode($productt->specifications) ?>
                                                <?php $__currentLoopData = $specifications; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $specification): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <h3 class="tm-attributes-title"><?php echo e($specification->heading); ?></h3>
                                                <table class="shop_attributes">
                                                    <tbody>
                                                        <?php $__currentLoopData = $specification->specs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $specs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <th><?php echo e($specs->title); ?></th>
                                                            <td>
                                                                <p><a href="#" rel="tag"><?php echo e($specs->value); ?></a></p>
                                                            </td>
                                                        </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </tbody>
                                                </table>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                            <!-- /.tm-shop-attributes-detail -->
                                            <?php endif; ?>
                                        </div>
                                        <!-- .tab-content -->
                                    </div>
                                    <!-- .techmarket-tab -->
                                    <div id="tab-reviews" class="techmarket-tab">
                                        <div class="tab-content">
                                            <ul role="tablist" class="nav tm-tabs">
                                                <!-- <li class="accessories_tab">
                                                    <a href="#tab-accessories">Accessories</a>
                                                </li> -->
                                                <li class="description_tab">
                                                    <a href="#tab-description">Description</a>
                                                </li>
                                                <?php if($productt->specifications): ?>
                                                <li class="specification_tab">
                                                    <a href="#tab-specification">Specification</a>
                                                </li>
                                                <?php endif; ?>
                                                <li class="reviews_tab active">
                                                    <a href="#tab-reviews">Reviews (<?php echo e(count($productt->ratings)); ?>)</a>
                                                </li>
                                            </ul>
                                            <!-- /.ec-tabs -->
                                            <div class="techmarket-advanced-reviews" id="reviews">
                                            <?php if(Auth::guard('web')->check()): ?>
                                                <div class="advanced-review row">
                                                    <div class="advanced-review-rating">
                                                        <h2 class="based-title">Review (1)</h2>
                                                        <div class="avg-rating">
                                                            <span class="avg-rating-number">5.0</span>
                                                            <div title="Rated 5.0 out of 5" class="star-rating">
                                                                <span style="width:100%"></span>
                                                            </div>
                                                        </div>
                                                        <!-- /.avg-rating -->
                                                        <div class="rating-histogram">
                                                            <div class="rating-bar">
                                                                <div title="Rated 5 out of 5" class="star-rating">
                                                                    <span style="width:100%"></span>
                                                                </div>
                                                                <div class="rating-count">5</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:100%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 4 out of 5" class="star-rating">
                                                                    <span style="width:80%"></span>
                                                                </div>
                                                                <div class="rating-count zero">4</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:80%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 3 out of 5" class="star-rating">
                                                                    <span style="width:60%"></span>
                                                                </div>
                                                                <div class="rating-count zero">3</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:60%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 2 out of 5" class="star-rating">
                                                                    <span style="width:40%"></span>
                                                                </div>
                                                                <div class="rating-count zero">2</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:40%"></span>
                                                                </div>
                                                            </div>
                                                            <div class="rating-bar">
                                                                <div title="Rated 1 out of 5" class="star-rating">
                                                                    <span style="width:20%"></span>
                                                                </div>
                                                                <div class="rating-count zero">1</div>
                                                                <div class="rating-percentage-bar">
                                                                    <span class="rating-percentage" style="width:20%"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.rating-histogram -->
                                                    </div>
                                                    <!-- /.advanced-review-rating -->
                                                    <div class="advanced-review-comment">
                                                        <div id="review_form_wrapper">
                                                            <div id="review_form">
                                                                <div class="comment-respond" id="respond">
                                                                    <h3 class="comment-reply-title" id="reply-title">Add a review</h3>
                                                                    <form id="reviewform" class="comment-form" action="<?php echo e(route('front.review.submit')); ?>" data-href="<?php echo e(route('front.reviews',$productt->id)); ?>" method="POST">
                                                                    <?php echo $__env->make('includes.admin.form-both', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                                                    <?php echo e(csrf_field()); ?>


                                                                    <input type="hidden" id="rating" name="rating" value="5">
                                                                    <input type="hidden" name="user_id" value="<?php echo e(Auth::guard('web')->user()->id); ?>">
                                                                    <input type="hidden" name="product_id" value="<?php echo e($productt->id); ?>">

                                                                        <div class="comment-form-rating">
                                                                            <label>Your Rating</label>
                                                                            <fieldset class="rating star">
                                            <input type="radio" id="field6_star5" name="rating" value="5" /><label class = "rating full" id="5" for="field6_star5"></label>
                                            <input type="radio" id="field6_star4" name="rating" value="4" /><label class = "rating full" id="4" for="field6_star4"></label>
                                            <input type="radio" id="field6_star3" name="rating" value="3" /><label class = "rating full" id="3" for="field6_star3"></label>
                                            <input type="radio" id="field6_star2" name="rating" value="2" /><label class = "rating full" id="2" for="field6_star2"></label>
                                            <input type="radio" id="field6_star1" name="rating" value="1" /><label class = "rating full" id="1" for="field6_star1"></label>
                                        </fieldset>
                                                                        </div>
                                                                        <p class="comment-form-comment">
                                                                            <label for="comment">Your Review</label>
                                                                            <textarea aria-required="true" rows="8" cols="45" name="review" id="comment"></textarea>
                                                                        </p>
                                                                        <p class="comment-form-author">
                                                                            <label for="author">Name
                                                                                <span class="required">*</span>
                                                                            </label>
                                                                            <input type="text" aria-required="true" size="30" value="<?php echo e(Auth::user()->name); ?>" readonly name="author" id="author">
                                                                        </p>
                                                                        <p class="comment-form-email">
                                                                            <label for="email">Email
                                                                                <span class="required">*</span>
                                                                            </label>
                                                                            <input type="text" aria-required="true" size="30" value="<?php echo e(Auth::user()->email); ?>" name="email" readonly id="email">
                                                                        </p>
                                                                        <p class="form-submit">
                                                                            <input type="submit" value="Add Review" class="submit" id="submit" name="submit">
                                                                        </p>
                                                                    </form>
                                                                    <!-- /.comment-form -->
                                                                </div>
                                                                <!-- /.comment-respond -->
                                                            </div>
                                                            <!-- /#review_form -->
                                                        </div>
                                                        <!-- /#review_form_wrapper -->
                                                    </div>
                                                    <!-- /.advanced-review-comment -->
                                                    <?php else: ?>
                                                </div>
                                                <!-- /.advanced-review -->
                                                <div id="comments">
                                                    <ol class="commentlist">
                                                        <?php $__currentLoopData = $productt->ratings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rating): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li id="li-comment-83" class="comment byuser comment-author-admin bypostauthor even thread-even depth-1">
                                                            <div class="comment_container" id="comment-83">
                                                                <div class="comment-text the-rating">
                                                                    <div class="star-rating">
                                                                    <?php
                                                                        $i = 0;
                                                                        for($i=0;$i<5;$i++){
                                                                        if($i<$rating->rating){
                                                                            echo '<i style="color:#fed700" class="fa fa-star"></i>';                                                       
                                                                        }else{
                                                                            echo '<i style="color:#d3d3d3" class="fa fa-star-o"></i>';                                                       
                                                                        }     
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                    <p class="meta">
                                                                        <strong itemprop="author" class="woocommerce-review__author"><?php echo e($rating->user->name); ?></strong>
                                                                        <time datetime="2017-06-21T08:05:40+00:00" itemprop="datePublished" class="woocommerce-review__published-date"><?php echo e($rating->review_date); ?></time>
                                                                    </p>
                                                                    <div class="description">
                                                                        <p><?php echo e($rating->review); ?></p>
                                                                    </div>
                                                                    <!-- /.description -->
                                                                </div>
                                                                <!-- /.comment-text -->
                                                            </div>
                                                            <!-- /.comment_container -->
                                                        </li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                        <!-- /.comment -->

                                                    </ol>
                                                    <!-- /.commentlist -->
                                                </div>

                                                <div class="row">
                                                        <div class="col-md-3">
                                                            <a href="<?php echo e(route('user.login')); ?>"><button class="single_add_to_cart_button button alt wc-variation-selection-needed">Login to Submit Review</button></a>                                                                        
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>

                                                <!-- /#comments -->
                                            </div>
                                            <!-- /.techmarket-advanced-reviews -->
                                        </div>
                                        <!-- .tab-content -->
                                    </div>
                                    <!-- .techmarket-tab -->
                                </div>
                                <?php if(count($recentlyViewed) > 0): ?>
                                <section class="section-landscape-products-carousel recently-viewed" id="recently-viewed">
                                    <header class="section-header">
                                        <h2 class="section-title">Recently viewed products</h2>
                                        <nav class="custom-slick-nav"></nav>
                                    </header>
                                    <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:2,&quot;dots&quot;:true,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#recently-viewed .custom-slick-nav&quot;,&quot;responsive&quot;:[{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:2}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1700,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}}]}">
                                        <div class="container-fluid">
                                            <div class="woocommerce columns-5">
                                                <div class="products">
                                                <?php $__currentLoopData = $recentlyViewed; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="landscape-product product">
                                                        <a class="woocommerce-LoopProduct-link" href="<?php echo e(route('front.product' , $prod->slug)); ?>">
                                                            <div class="media">
                                                                <img class="wp-post-image" src="<?php echo e(filter_var($prod->photo, FILTER_VALIDATE_URL) ?$prod->photo:asset('public/assets/images/products/'.$prod->photo)); ?>" alt="">
                                                                <div class="media-body">
                                                                    <span class="price">
                                                                        <ins>
                                                                            <span><?php echo e($prod->setCurrency()); ?></span>
                                                                        </ins>
                                                                        <span class="amount"> </span>
                                                                    </span>
                                                                    <!-- .price -->
                                                                    <h2 class="woocommerce-loop-product__title"><?php echo e($prod->showName()); ?></h2>
                                                                    <div class="techmarket-product-rating">
                                                                        <?php echo $prod->showRating(); ?>

                                                                        <span class="review-count">(<?php echo e(count($prod->ratings)); ?>)</span>
                                                                    </div>
                                                                    <!-- .techmarket-product-rating -->
                                                                </div>
                                                                <!-- .media-body -->
                                                            </div>
                                                            <!-- .media -->
                                                        </a>
                                                        <!-- .woocommerce-LoopProduct-link -->
                                                    </div>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                            </div>
                                            <!-- .woocommerce -->
                                        </div>
                                        <!-- .container-fluid -->
                                    </div>
                                    <!-- .products-carousel -->
                                </section>
                                <?php endif; ?>
                                <!-- .section-landscape-products-carousel -->
                                <section class="brands-carousel">
                                    <h2 class="sr-only">Brands Carousel</h2>
                                    <div class="col-full" data-ride="tm-slick-carousel" data-wrap=".brands" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;responsive&quot;:[{&quot;breakpoint&quot;:400,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:800,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}},{&quot;breakpoint&quot;:1400,&quot;settings&quot;:{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:5}}]}">
                                        <div class="brands">
                                            <?php $__currentLoopData = $b_sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="item">
                                                <a href="javascript:;">
                                                    <figure>
                                                        <figcaption class="text-overlay">
                                                            <div class="info">
                                                                <h4><?php echo e($bds->photo); ?></h4>
                                                            </div>
                                                            <!-- /.info -->
                                                        </figcaption>
                                                        <img width="145" height="50" class="img-responsive desaturate" alt="apple" src="<?php echo e(url('assets/images/brand/sliders/'.$bds->photo)); ?>">
                                                    </figure>
                                                </a>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                    <!-- .col-full -->
                                </section>
                                <!-- .brands-carousel -->
                            </div>
                            <!-- .product -->
                        </main>
                        <!-- #main -->
                    </div>
                    <!-- #primary -->
                </div>
                <!-- .row -->
            </div>
            <!-- .col-full -->    



<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
$('.social-click').click(function(e){
    e.preventDefault();
    window.open($(this).attr('href'), '_blank');
});

$(document).ready(function(){
    $('.inc').click(function(e){
        var amount = $('#dynamic_price').attr('value');            
    });


$("label.rating").click(function(){
    $(this).parent().find("label").css({"background-color": "#d8d8d8"});
    $(this).css({"background-color": "#fed700"});
    $(this).nextAll().css({"background-color": "#fed700"});

    var val = $(this).attr('id');
    $('#rating').val(val);
});

$(".star label").click(function(){
    $(this).parent().find("label").css({"color": "#d8d8d8"});
    $(this).css({"color": "#fed700"});
    $(this).nextAll().css({"color": "#fed700"});
    $(this).css({"background-color": "transparent"});
    $(this).nextAll().css({"background-color": "transparent"});
});
});

$(document).ready(function(){
    $(function() {
        $('.zoom-image').each(function(){
        var originalImagePath = $(this).find('img').data('original-image');
        $(this).zoom({
        url: originalImagePath,
        magnify: 1
        });
        });

});
}); 
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\mobs\resources\views/front/product.blade.php ENDPATH**/ ?>