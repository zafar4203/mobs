<?php echo $__env->yieldContent('styles'); ?>

<?php echo $__env->yieldContent('content'); ?>

<script src="<?php echo e(asset('public/assets/admin/js/vendors/jquery-1.12.4.min.js')); ?>"></script>
		<script src="<?php echo e(asset('public/assets/admin/js/jqueryui.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/admin/js/vendors/vue.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/admin/js/bootstrap-colorpicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/admin/js/plugin.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/admin/js/tag-it.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/admin/js/load.js')); ?>"></script>

<?php echo $__env->yieldContent('scripts'); ?>
<?php /**PATH C:\wamp64\www\mobs\resources\views/layouts/load.blade.php ENDPATH**/ ?>