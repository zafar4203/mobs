<footer class="site-footer footer-v1">
<div class="col-full">
    <div class="before-footer-wrap">
        <div class="col-full">
            <div class="footer-newsletter">
                <div class="media">
                    <i class="footer-newsletter-icon tm tm-newsletter"></i>
                    <div class="media-body">
                        <div class="clearfix">
                            <div class="newsletter-header">
                                <h5 class="newsletter-title">Sign up to Newsletter</h5>
                                <span class="newsletter-marketing-text">...and receive
                                    <strong>$20 coupon for first shopping</strong>
                                </span>
                            </div>
                            <!-- .newsletter-header -->
                            <div class="newsletter-body">
                                <?php echo $__env->make('includes.admin.form-login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <form id="subscribeform" action="<?php echo e(route('front.subscribe')); ?>" class="newsletter-form" method="post">
                                <?php echo csrf_field(); ?>
                                    <input type="text" name="email" placeholder="Enter Email Address" required />
                                    <button class="button" type="submit">Sign up</button>
                                </form>
                            </div>
                            <!-- .newsletter body -->
                        </div>
                        <!-- .clearfix -->
                    </div>
                    <!-- .media-body -->
                </div>
                <!-- .media -->
            </div>
            <!-- .footer-newsletter -->
            <div class="footer-social-icons">
                <ul class="social-icons nav">
                    <?php if(App\Models\Socialsetting::find(1)->f_status == 1): ?>
                        <li class="nav-item">
                            <a href="<?php echo e(App\Models\Socialsetting::find(1)->facebook); ?>" class="sm-icon-label-link nav-link" target="_blank">
                                <i class="fa fa-facebook"></i> Facebook
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if(App\Models\Socialsetting::find(1)->g_status == 1): ?>
                    <li class="nav-item">
                            <a href="<?php echo e(App\Models\Socialsetting::find(1)->gplus); ?>" class="sm-icon-label-link nav-link" target="_blank">
                                <i class="fa fa-google-plus"></i> Google Plus
                            </a>
                    </li>
                    <?php endif; ?>

                    <?php if(App\Models\Socialsetting::find(1)->t_status == 1): ?>
                    <li class="nav-item">

                        <a href="<?php echo e(App\Models\Socialsetting::find(1)->twitter); ?>" class="sm-icon-label-link nav-link" target="_blank">
                            <i class="fa fa-twitter"></i> Twitter
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(App\Models\Socialsetting::find(1)->l_status == 1): ?>
                    <li class="nav-item">

                        <a href="<?php echo e(App\Models\Socialsetting::find(1)->linkedin); ?>" class="sm-icon-label-link nav-link" target="_blank">
                            <i class="fa fa-linkedin"></i> Linkedin
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(App\Models\Socialsetting::find(1)->d_status == 1): ?>
                    <li class="nav-item">
                    <a href="<?php echo e(App\Models\Socialsetting::find(1)->dribble); ?>" class="sm-icon-label-link nav-link" target="_blank">
                            <i class="fa fa-dribbble"></i> Dribble
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(App\Models\Socialsetting::find(1)->i_status == 1): ?>
                    <li class="nav-item">
                        <a href="<?php echo e(App\Models\Socialsetting::find(1)->instagram); ?>" class="sm-icon-label-link nav-link" target="_blank">
                            <i class="fa fa-instagram"></i> Instagram
                        </a>
                    </li>
                    <?php endif; ?>

                </ul>
            </div>
            <!-- .footer-social-icons -->
        </div>
        <!-- .col-full -->
    </div>
    <!-- .before-footer-wrap -->
    <div class="footer-widgets-block">
        <div class="row">
            <div class="footer-contact">
                <div class="footer-logo">
                    <a href="home-v1.html" class="custom-logo-link" rel="home">
                        <img style="height:80px;" src="<?php echo e(asset('assets/images/'.$gs->footer_logo)); ?>" class="img" alt="footer-logo-image">
                    </a>
                </div>
                <!-- .footer-logo -->
                <div class="contact-payment-wrap">
                    <div class="footer-contact-info">
                        <div class="media">
                            <span class="media-left icon media-middle">
                                <i class="tm tm-call-us-footer"></i>
                            </span>
                            <div class="media-body">
                                <span class="call-us-title">Got Questions ? Call us 24/7!</span>
                                <span class="call-us-text"><?php echo e($ps->phone); ?></span>
                                <address class="footer-contact-address"><?php echo e($ps->street); ?></address>
                                <!-- <a href="#" class="footer-address-map-link">
                                    <i class="tm tm-map-marker"></i>Find us on map</a> -->
                            </div>
                            <!-- .media-body -->
                        </div>
                        <!-- .media -->
                    </div>
                    <!-- .footer-contact-info -->
                    <div class="footer-payment-info">
                        <div class="media">
                            <span class="media-left icon media-middle">
                                <i class="tm tm-safe-payments"></i>
                            </span>
                            <div class="media-body">
                                <h5 class="footer-payment-info-title">We are using safe payments</h5>
                                <div class="footer-payment-icons">
                                    <ul class="list-payment-icons nav">
                                        <li class="nav-item">
                                            <img class="payment-icon-image" src="<?php echo e(asset('public/assets/front/assets/images/credit-cards/mastercard.svg')); ?>" alt="mastercard" />
                                        </li>
                                        <li class="nav-item">
                                            <img class="payment-icon-image" src="<?php echo e(asset('public/assets/front/assets/images/credit-cards/visa.svg')); ?>" alt="visa" />
                                        </li>
                                        <li class="nav-item">
                                            <img class="payment-icon-image" src="<?php echo e(asset('public/assets/front/assets/images/credit-cards/paypal.svg')); ?>" alt="paypal" />
                                        </li>
                                        <li class="nav-item">
                                            <img class="payment-icon-image" src="<?php echo e(asset('public/assets/front/assets/images/credit-cards/maestro.svg')); ?>" alt="maestro" />
                                        </li>
                                    </ul>
                                </div>
                                <!-- .footer-payment-icons -->
                                <div class="footer-secure-by-info">
                                    <h6 class="footer-secured-by-title">Secured by:</h6>
                                    <ul class="footer-secured-by-icons">
                                        <li class="nav-item">
                                            <img class="secure-icons-image" src="<?php echo e(asset('public/assets/front/assets/images/secured-by/norton.svg')); ?>" alt="norton" />
                                        </li>
                                        <li class="nav-item">
                                            <img class="secure-icons-image" src="<?php echo e(asset('public/assets/front/assets/images/secured-by/mcafee.svg')); ?>" alt="mcafee" />
                                        </li>
                                    </ul>
                                </div>
                                <!-- .footer-secure-by-info -->
                            </div>
                            <!-- .media-body -->
                        </div>
                        <!-- .media -->
                    </div>
                    <!-- .footer-payment-info -->
                </div>
                <!-- .contact-payment-wrap -->
            </div>
            <!-- .footer-contact -->
            <div class="footer-widgets">
                <div class="col-md-6">
                    <aside class="widget clearfix">
                        <div class="body">
                            <h4 class="widget-title">Find it Fast</h4>
                            <div class="menu-footer-menu-1-container">
                                <ul id="menu-footer-menu-1" class="menu">

                                <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($page->category == 'find' && $page->footer == 1): ?>
                                    <li class="menu-item"><a href="<?php echo e(route('front.page',$page->slug)); ?>"><?php echo e($page->title); ?></a></li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <!-- .menu-footer-menu-1-container -->
                        </div>
                        <!-- .body -->
                    </aside>
                    <!-- .widget -->
                </div>
                <!-- .columns -->
                <div class="col-md-6">
                    <aside class="widget clearfix">
                        <div class="body">
                            <h4 class="widget-title">Customer Care</h4>
                            <div class="menu-footer-menu-3-container">
                                <ul id="menu-footer-menu-3" class="menu">
                                <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($page->category == 'customer' && $page->footer == 1): ?>
                                    <li class="menu-item"><a href="<?php echo e(route('front.page',$page->slug)); ?>"><?php echo e($page->title); ?></a></li>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <!-- .menu-footer-menu-3-container -->
                        </div>
                        <!-- .body -->
                    </aside>
                    <!-- .widget -->
                </div>
                <!-- .columns -->
            </div>
            <!-- .footer-widgets -->
        </div>
        <!-- .row -->
    </div>
    <!-- .footer-widgets-block -->
    <div class="site-info">
        <div class="col-full">
            <div class="copyright">Copyright &copy; 2017 <a href="home-v1.html">Techmarket</a> Theme. All rights reserved.</div>
            <!-- .copyright -->
            <div class="credit">Made with
                <i class="fa fa-heart"></i> by bcube.</div>
            <!-- .credit -->
        </div>
        <!-- .col-full -->
    </div>
    <!-- .site-info -->
</div>
<!-- .col-full -->
</footer>
<?php /**PATH C:\wamp64\www\mobs\resources\views/front/chunks/footer.blade.php ENDPATH**/ ?>