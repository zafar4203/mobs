<?php if(Session::has('cart')): ?>
<li>
	<div class="widget woocommerce widget_shopping_cart">
		<div class="widget_shopping_cart_content">
			<ul class="woocommerce-mini-cart cart_list product_list_widget ">
			<?php if(Session::has('cart') && Session::get('cart')->items): ?>
		    <?php $__currentLoopData = Session::get('cart')->items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li class="woocommerce-mini-cart-item mini_cart_item cremove<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>">
					<a href="javascript:;" class="remove cart-remove" data-class="cremove<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>" data-href="<?php echo e(route('product.cart.remove',$product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values']))); ?>" aria-label="Remove this item" data-product_id="65" data-product_sku="">×</a>
					<a href="<?php echo e(route('front.product',$product['item']['slug'])); ?>">
						<img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="<?php echo e($product['item']['photo'] ? filter_var($product['item']['photo'], FILTER_VALIDATE_URL) ?$product['item']['photo']:asset('public/assets/images/products/'.$product['item']['photo']):asset('assets/images/noimage.png')); ?>" alt="product"><?php echo e($product['item']['name']); ?>&nbsp;
					</a>
					<span id="cqt<?php echo e($product['item']['id'].$product['size'].$product['color'].str_replace(str_split(' ,'),'',$product['values'])); ?>" class="quantity"><?php echo e($product['qty']); ?> × 		
						<span class="woocommerce-Price-amount amount">
							<?php echo e(App\Models\Product::convertPrice($product['item']['price'])); ?>

						</span>
					</span>
				</li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			<?php endif; ?>
			</ul>
			<!-- .cart_list -->
			<p class="woocommerce-mini-cart__total total">
				<strong>Subtotal:</strong>
				<span class="woocommerce-Price-amount amount cart-total">
					<?php echo e(Session::has('cart') ? App\Models\Product::convertPrice(Session::get('cart')->totalPrice) : '0.00'); ?>

				</span>
			</p>
			<p class="woocommerce-mini-cart__buttons buttons">
				<a href="<?php echo e(route('front.cart')); ?>" class="button wc-forward">View cart</a>
				<a href="<?php echo e(route('front.checkout')); ?>" class="button checkout wc-forward">Checkout</a>
			</p>
		</div>
		<!-- .widget_shopping_cart_content -->
	</div>
	<!-- .widget_shopping_cart -->
</li>

<?php else: ?> 
<p class="mt-1 p-2 text-center"><?php echo e($langg->lang8); ?></p>
<?php endif; ?>
<?php /**PATH C:\wamp64\www\mobs\resources\views/load/cart.blade.php ENDPATH**/ ?>