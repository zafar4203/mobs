<?php $__env->startSection('styles'); ?>
	<style>
		.child-select{
			color:white;
			background:#2d3274;
			border-radius:25px;
			margin-bottom:0px !important;
		}
	</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
					<input type="hidden" id="headerdata" value="<?php echo e(__('CHILD CATEGORY')); ?>">
<input type="hidden" id="attribute_data" value="<?php echo e(__('ADD NEW ATTRIBUTE')); ?>">
					<div class="content-area">
						<div class="mr-breadcrumb">
							<div class="row">
								<div class="col-lg-12">
										<h4 class="heading"><?php echo e(__('Child Categories')); ?></h4>
										<ul class="links">
											<li>
												<a href="<?php echo e(route('admin.dashboard')); ?>"><?php echo e(__('Dashboard')); ?> </a>
											</li>
											<li><a href="javascript:;"><?php echo e(__('Manage Categories')); ?></a></li>
											<li>
												<a href="<?php echo e(route('admin-childcat-index')); ?>"><?php echo e(__('Child Categories')); ?></a>
											</li>
										</ul>
								</div>
							</div>
						</div>

						<div class="row">
                                <div class="col-lg-12">
                                    <form>
                                        <div class="row filters-row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="search">Category:</label>
                                                    <input type="text" class="form-control" id="category">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
													<div class="form-group">
													<label for="search">Sub Category:</label>
													<input type="text" class="form-control" id="subcategory">
												</div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
													<label for="search">Name:</label>
													<input type="text" class="form-control" id="search">
                                                </div>
                                            </div>
											<div class="col-md-2 mt-4">
												<button id="clearFilter" class="btn btn-primary">Clear Filter</button>
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>


						<div class="product-area">
							<div class="row">
								<div class="col-lg-12">
									<div class="mr-table allproduct">

                        <?php echo $__env->make('includes.admin.form-success', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

										<div class="table-responsiv">
												<table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
													<thead>
														<tr>
															<th><?php echo e(__('Category')); ?></th>
															<th><?php echo e(__('Sub Category')); ?></th>
															<th><?php echo e(__('Name')); ?></th>
															<th><?php echo e(__('Slug')); ?></th>
															<th width="20%"><?php echo e(__('Attributes')); ?></th>
															<th><?php echo e(__('Status')); ?></th>
															<th><?php echo e(__('Options')); ?></th>
																				</tr>
													</thead>
												</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>



										<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">

										<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
												<div class="submit-loader">
														<img  src="<?php echo e(asset('assets/images/'.$gs->admin_loader)); ?>" alt="">
												</div>
											<div class="modal-header">
											<h5 class="modal-title"></h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											</div>
											<div class="modal-body">

											</div>
											<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></button>
											</div>
										</div>
										</div>
</div>





										<div class="modal fade" id="attribute" tabindex="-1" role="dialog" aria-labelledby="attribute" aria-hidden="true">

										<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
												<div class="submit-loader">
														<img  src="<?php echo e(asset('assets/images/'.$gs->admin_loader)); ?>" alt="">
												</div>
											<div class="modal-header">
											<h5 class="modal-title"></h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
											</div>
											<div class="modal-body">

											</div>
											<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo e(__('Close')); ?></button>
											</div>
										</div>
										</div>
</div>






<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

	<div class="modal-header d-block text-center">
		<h4 class="modal-title d-inline-block"><?php echo e(__('Confirm Delete')); ?></h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
	</div>

      <!-- Modal body -->
      <div class="modal-body">
            <p class="text-center"><?php echo e(__('You are about to delete this Child Category. Everything under this category will be deleted.')); ?></p>
            <p class="text-center"><?php echo e(__('Do you want to proceed?')); ?></p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer justify-content-center">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo e(__('Cancel')); ?></button>
            <a class="btn btn-danger btn-ok"><?php echo e(__('Delete')); ?></a>
      </div>

    </div>
  </div>
</div>



<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>



    <script type="text/javascript">

		var table = $('#geniustable').DataTable({
			   ordering: false,
               processing: true,
			   serverSide: false,
			   sDom: 'lrtip',
               ajax: '<?php echo e(route('admin-childcat-datatables')); ?>',
               columns: [
               			{ data: 'category'},
               			{ data: 'subcategory'},
    	                { data: 'name', name: 'name' },
								{ data: 'slug', name: 'slug' },
                    { data: 'attributes', name: 'attributes', searchable: false, orderable: false },
                    { data: 'status', searchable: false, orderable: false},
            				{ data: 'action', searchable: false, orderable: false }
								],
                language : {
                	processing: '<img src="<?php echo e(asset('assets/images/'.$gs->admin_loader)); ?>">'
                },
				drawCallback : function( settings ) {
	    				$('.select').niceSelect();
				}
            });

		$(document).on('keyup', '#search', function() {
			var value = $(this).val();
			if(value.length > 0){
				table.columns(2).search(value).draw();
			}else{
				table.columns(2).search(" ").draw();
			}
		});

		$(document).on('keyup', '#category', function() {
			var value = $(this).val();
			table.columns(0).search(value).draw();
		});

		$(document).on('keyup', '#subcategory', function() {
			var value = $(this).val();
			table.columns(1).search(value).draw();
		});
		
      	$(function() {
        $(".filters-row").prepend('<div class="col-sm-12 mb-3 text-right">'+
        	'<a class="add-btn" data-href="<?php echo e(route('admin-childcat-create')); ?>" id="add-data" data-toggle="modal" data-target="#modal1">'+
          '<i class="fas fa-plus"></i> <?php echo e(__('Add New Child Category')); ?>'+
          '</a>'+
          '</div>');
      });

	  $('#clearFilter').on('click', function(e){
			e.preventDefault();
			$('#category').val("");
			$('#search').val("");
			$('#subcategory').val("");

			table.columns(0).search($('#category').val()).draw();
			table.columns(1).search($('#subcategory').val()).draw();
			table.columns(2).search($('#search').val()).draw();
			table.search('').draw(); 
		});  




$(document).on('change','.child-select',function () {

var link = $(this).val();

var data = $(this).find(':selected').attr('data-val');
if(data == 0)
{
  $(this).next(".nice-select.process.select1.vdroplinks").removeClass("drop-success").addClass("drop-danger");
}
else{
  $(this).next(".nice-select.process.select1.vdroplinks").removeClass("drop-danger").addClass("drop-success");
}
$.get(link);
$.notify("Status Updated Successfully.","success");
});

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\mobs\resources\views/admin/childcategory/index.blade.php ENDPATH**/ ?>