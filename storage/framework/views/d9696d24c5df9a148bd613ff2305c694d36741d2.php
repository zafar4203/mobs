<?php $__env->startSection('styles'); ?>
    <style>
    .disabled{
        pointer-events: none;
        }
    .main-shop-page{
        margin-top:3%;
        margin-bottom:3%;
    }    
    .page-content{
        margin-top:5%;
    }    


    .breadcrumb-area .container{
        width: auto !important;
    }

    @media  only screen and (max-width: 600px) {
        ol.breadcrumb{
            margin-left:0px !important
        }
    }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('class','page-template-default'); ?>

<?php $__env->startSection('content'); ?>

<div class="breadcrumb-area">
            <div class="container">
                <ol class="breadcrumb breadcrumb-list">
                    <li class="breadcrumb-item"><a href="<?php echo e(route('front.index')); ?>">Home</a></li>
                    <li class="breadcrumb-item active"> <?php echo e($page->title); ?> </li>
                </ol>
            </div>
        </div>

<div class="col-full">
    <div class="row">
                    <!-- Breadcrumb Area Start Here -->
       
        <!-- Breadcrumb Area End Here -->
        <!-- Shop Page Start -->
             <!-- .woocommerce-breadcrumb -->
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                <div class="type-page hentry">
                    <div class="entry-content">
                        <!-- Row End -->
                            <h1><?php echo e($page->title); ?></h1>
                            <div class="page-content">
                                <?php echo $page->details; ?>

                            </div>
                        <!-- Row End -->
                    </div>
                    <!-- Container End -->
                </div>
            </main>
        </div>
    </div>
</div>




<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\wamp64\www\mobs\resources\views/front/page.blade.php ENDPATH**/ ?>