<input type="hidden" value="<?php echo e($prods->total()); ?>" id="total-pp" />
<?php if(count($prods) > 0): ?>
	<?php $__currentLoopData = $prods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <!-- .product -->
    <div class="product <?php echo e($key == 0?'first':''); ?>">
        <div class="yith-wcwl-add-to-wishlist">
            <a id="<?php echo e($prod->id); ?>" href="javascript:;" class="add-to-wish" data-href="<?php echo e(route('user-wishlist-add',$prod->id)); ?>" rel="nofollow" class="add_to_wishlist"> Add to Wishlist</a>
        </div>
        <!-- .yith-wcwl-add-to-wishlist -->
        <a class="woocommerce-LoopProduct-link woocommerce-loop-product__link" href="<?php echo e(route('front.product',$prod->slug)); ?>">
            <img width="224" height="197" alt="" class="attachment-shop_catalog size-shop_catalog wp-post-image" src="<?php echo e($prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png')); ?>">
            <span class="price">
                <span class="woocommerce-Price-amount amount"><?php echo e($prod->setCurrency()); ?></span>
            </span>
            <h2 class="woocommerce-loop-product__title"><?php echo e($prod->showName()); ?></h2>
        </a>
        <!-- .woocommerce-LoopProduct-link -->
        <div class="hover-area">
            <a data-href="<?php echo e(route('product.cart.add',$prod->id)); ?>" class="add-to-cart button" href="javascript:;">Add to cart</a>
            <a class="add-to-compare-link add-to-compare" data-href="<?php echo e(route('product.compare.add',$prod->id)); ?>">Add to compare</a>
        </div>
        <!-- .hover-area -->
    </div>
    <!-- .product -->
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    

<div class="col-lg-12">
    <div class="page-center mt-5">
        <?php echo $prods->appends(['search' => request()->input('search')])->links('pagination.default'); ?>



    </div>
</div>
<?php else: ?>
<div class="col-lg-12">
<div class="page-center">
        <h4 class="text-center"><?php echo e($langg->lang60); ?></h4>
</div>
</div>
<?php endif; ?>


<?php if(isset($ajax_check)): ?>


<script type="text/javascript">


// Tooltip Section


$('[data-toggle="tooltip"]').tooltip({
});
$('[data-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});




$('[rel-toggle="tooltip"]').tooltip();

$('[rel-toggle="tooltip"]').on('click',function(){
$(this).tooltip('hide');
});


// Tooltip Section Ends

</script>

<?php endif; ?><?php /**PATH C:\wamp64\www\mobs\resources\views/includes/product/filtered-products.blade.php ENDPATH**/ ?>