<div id="secondary" class="widget-area shop-sidebar" role="complementary">
    <div class="widget woocommerce widget_product_categories techmarket_widget_product_categories" id="techmarket_product_categories_widget-2">
        <ul class="product-categories ">
            <li class="product_cat">
                <span>Browse Categories</span>
                <ul class="show-all-cat">
                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if(count($element->subs) > 0): ?>
                        <li class="product_cat">
                            <span class="show-all-cat-dropdown"><a href="<?php echo e(route('front.category' , $element->slug)); ?>"><?php echo e($element->name); ?> (<?php echo e(count($element->products)); ?>)</a></span>
                            <ul style="border-top:1px solid #ececec;">
                                <?php $__currentLoopData = $element->subs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(count($sub->childs) > 0): ?>
                                        <li class="product_cat">
                                            <span class="show-all-cat-dropdown"><a href="<?php echo e(route('front.category' , [$element->slug , $sub->slug])); ?>"><?php echo e($sub->name); ?> (<?php echo e(count($sub->products)); ?>)</a></span>
                                            <ul style="border-top:1px solid #ececec;">
                                                <?php $__currentLoopData = $sub->childs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $child): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li class="cat-item"><a href="<?php echo e(route('front.category' , [$element->slug , $sub->slug , $child->slug])); ?>"><?php echo e($child->name); ?> (<?php echo e(count($child->products)); ?>)</a></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                        </li>
                                    <?php else: ?>
                                    <li class="cat-item"><a href="<?php echo e(route('front.category' , [$element->slug , $sub->slug])); ?>"><?php echo e($sub->name); ?> (<?php echo e(count($sub->products)); ?>)</a></li>  
                                    <?php endif; ?>




                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li class="cat-item">
                            <a href="<?php echo e(route('front.category' , $element->slug)); ?>"><span class="no-child"></span><?php echo e($element->name); ?> (<?php echo e(count($element->products)); ?>)</a>
                        </li>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </li>
        </ul>
    </div>
    <div id="techmarket_products_filter-3" class="widget widget_techmarket_products_filter">
        <span class="gamma widget-title">Filters</span>
        <div class="widget woocommerce widget_price_filter" id="woocommerce_price_filter-2">
            <p>
                <span class="gamma widget-title">Filter by price</span>
            </p>
            <form id="catalogForm" action="<?php echo e(route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])); ?>" method="GET">

                <?php if(!empty(request()->input('search'))): ?>
                <input type="hidden" name="search" value="<?php echo e(request()->input('search')); ?>">
                <?php endif; ?>
                <?php if(!empty(request()->input('sort'))): ?>
                <input type="hidden" name="sort" value="<?php echo e(request()->input('sort')); ?>">
                <?php endif; ?>

                <input type="hidden" name="min" value="0" id="minamount" />
                <input type="hidden" name="max" value="500" id="maxamount" />
    
                <div class="price_slider_amount">
                    <input id="amount" type="text" placeholder="Min price" data-min="6" value="33" name="min_price" style="display: none;">
                    <button type="submit" class="button filter-btn" type="button">Filter</button>
                </div>
                <div id="slider-range" class="price_slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                    <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div>
                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span>
                    <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div>
                </div>
            </form>
        </div>

<div class="widget woocommerce widget_layered_nav maxlist-more" id="woocommerce_layered_nav-2">
        <?php if((!empty($cat) && !empty(json_decode($cat->attributes, true))) || (!empty($subcat) && !empty(json_decode($subcat->attributes, true))) || (!empty($childcat) && !empty(json_decode($childcat->attributes, true)))): ?>
        <form id="attrForm" action="<?php echo e(route('front.category', [Request::route('category'), Request::route('subcategory'), Request::route('childcategory')])); ?>" method="post">

        <?php if(!empty($cat) && !empty(json_decode($cat->attributes, true))): ?>
            <?php $__currentLoopData = $cat->attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $attr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <!-- Product Size Start -->

                        <span class="gamma widget-title"><?php echo e($attr->name); ?></span>
                        <ul>
                            <?php if(!empty($attr->attribute_options)): ?>
                            <?php $__currentLoopData = $attr->attribute_options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="wc-layered-nav-term ">
                                <input class="attribute-input" name="<?php echo e($attr->input_name); ?>[]" id="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>" value="<?php echo e($option->name); ?>" type="checkbox">
                                <label class="form-check-label" for="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>"><?php echo e($option->name); ?></label>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>  

                        </ul>


            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>



        <?php if(!empty($subcat) && !empty(json_decode($subcat->attributes, true))): ?>
            <?php $__currentLoopData = $subcat->attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $attr): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                        <span class="gamma widget-title"><?php echo e($attr->name); ?></span>
                        <ul>
                            <?php if(!empty($attr->attribute_options)): ?>
                            <?php $__currentLoopData = $attr->attribute_options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="wc-layered-nav-term ">
                                <input class="form-check-input attribute-input" name="<?php echo e($attr->input_name); ?>[]" id="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>" value="<?php echo e($option->name); ?>" type="checkbox">
                                <label class="form-check-label" for="<?php echo e($attr->input_name); ?><?php echo e($option->id); ?>"><?php echo e($option->name); ?></label>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>  

                        </ul>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>


        </form>
        <?php endif; ?>

        </div>
    </div>


    <div class="widget widget_techmarket_products_carousel_widget">
    <section id="single-sidebar-carousel" class="section-products-carousel">
        <header class="section-header">
            <h2 class="section-title">Latest Products</h2>
            <nav class="custom-slick-nav"></nav>
        </header>
        <!-- .section-header -->
        <div class="products-carousel" data-ride="tm-slick-carousel" data-wrap=".products" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;rows&quot;:2,&quot;slidesPerRow&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;prevArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-left\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;nextArrow&quot;:&quot;&lt;a href=https://transvelo.github.io/"#\&quot;&gt;&lt;i class=\&quot;tm tm-arrow-right\&quot;&gt;&lt;\/i&gt;&lt;\/a&gt;&quot;,&quot;appendArrows&quot;:&quot;#single-sidebar-carousel .custom-slick-nav&quot;}">
            <div class="container-fluid">
                <div class="woocommerce columns-1">
                    <div class="products">

                        <?php $i=0; 
                            $products = \App\models\Product::query()->where("status",1)->where("latest",1)->get();
                        ?>
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $prod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($prod->latest == 1): ?>
                        <?php if($i < 3): ?>

                        <div class="landscape-product-widget product">
                            <a class="woocommerce-LoopProduct-link" href="<?php echo e(route('front.product',$prod->slug)); ?>" tabindex="-1">
                                <div class="media">
                                <img class="wp-post-image" src="<?php echo e($prod->photo ? asset('public/assets/images/products/'.$prod->photo):asset('public/assets/images/noimage.png')); ?>" alt="">
                                        <div class="media-body">
                                            <span class="price">
                                                <ins>
                                                    <span class="amount"> <?php echo e($prod->setCurrency()); ?></span>
                                                </ins>
                                                <!-- <del>
                                                    <span class="amount">26.99</span>
                                                </del> -->
                                            </span>
                                            <!-- .price -->
                                            <h2 class="woocommerce-loop-product__title"><?php echo e($prod->showName()); ?></h2>
                                            <div class="techmarket-product-rating">
                                                <div title="Rated 0 out of 5" class="star-rating">
                                                    <span style="width:0%">
                                                        <strong class="rating">0</strong> out of 5</span>
                                                </div>
                                                <span class="review-count">(0)</span>
                                            </div>
                                            <!-- .techmarket-product-rating -->
                                        </div>
                                        <!-- .media-body -->
                                </div>
                                <!-- .media -->
                            </a>
                            <!-- .woocommerce-LoopProduct-link -->
                        </div>
                        <?php $i++; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>
                    <!-- .products -->
                </div>
                <!-- .woocommerce -->
            </div>
            <!-- .container-fluid -->
        </div>
        <!-- .products-carousel -->
    </section>
    <!-- .section-products-carousel -->
</div>

</div><?php /**PATH C:\wamp64\www\mobs\resources\views/includes/catalog.blade.php ENDPATH**/ ?>